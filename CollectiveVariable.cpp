/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "CollectiveVariable.h"
namespace py = pybind11;

void export_integerCollective(pybind11::module& m){
py::class_<CollectiveVariable<unsigned int>, hoomd::Compute, std::shared_ptr<CollectiveVariable<unsigned int>>>(m, "IntegerCollective")
.def(py::init<std::shared_ptr<hoomd::SystemDefinition>, std::shared_ptr<Molecules>>());
}