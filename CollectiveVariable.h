/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_COLLECTIVEVARIABLE_H
#define HOOMD_COLLECTIVEVARIABLE_H

#include <hoomd/Compute.h>
#include "hoomd/ParticleGroup.h"
#include "Molecules.h"

namespace py = pybind11;

template<class T>
class  PYBIND11_EXPORT CollectiveVariable : public hoomd::Compute {
public:
    CollectiveVariable(std::shared_ptr<hoomd::SystemDefinition> sysdef, std::shared_ptr<Molecules> molecules) : Compute(sysdef), m_molecules(molecules) {
        hoomd::GPUArray<T> t_col(m_pdata->getN(), m_exec_conf);
        m_collective.swap(t_col);
        //m_collective.resize(m_pdata->getN());
    }

    hoomd::GPUArray<T>& getCollective(){
        return m_collective;
    }

protected:
    std::shared_ptr<Molecules> m_molecules;
    hoomd::GPUArray<T> m_collective;
};

void export_integerCollective(pybind11::module& m);

#endif //HOOMD_COLLECTIVEVARIABLE_H
