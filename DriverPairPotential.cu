//
// Created by martin on 18-09-21.
//

#include <hoomd/md/PotentialPairGPU.cuh>
#include <hoomd/md/PotentialPairDPDThermoGPU.cuh>
#include "PotentialPairGPU_MC.cuh"

//#include "DriverPairPotential.cuh"
#include "EvaluatorPairTruncatedCoulomb.h"
#include "EvaluatorPairCoulombMC.h"
#include "EvaluatorPairGhostDPDLJThermo.h"
//#include "EvaluatorPairCosineSq.h"
#include "EvaluatorPairAlchemicalLJ.h"
#include <hoomd/md/EvaluatorPairLJ.h>
//#include <hoomd/md/AllDriverPotentialPairGPU.cuh>

namespace hoomd::md::kernel
    {
//! normal pair forces

template __attribute__((visibility("default"))) hipError_t
gpu_compute_pair_forces<EvaluatorPairTruncatedCoulomb>(
    const pair_args_t& pair_args,
    const EvaluatorPairTruncatedCoulomb::param_type* d_params);

template __attribute__((visibility("default"))) hipError_t
gpu_compute_pair_forces<EvaluatorPairAlchemicalLJ>(
    const pair_args_t& pair_args,
    const EvaluatorPairAlchemicalLJ::param_type* d_params);

template __attribute__((visibility("default"))) hipError_t
gpu_compute_pair_forces<EvaluatorPairGhostDPDLJThermo>(
    const pair_args_t& pair_args,
    const EvaluatorPairGhostDPDLJThermo::param_type* d_params);

template __attribute__((visibility("default"))) hipError_t
gpu_compute_dpd_forces<EvaluatorPairGhostDPDLJThermo>(const dpd_pair_args_t& args,
                                        const typename EvaluatorPairGhostDPDLJThermo::param_type* d_params);

//! due to inheritance, potentials used for MC need to be available in regular computes
    template __attribute__((visibility("default"))) hipError_t
    gpu_compute_pair_forces<EvaluatorPairCoulombMC>(
            const pair_args_t& pair_args,
            const EvaluatorPairCoulombMC::param_type* d_params);

    }


template __attribute__((visibility("default"))) hipError_t
MC_gpu_compute_pair_forces<hoomd::md::EvaluatorPairLJ>(const MC_pair_args_t& pair_args,
                                                       const hoomd::md::EvaluatorPairLJ::param_type* d_params);

template __attribute__((visibility("default"))) hipError_t
MC_gpu_compute_pair_forces<EvaluatorPairCoulombMC>(const MC_pair_args_t& pair_args,
                                               const EvaluatorPairCoulombMC::param_type* d_params);



/*

cudaError_t gpu_compute_lj_mc_forces(const MC_pair_args_t& pair_args, const Scalar2 *d_params)
{
    return MC_gpu_compute_pair_forces_mc<EvaluatorPairLJ>(pair_args, d_params);
}


cudaError_t gpu_compute_truncated_coulomb_forces(const pair_args_t& pair_args, const Scalar *d_params){
    return gpu_compute_pair_forces<EvaluatorPairTruncatedCoulomb>(pair_args, d_params);
}

cudaError_t gpu_compute_coulomb_mc_forces(const MC_pair_args_t& pair_args, const Scalar *d_params){
    return MC_gpu_compute_pair_forces_mc<EvaluatorPairCoulombMC>(pair_args, d_params);
}


cudaError_t gpu_compute_ghost_dpdljthermodpd_forces(const dpd_pair_args_t& args,
                                              const Scalar4 *d_params)
{
    return gpu_compute_dpd_forces<EvaluatorPairGhostDPDLJThermo>(args,
                                                                 d_params);
}


cudaError_t gpu_compute_ghost_dpdljthermo_forces(const pair_args_t& args,
                                           const Scalar4 *d_params)
{
    return gpu_compute_pair_forces<EvaluatorPairGhostDPDLJThermo>(args,
                                                                  d_params);
}

cudaError_t gpu_compute_cosinesq_forces(const pair_args_t& args, const Scalar2 *d_params){
    return gpu_compute_pair_forces<EvaluatorPairCosineSq>(args, d_params);
}

cudaError_t gpu_compute_lj_alchemical_forces(const pair_args_t& args, const Scalar4 *d_params){
    return gpu_compute_pair_forces<EvaluatorPairAlchemicalLJ>(args, d_params);
}*/