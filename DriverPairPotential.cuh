//
// Created by martin on 18-09-21.
//

#ifndef HOOMD_DRIVERPAIRPOTENTIAL_CUH
#define HOOMD_DRIVERPAIRPOTENTIAL_CUH

#include "PotentialPairGPU_MC.cuh"
#include <hoomd/md/PotentialPairGPU.cuh>
#include <hoomd/md/PotentialPairDPDThermoGPU.cuh>

cudaError_t gpu_compute_lj_mc_forces(const MC_pair_args_t& pair_args, const Scalar2 *d_params);


cudaError_t gpu_compute_truncated_coulomb_forces(const pair_args_t& pair_args, const Scalar *d_params);

cudaError_t gpu_compute_coulomb_mc_forces(const MC_pair_args_t& pair_args, const Scalar *d_params);

cudaError_t gpu_compute_ghost_dpdljthermodpd_forces(const dpd_pair_args_t& args, const Scalar4 *d_params);

cudaError_t gpu_compute_ghost_dpdljthermo_forces(const pair_args_t& args, const Scalar4 *d_params);

cudaError_t gpu_compute_cosinesq_forces(const pair_args_t&, const Scalar2*);

cudaError_t gpu_compute_lj_alchemical_forces(const pair_args_t&, const Scalar4*);
#endif //HOOMD_DRIVERPAIRPOTENTIAL_H
