/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

#ifndef HOOMD_EVALUATORPAIRALCHEMICALLJ_H
#define HOOMD_EVALUATORPAIRALCHEMICALLJ_H


#ifndef NVCC
#include <string>
#endif

#include "hoomd/HOOMDMath.h"

/*! \file EvaluatorPairLJ.h
    \brief Defines the pair evaluator class for truncated coulombic potentials
*/

#ifdef __HIPCC__
#define DEVICE __device__
#define HOSTDEVICE __host__ __device__
#else
#define DEVICE
#define HOSTDEVICE
#endif

using namespace hoomd;

class EvaluatorPairAlchemicalLJ
{
public:
    //! Define the parameter type used by this pair potential evaluator
    //typedef Scalar4 param_type;
    struct param_type{
        Scalar mean_eps, mean_sigma_pow_6, delta_eps, delta_sigma_over_mean_sigma;

        DEVICE void load_shared(char*& ptr, unsigned int& available_bytes) { }

        HOSTDEVICE void allocate_shared(char*& ptr, unsigned int& available_bytes) const { }

#ifdef ENABLE_HIP
        //! Set CUDA memory hints
        void set_memory_hint() const
            {
            // default implementation does nothing
            }
#endif

#ifndef __HIPCC__
        param_type() : mean_eps(0), mean_sigma_pow_6(0), delta_eps(0), delta_sigma_over_mean_sigma(0) { }

        param_type(pybind11::dict v, bool managed = false)
            {
            mean_eps = (v["mean_epsilon"].cast<Scalar>());
            mean_sigma_pow_6 = (v["mean_sigma_pow_6"].cast<Scalar>());
            delta_eps = (v["delta_epsilon"].cast<Scalar>());
            delta_sigma_over_mean_sigma = (v["delta_sigma_pow_6"].cast<Scalar>());
            }

        pybind11::dict asDict()
            {
            pybind11::dict v;
            v["mean_epsilon"] = mean_eps;
            v["mean_sigma_pow_6"] = mean_sigma_pow_6;
            v["delta_epsilon"] = delta_eps;
            v["delta_sigma_pow_6"] = delta_sigma_over_mean_sigma;
            return v;
            }
#endif

        } __attribute__((aligned(32)));
    //! Constructs the pair potential evaluator
    /*! \param _rsq Squared distance beteen the particles
        \param _rcutsq Sqauared distance at which the potential goes to 0
        \param _params Per type pair parameters of this potential
    */
    DEVICE EvaluatorPairAlchemicalLJ(Scalar _rsq, Scalar _rcutsq, const param_type& _params)
            : rsq(_rsq), rcutsq(_rcutsq), mean_eps(_params.mean_eps), mean_sigma_pow6(_params.mean_sigma_pow_6), delta_eps(_params.delta_eps), delta_sigma_over_mean_sigma(_params.delta_sigma_over_mean_sigma)
    {
    }

    //! Coulomb doesn't use diameter
    DEVICE static bool needsDiameter() { return false; }
    //! Accept the optional diameter values
    /*! \param di Diameter of particle i
        \param dj Diameter of particle j
    */
    DEVICE void setDiameter(Scalar di, Scalar dj) { }

    //! Coulomb uses charge
    DEVICE static bool needsCharge() { return false; }
    //! Accept the optional charges
    /*! \param qi Charge of particle i
        \param qj Charge of particle j
    */
    DEVICE void setCharge(Scalar qi, Scalar qj)
    {}

    //! Evaluate the force and energy
    /*! \param pair_eng Outputs dU / dlambda for a LJ potential

        \return True if they are evaluated or false if they are not because we are beyond the cuttoff
    */

    DEVICE bool evalForceAndEnergy(Scalar& force_divr, Scalar& pair_eng, bool energy_shift)
    { // only compute energy since we dont need the force from this, its meant to evaluate dH / dlambda
        // compute the force divided by r in force_divr
        if (rsq < rcutsq && (delta_eps != Scalar(0.0) || delta_sigma_over_mean_sigma != Scalar(0.0)))
        {
            Scalar rinv2 = Scalar(1.0) / rsq;
            Scalar rinv6  = rinv2 * rinv2 * rinv2;

            Scalar mean_sigma_6_over_r6 = mean_sigma_pow6 * rinv6;
            Scalar e = mean_eps * delta_sigma_over_mean_sigma *  (Scalar(-6.0) + Scalar(12.0) * mean_sigma_6_over_r6)
                    + delta_eps * (Scalar(-1.0) + mean_sigma_6_over_r6);
            pair_eng += Scalar(4.0) * e * mean_sigma_pow6 * rinv6;

            return true;
        }
        else
            return false;
    }

#ifndef NVCC
    //! Get the name of this potential
    /*! \returns The potential name. Must be short and all lowercase, as this is the name energies will be logged as
        via analyze.log.
    */
    static std::string getName()
    {
        return std::string("alchemicalLJ");
    }
    std::string getShapeSpec() const
    {
    throw std::runtime_error("Shape definition not supported for this pair potential.");
    }
#endif

    DEVICE Scalar evalPressureLRCIntegral()
        {
        return Scalar(0.0);
        }

    DEVICE Scalar evalEnergyLRCIntegral()
        {
            return Scalar(0.0);
        }

protected:
    Scalar rsq;     //!< Stored rsq from the constructor
    Scalar rcutsq;  //!< Stored rcutsq from the constructor
    Scalar mean_eps; //! lambda epsilon1 + (1-lambda) epsilon 2
    Scalar mean_sigma_pow6;  //! (lambda sigma_1 + (1-lambda) sigma_2)^6
    Scalar delta_eps; //! epsilon_1 - epsilon_2
    Scalar delta_sigma_over_mean_sigma; //! (sigma_1 - sigma_2) / (lambda sigma_1 + (1-lambda) sigma_2)
};


#endif //HOOMD_EVALUATORPAIRALCHEMICALLJ_H
