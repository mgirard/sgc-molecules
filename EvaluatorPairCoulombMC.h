/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */


#ifndef HOOMD_EVALUATORPAIRCOULOMBMC_H
#define HOOMD_EVALUATORPAIRCOULOMBMC_H


#ifndef NVCC
#include <string>
#endif

#include "hoomd/HOOMDMath.h"

/*! \file EvaluatorPairLJ.h
    \brief Defines the pair evaluator class for LJ potentials
    \details As the prototypical example of a MD pair potential, this also serves as the primary documetnation and
    base reference for the implementation of pair evaluators.
*/

// need to declare these class methods with __device__ qualifiers when building in nvcc
// DEVICE is __host__ __device__ when included in nvcc and blank when included into the host compiler
#ifdef __HIPCC__
#define DEVICE __device__
#define HOSTDEVICE __host__ __device__
#else
#define DEVICE
#define HOSTDEVICE
#endif

//! Class for evaluating the LJ pair potential
/*! <b>General Overview</b>


*/
using namespace hoomd;
class EvaluatorPairCoulombMC
{
public:
    //! Define the parameter type used by this pair potential evaluator
struct param_type{

    Scalar _;

    DEVICE void load_shared(char*&, unsigned int&){}

    HOSTDEVICE void allocate_shared(char*&, unsigned int&) const{}

#ifdef ENABLE_HIP
    //! Set CUDA memory hints
    void set_memory_hints() const { }
#endif
#ifndef __HIPCC__
    param_type() { }

    param_type(pybind11::dict v, bool managed = false)
        {
        }

    pybind11::dict asDict()
        {
        pybind11::dict v;
        return v;
        }
#endif
    }__attribute__((aligned(16)));

    //! Constructs the pair potential evaluator
    /*! \param _rsq Squared distance beteen the particles
        \param _rcutsq Sqauared distance at which the potential goes to 0
        \param _params Per type pair parameters of this potential
    */
    DEVICE EvaluatorPairCoulombMC(Scalar _rsq, Scalar _rcutsq, const param_type& _params)
            : rsq(_rsq), rcutsq(_rcutsq), qiqj(0.0)
    {
    }

    //! LJ doesn't use diameter
    DEVICE static bool needsDiameter() { return false; }
    //! Accept the optional diameter values
    /*! \param di Diameter of particle i
        \param dj Diameter of particle j
    */
    DEVICE void setDiameter(Scalar di, Scalar dj) { }

    //! Coulomb uses charge
    DEVICE static bool needsCharge() { return true; }
    //! Accept the optional diameter values
    /*! \param qi Charge of particle i
        \param qj Charge of particle j
    */
    DEVICE void setCharge(Scalar qi, Scalar qj)
    {qiqj = qj;} // compute the potential as if qi = 1.0

    //! Evaluate the force and energy
    /*! \param force_divr Output parameter to write the computed force divided by r.
        \param pair_eng Output parameter to write the computed pair energy
        \param energy_shift If true, the potential must be shifted so that V(r) is continuous at the cutoff
        \note There is no need to check if rsq < rcutsq in this method. Cutoff tests are performed
              in PotentialPair.

        \return True if they are evaluated or false if they are not because we are beyond the cuttoff
    */

    DEVICE bool evalForceAndEnergy(Scalar& force_divr, Scalar& pair_eng, bool energy_shift)
    {
        // compute the force divided by r in force_divr
        if (rsq < rcutsq && qiqj != 0)
        {
            Scalar rinv = fast::rsqrt(rsq);
            Scalar r = Scalar(1.0) / rinv;
            Scalar rcinv = fast::rsqrt(rcutsq);
            Scalar rc = Scalar(1.0) / rcinv;
            pair_eng = qiqj * (rinv - rcinv + rcinv * rcinv * (r - rc));
            force_divr = qiqj * rinv * (rinv * rinv - rcinv * rcinv);
            return true;
        }
        else
            return false;
    }
    DEVICE Scalar evalPressureLRCIntegral()
        {
        return Scalar(0.0);
        }

    DEVICE Scalar evalEnergyLRCIntegral()
        {
        return Scalar(0.0);
        }
#ifndef NVCC
    //! Get the name of this potential
    /*! \returns The potential name. Must be short and all lowercase, as this is the name energies will be logged as
        via analyze.log.
    */
    static std::string getName()
    {
        return std::string("coulombMC");
    }
    std::string getShapeSpec() const
    {
    throw std::runtime_error("Shape definition not supported for this pair potential.");
    }
#endif

protected:
    Scalar rsq;     //!< Stored rsq from the constructor
    Scalar rcutsq;  //!< Stored rcutsq from the constructor
    Scalar qiqj;
};


#endif //HOOMD_EVALUATORPAIRMARTINI_H
