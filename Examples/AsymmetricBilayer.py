import numpy as np
import math
import hoobas as Hoobas
import hoomd
import hoomd.md
import os
from hoomd import plugin_muMC

hoomd.context.initialize("--single-mpi")
hoomd.option.set_notice_level(5)

units = Hoobas.Units.SimulationUnits()
units.set_mass('amu')
units.set_energy('kJ/mol')
units.set_length('nm')

builder_box = Hoobas.SimulationDomain.EmptyHexagonalPrism(22.0, c=12.5, units=units)
nchol = 200

set_kT = 2.5
mu = 5.0

seed = np.random.randint(65535, 2000000, 1)
file_path = './Example_'

try:
    this_path = os.path.dirname(os.path.realpath(__file__))
except NameError:
    this_path = os.getcwd()

dppc_chain = Hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_PUPC_01M.itp'), os.path.join(this_path, 'lipids/PUPC-em.gro'))
dppe_chain = Hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_PUPE_01M.itp'), os.path.join(this_path, 'lipids/PUPE-em.gro'))
chol_chain = Hoobas.MartiniModels.ITPMartiniParser(os.path.join(this_path, 'lipids/martini_v2.0_CHOL_01.itp'), os.path.join(this_path, 'lipids/CHOL-em.gro'))


# tag the molecules and specific beads we need to allow chemical changes on
chol_chain.set_residues('CHOL')
dppc_chain.set_residues('PC')
dppe_chain.set_residues('PE')
for i in range(4, 9):
    dppc_chain.beads[i].residue = 'V1'
    dppe_chain.beads[i].residue = 'V1'
dppc_chain.beads[10].residue = 'V2'
dppc_chain.beads[11].residue = 'V2'
dppe_chain.beads[10].residue = 'V2'
dppe_chain.beads[11].residue = 'V2'

ml = Hoobas.Layers.Multilayer(NLayers=2, units=units)
ml.add_species(dppc_chain, 400)
ml.add_species(dppe_chain, 400)
ml.add_species(chol_chain, nchol, squish=0.8)
ml.squish = 1.0

builder = Hoobas.Build.HOOMDBuilder(builder_box, units=units)
# the add ions command is used to create water
builder.add_rho_molar_ions(55.0 / 4.0 * 0.9 * 0.90, qtype='P4', q=0.0, ion_mass=72.0, ion_diam=0.0)
builder.add_rho_molar_ions(55.0 / 4.0 * 0.1 * 0.90, qtype='BP4', q=0.0, ion_mass=72.0, ion_diam=0.0)

builder.add_layer(ml, 0.0)
#builder.correct_pnum_body_lists()
builder.remap_beadtype('P1', 'ghost')
builder.reduce_topology(bonds=True, angles=True, dihedrals=True)

builder.Electrostatics.permittivity = 15.0
builder.Electrostatics.normalize()


sysbox = hoomd.data.boxdim(*builder.sys_box)
sn = hoomd.data.make_snapshot(builder.num_beads, sysbox, builder.bead_types)
builder.set_snapshot(sn)
system = hoomd.init.read_snapshot(sn)

dist_cons = hoomd.md.constrain.distance()
dist_cons.set_params(0.03)
bond = hoomd.md.bond.harmonic(name='regular')

for bond_type in builder.bond_types:
    bond.bond_coeff.set(bond_type.typename, k=bond_type['energy_constant'], r0=bond_type['distance_constant'])
bond.bond_coeff.set('Excl', k=0.0, r0=0.0)

# to estimate energy changes in monte-carlo, we setup disable fictitious forces; the plugin will compute energy based on these potentials
angle = hoomd.md.angle.cosinesq()
sgc_angle_saturated = plugin_muMC.angle.cosinesq(name='sat')
sgc_angle_unsaturated = plugin_muMC.angle.cosinesq(name='unsat')
for angle_type in builder.angle_types:
    print("Angle :" + str(angle_type.typename) + ", k = " + str(angle_type['energy_constant']) + ", t0 = " + str(angle_type['angle_constant']))
    angle.angle_coeff.set(angle_type.typename, k=angle_type['energy_constant'], t0=angle_type['angle_constant'])
    sgc_angle_saturated.angle_coeff.set(angle_type.typename, k=25.0, t0=math.radians(180.0))
    sgc_angle_unsaturated.angle_coeff.set(angle_type.typename, k=45.0, t0=math.radians(120.0))
sgc_angle_saturated.disable(log=True)
sgc_angle_unsaturated.disable(log=True)


# MARTINI harmonic dihedral forces
def dharm(theta, kappa, theta0):
    tangle = ((theta-theta0) + np.pi) % (2*np.pi) - np.pi
    V = 0.5 * kappa * (tangle)**2.0
    T = - kappa * (tangle)
    return (V, T)

dies = hoomd.md.dihedral.table(width=512)
for die_type in builder.dihedral_types:
    dies.dihedral_coeff.set(die_type.typename, func=dharm, coeff=dict(kappa=die_type['energy_constant'], theta0=die_type['angle_constant']))

nlist = hoomd.md.nlist.cell(r_buff=0.15)
nlist.reset_exclusions(['1-2', 'constraint'])

# this is the normal LJ force. We include DPD as a local thermostat to dissipate energy that may be produced in unlikely events
nonbonded = plugin_muMC.pair.dpdlj(nlist=nlist, r_cut=2.5 * 0.62, name='local', kT=set_kT, seed=seed)

# similar to angle forces, we setup fictitious LJ forces and disable them to evaluate energy changes in Monte-Carlo updates
nonBondedForcesList = []

sgc_lj_unsaturated = plugin_muMC.pair.lj_mc(nlist=nlist, r_cut=2.5 * 0.62, override='C3', name='sat')
nonBondedForcesList.append(sgc_lj_unsaturated)

sgc_lj_saturated = plugin_muMC.pair.lj_mc(nlist=nlist, r_cut=2.5 * 0.62, override='C1', name='unsat')
nonBondedForcesList.append(sgc_lj_saturated)

sgc_lj_emptybead = plugin_muMC.pair.lj_mc(nlist=nlist, r_cut=2.5 * 0.62, override='ghost', name='ghost')
nonBondedForcesList.append(sgc_lj_emptybead)

sgc_lj_pe_headgroup = plugin_muMC.pair.lj_mc(nlist=nlist, r_cut=2.5 * 0.62, override='Qd', name='Qd')
nonBondedForcesList.append(sgc_lj_pe_headgroup)

sgc_lj_pc_headgroup = plugin_muMC.pair.lj_mc(nlist=nlist, r_cut=2.5 * 0.62, override='Q0', name='Q0')
nonBondedForcesList.append(sgc_lj_pc_headgroup)

sgc_lj_saturated.disable(log=True)
sgc_lj_unsaturated.disable(log=True)
sgc_lj_emptybead.disable(log=True)
sgc_lj_pe_headgroup.disable(log=True)
sgc_lj_pc_headgroup.disable(log=True)

# the automated system builder generates arbitrary names when importing martini files and we need to recover angle names
for atype in builder.angle_types:
    if abs(atype['angle_constant'] - math.radians(180.0)) < 1e-2 and abs(atype['energy_constant']-25.0) < 1e-2:
        atype180 = atype.typename
    if abs(atype['angle_constant'] - math.radians(120.0)) < 1e-2 and abs(atype['energy_constant']-45.0) < 1e-2:
        atype120 = atype.typename

# applies the LJ forces with a prefactor on sigma to perform initial equilibration
def set_lj(m, friction=1.0):
    hoomd.util.quiet_status()
    for beadtypeA in builder.bead_types:
        for beadtypeB in builder.bead_types:
            if beadtypeA != 'ghost' and beadtypeB != 'ghost':
                params = Hoobas.MartiniModels.MartiniForceMappings.get_pair(beadtypeA, beadtypeB, units)
            else:
                params = {'sigma': 0.62, 'eps': 0.0}
            rcm = 2.5 * 0.62
            nonbonded.pair_coeff.set(beadtypeA, beadtypeB, epsilon=m*params['eps'], sigma=m*params['sigma'], gamma=friction, r_cut=rcm)

            for force in nonBondedForcesList:
                force.pair_coeff.set(beadtypeA, beadtypeB, epsilon=m*params['eps'], sigma=m*params['sigma'], r_cut=rcm)
    hoomd.util.unquiet_status()

group_all = hoomd.group.all()

membrane_group = hoomd.group.tag_list('membrane', [i for i, b in enumerate(builder.beads) if (b.residue == 'CHOL' or b.residue == 'PC' or b.residue == 'PE' or b.residue == 'V1' or b.residue == 'V2')])
phospholipids = hoomd.group.tag_list('PL', [i for i, b in enumerate(builder.beads) if (b.residue == 'PC' or b.residue =='V1' or b.residue == 'V2')])
group_chol = hoomd.group.tag_list('chol', [i for i, b in enumerate(builder.beads) if (b.residue == 'CHOL')])

# create hoomd groups for different acyl tails & headgroups
group_sn2 = hoomd.group.tag_list('mc_pc', [i for i, b in enumerate(builder.beads) if (b.residue == 'V1')])
group_sn1 = hoomd.group.tag_list('mc_pc', [i for i, b in enumerate(builder.beads) if (b.residue == 'V2')])
group_headgroup = hoomd.group.tag_list('mc_ps', [i for i, b in enumerate(builder.beads) if (b.beadtype == 'Q0' or b.beadtype == 'Qd')])

# create a molecule ensemble
molecules = plugin_muMC.collectives.Molecules(phospholipids)

# if beads require changes in charge, e.g. for exchanges between phosphatidylcholine and phosphatidylserine, then an extra potential needs to be calculated. Due to the shape of coulombic interactions, it only needs a single potential and not one per type
#sgc_nonbonded_charge = plugin_muMC.pair.charge_mc(nlist=nlist, r_cut=1.2, name='MC-coulomb')

# describe the SGC states
sn1_states = plugin_muMC.update.StateDescriptor(types=['C3', 'C1'], type_force=[sgc_lj_unsaturated, sgc_lj_saturated], angles=[atype120, atype180], angle_force=[sgc_angle_unsaturated, sgc_angle_saturated])
sn2_states = plugin_muMC.update.StateDescriptor(types=['C3', 'C1', 'ghost'], type_force=[sgc_lj_unsaturated, sgc_lj_saturated, sgc_lj_emptybead], angles=[atype120, atype180, atype180], angle_force=[sgc_angle_unsaturated, sgc_angle_saturated, sgc_angle_saturated])
head_states = plugin_muMC.update.StateDescriptor(types=['Q0', 'Qd'], type_force=[sgc_lj_pc_headgroup, sgc_lj_pe_headgroup])

switcher_sn1 = plugin_muMC.update.ManyStatesSwitcher(group_sn1, molecules, sn1_states, phase=0, name='sn1', stateNames=['unsat', 'sat'])
switcher_sn2 = plugin_muMC.update.ManyStatesSwitcher(group_sn2, molecules, sn2_states, phase=7, name='sn2', stateNames=['unsat', 'sat', 'ghost'])
switcher_exch = plugin_muMC.update.ManyStatesSwitcher(group_headgroup, molecules, head_states, phase=14, name='PCPE', stateNames=['Q0', 'Qd'])

switcher_sn1.setRCut(2.5 * 0.62)
switcher_sn2.setRCut(2.5 * 0.62)
switcher_exch.setRCut(2.5 * 0.62)

# add the leaflet into hash computation and base it on a 1.4 nm grid
leaflet = plugin_muMC.collectives.LipidLeaflet(molecules)
leaflet.setCellListRCut(1.4)

# remove unphysical fragments: non-contiguous unsaturations or empty beads in the middle of the molecule
for element in molecules.fragments():
    fragmentSN1 = element['SGCsn1']
    fragmentSN2 = element['SGCsn2']
    hasGhost = 'ghost' in fragmentSN2
    nunsat_sn2 = sum([1 for x in fragmentSN2 if x == 'unsat'])
    nunsat_sn1 = sum([1 for x in fragmentSN1 if x == 'unsat'])
    valid = True
    if(not hasGhost and nunsat_sn2 < 3) or nunsat_sn1 > nunsat_sn2:
        valid = False

    if sum([1 for x in fragmentSN2 if x == 'ghost']) > 1:
        valid = False

    if hasGhost and list(fragmentSN2)[-1] != 'ghost':
        valid = False

    if nunsat_sn2 >= 2:
        idiff = np.diff([i for i, x in enumerate(fragmentSN2) if x == 'unsat'])
        valid &= np.all(idiff == 1)

    if not valid:
        continue

    if 'Q0' in element['SGCPCPE'] and 'upper' in element['leaflet']:
        molecules.set_mu(element['hash'], mu)
        print('Set non-zero potential to ' + str(element))
    else:
        molecules.set_mu(element['hash'], 0.0)
        print('Set zero potential to ' + str(element))

# setup charges, martini force-field uses truncated coulombic potentials
nonbonded_charge = plugin_muMC.pair.TruncatedCoulomb(nlist=nlist, r_cut=1.2, name='Coulomb')

hoomd.util.quiet_status()
for beadtypeA in builder.bead_types:
    for beadtypeB in builder.bead_types:
        nonbonded_charge.pair_coeff.set(beadtypeA, beadtypeB, r_on=0.001, r_cut=1.2)
        # sgc_nonbonded_charge.pair_coeff.set(beadtypeA, beadtypeB, r_on=0.001, r_cut=1.2)
hoomd.util.unquiet_status()
nonbonded_charge.disable()


switcher_sn1.set_params(set_kT)
switcher_sn2.set_params(set_kT)
switcher_exch.set_params(set_kT)

traj = hoomd.dump.gsd(filename=file_path+'Trajectory.gsd', period=10000, group=group_all, dynamic=['property', 'momentum', 'attribute', 'topology'], overwrite=False)
# hoomd can dump the molecular description into gsd files, with chunks state/molecules/hash for a list of all hashes, and state/molecules/molecules for a list of to which molecule beads belong
traj.dump_state(molecules)
logged_quantities = ['temperature', 'potential_energy', 'kinetic_energy', 'pressure', 'pressure_xx', 'pressure_yy', 'pressure_zz', 'volume', 'bond_harmonic_energy', 'angle_cosinesq_energy', 'pair_dpdlj_energy_local']
logger = hoomd.analyze.log(filename=file_path+'log.log', period=1000, quantities=logged_quantities, overwrite=False)


neq_steps = 20
neq_values = np.logspace(-2, 0.0, neq_steps, endpoint=True)
nvei = hoomd.md.integrate.nve(group=group_all, limit=.001)

# slowly equilibrate the system
for step in range(neq_steps):
    set_lj(neq_values[step], 500.0)
    print('setting LJ to :' + str(neq_values[step]))
    hoomd.md.integrate.mode_standard(dt=.003)
    hoomd.run(1000)

nonbonded_charge.enable()
hoomd.run(1e4)
nvei.disable()
nve_integrator = hoomd.md.integrate.nve(group=group_all)
set_lj(1.0, 20.0)
hoomd.md.integrate.mode_standard(dt=1e-5)
hoomd.run(3e4)
set_lj(1.0, 5.0)
hoomd.run(3e4)
logger.set_period(1000)
hoomd.run(1e4)
hoomd.md.integrate.mode_standard(dt=.001)
hoomd.run(1e5)

logger.set_period(2500)
nonbonded_charge.enable()


hoomd.run(1e5)
switcher_sn1.enable()
switcher_sn2.enable()
switcher_exch.enable()
hoomd.run(2e5, profile=True)
nve_integrator.disable()


nph_integrator = hoomd.plugin_muMC.integrate.nphstochastic(group=group_all, P=0.0602, tauP=80.0, couple='xy', gamma=8.0)
hoomd.md.integrate.mode_standard(dt=0.0003)
hoomd.run(1e5)
hoomd.md.integrate.mode_standard(dt=.003)
hoomd.run(1e5)
nph_integrator.set_params(tauP=20.0)
hoomd.run(1e5)


hoomd.md.integrate.mode_standard(dt=.008)
nph_integrator.set_params(tauP=8.0)
hoomd.run(1e6)

# production run with 10fs steps
hoomd.md.integrate.mode_standard(dt=0.010)
hoomd.run(2e7)
