//
// Created by mgirard on 3/2/23.
//

#include "HarmonicKurtosis.h"
namespace py = pybind11;

void export_KurtosisForceCompute(pybind11::module& m){
    py::class_<KurtosisForceCompute, std::shared_ptr<KurtosisForceCompute> >(m,"KurtosisForceCompute",py::base<ForceCompute>())
            .def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup>>())
            .def("setParameters_mu2", &KurtosisForceCompute::setParameters_mu2)
            .def("setParameters_mu4", &KurtosisForceCompute::setParameters_mu4)
            .def("getTotalEnergy", &KurtosisForceCompute::getTotalEnergy)
            .def("getKurtosis", &KurtosisForceCompute::getKurtosisValue)
            .def("getVariance", &KurtosisForceCompute::getVarianceValue)
            ;
}