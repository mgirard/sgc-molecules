//
// Created by mgirard on 3/2/23.
//

#ifndef HOOMD_HARMONICKURTOSIS_H
#define HOOMD_HARMONICKURTOSIS_H

#include <hoomd/ForceCompute.h>
using namespace hoomd;
class PYBIND11_EXPORT KurtosisForceCompute : public ForceCompute{
public:
    KurtosisForceCompute(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<ParticleGroup> group) :
            ForceCompute(sysdef), m_group(group){
        GPUArray<Scalar4> _moments(1, m_exec_conf);
        m_moments.swap(_moments);
    }

    virtual ~KurtosisForceCompute(){}

    virtual std::vector< std::string > getProvidedLogQuantities(){
        std::vector<std::string> list;
        list.push_back("kurtosis_energy_" + m_name);
        list.push_back("variance_energy_" + m_name);
        return list;
    }

    virtual Scalar getLogValue(const std::string& quantity, unsigned int timestep) {
        if(quantity == "variance_energy_" + m_name) {
            compute(timestep);
            ArrayHandle<Scalar4> h_collective(m_moments, access_location::host, access_mode::read);
            auto moments = h_collective.data[0];
            Scalar mu4 = moments.w - 4 * moments.x * moments.z + 6 * moments.x * moments.x * moments.y -
                         3 * moments.x * moments.x * moments.x * moments.x;
            Scalar mu2 = moments.y - moments.x * moments.x;
            return 0.5 * mu4_params.x * (mu4 / (mu2 * mu2) - mu4_params.y) * (mu4 / (mu2 * mu2) - mu4_params.y);
        }
        if(quantity == "variance_energy_" + m_name){
            compute(timestep);
            ArrayHandle<Scalar4> h_collective(m_moments, access_location::host, access_mode::read);
            auto moments = h_collective.data[0];
            Scalar mu2 = moments.y - moments.x * moments.x;
            return 0.5 * mu2_params.x * (mu2 - mu2_params.y) * (mu2 - mu2_params.y);
        }
        throw std::runtime_error("Bad quantity " + quantity + " requested");
    }

    void setParameters_mu4(Scalar k, Scalar kappa){
        mu4_params = {k, kappa};
    }
    void setParameters_mu2(Scalar k, Scalar kappa){
        mu2_params = {k, kappa};
    }

    Scalar getTotalEnergy(unsigned int timestep = 0){
        compute(timestep);
        ArrayHandle<Scalar4> h_collective(m_moments, access_location::host, access_mode::read);
        auto moments = h_collective.data[0];
        Scalar mu4 = moments.w - 4 * moments.x * moments.z + 6 * moments.x * moments.x * moments.y - 3 * moments.x * moments.x * moments.x * moments.x;
        Scalar mu2 = moments.y - moments.x * moments.x;
        return 0.5 * mu4_params.x * (mu4 / (mu2 * mu2) - mu4_params.y) * (mu4 / (mu2 * mu2) - mu4_params.y) + 0.5 * mu2_params.x * (mu2 - mu2_params.y) * (mu2 - mu2_params.y);
    }

    Scalar getVarianceValue(unsigned int timestep = 0){
        compute(timestep);
        ArrayHandle<Scalar4> h_collective(m_moments, access_location::host, access_mode::read);
        auto moments = h_collective.data[0];
        Scalar mu2 = moments.y - moments.x * moments.x;
        return mu2;
    }

    Scalar getKurtosisValue(unsigned int timestep = 0){
        compute(timestep);
        ArrayHandle<Scalar4> h_collective(m_moments, access_location::host, access_mode::read);
        auto moments = h_collective.data[0];
        Scalar mu4 = moments.w - 4 * moments.x * moments.z + 6 * moments.x * moments.x * moments.y - 3 * moments.x * moments.x * moments.x * moments.x;
        Scalar mu2 = moments.y - moments.x * moments.x;
        return mu4 / (mu2 * mu2);
    }

protected:

    std::shared_ptr<ParticleGroup> m_group;
    GPUArray<Scalar4> m_moments;

    std::string m_name;

    Scalar2 mu2_params;
    Scalar2 mu4_params;
};

void export_KurtosisForceCompute(pybind11::module& m);

#endif //HOOMD_HARMONICKURTOSIS_H
