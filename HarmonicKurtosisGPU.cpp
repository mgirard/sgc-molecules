//
// Created by mgirard on 3/2/23.
//

#include "HarmonicKurtosisGPU.h"
#include "HarmonicKurtosisGPU.cuh"
namespace py = pybind11;

void KurtosisForceComputeGPU::computeForces(uint64_t timestep) {
    unsigned int group_size = m_group->getNumMembers();
    {
        ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::device, access_mode::read);
        ArrayHandle<Scalar4> h_for(m_force, access_location::device, access_mode::overwrite);
        ArrayHandle<Scalar> h_vir(m_virial, access_location::device, access_mode::overwrite);
        ArrayHandle<unsigned int> h_rtag(m_pdata->getRTags(), access_location::device, access_mode::read);
        BoxDim box = m_pdata->getBox();

        ArrayHandle<unsigned int> d_group_members(m_group->getIndexArray(), access_location::device, access_mode::read);
        ArrayHandle<Scalar4> d_red_data(m_moment_reduction_data, access_location::device, access_mode::readwrite);
        ArrayHandle<Scalar4> d_reduced_moments(m_moments, access_location::device, access_mode::readwrite);

        ArrayHandle<int3> d_image(m_pdata->getImages(), access_location::device, access_mode::read);
        m_tuner->begin();
        gpu_compute_kurtosis_forces(
                group_size,
                d_group_members.data,
                h_rtag.data,
                h_pos.data,
                d_image.data,
                h_for.data,
                h_vir.data,
                m_virial.getPitch(),
                d_red_data.data,
                d_reduced_moments.data,
                box,
                m_tuner->getParam()[0],
                m_pdata->getN(),
                mu2_params,
                mu4_params);

        if (m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();
        m_tuner->end();
    }
}

void export_KurtosisForceComputeGPU(pybind11::module& m){
    py::class_<KurtosisForceComputeGPU, std::shared_ptr<KurtosisForceComputeGPU> >(m,"KurtosisForceComputeGPU",py::base<KurtosisForceCompute>())
            .def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<ParticleGroup> >());
}