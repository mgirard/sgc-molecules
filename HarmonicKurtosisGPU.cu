//
// Created by mgirard on 3/2/23.
//

#include "HarmonicKurtosisGPU.cuh"
#include <hoomd/ParticleData.cuh>
#include <hoomd/HOOMDMath.h>
#include <cub/cub.cuh>

using namespace hoomd;

__global__ void gpu_kurtosis_zero_forces_kernel(Scalar4* d_force,
                                       Scalar* d_virial,
                                       unsigned int N,
                                       unsigned int virial_pitch){
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < N) {
        d_force[idx] = make_scalar4(0.0, 0.0, 0.0, 0.0);
        d_virial[virial_pitch*0 + idx] = Scalar(0.0);
        d_virial[virial_pitch*1 + idx] = Scalar(0.0);
        d_virial[virial_pitch*2 + idx] = Scalar(0.0);
        d_virial[virial_pitch*3 + idx] = Scalar(0.0);
        d_virial[virial_pitch*4 + idx] = Scalar(0.0);
        d_virial[virial_pitch*5 + idx] = Scalar(0.0);
    }
}

__global__ void gpu_compute_kurtosis_force(unsigned int group_size,
                                           unsigned int virial_pitch,
                                           unsigned int* d_rtags,
                                           unsigned int* d_group_members,
                                           Scalar4* d_pos,
                                           Scalar4* d_force,
                                           Scalar4* d_moments,
                                           Scalar* d_virial,
                                           Scalar2 µ2params,
                                           Scalar2 µ4params,
                                           const BoxDim box,
                                           int3* d_image){
    unsigned int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i >= group_size)
        return;
    unsigned int idx = d_group_members[i];
    //unsigned int idx = d_rtags[tag];

    const auto pos = d_pos[idx];
    const double image_offset = d_image[idx].z * box.getL().z;
    const double z = (double) d_pos[idx].z + image_offset;

    const double µ1 = (double) d_moments[0].x;
    const double µ2 = (double) d_moments[0].y;
    const double µ3 = (double) d_moments[0].z;
    const double µ4 = (double) d_moments[0].w;
    const double n = (double) group_size;
    const auto over_n = 1.0 / n;

    const auto var = µ2 - µ1 * µ1;
    const auto var_sq = var*var;

    const auto µ1_2 = µ1 * µ1;
    const auto µ1_3 = µ1_2 * µ1;
    const auto µ1_4 = µ1_3 * µ1;
    Scalar force_µ4, energy_µ4;
    {
        const auto µµ4 = µ4 - 4 * µ1 * µ3 + 6 * µ1_2 * µ2 - 3 * µ1_4;
        const auto offset_term = µµ4 / var_sq - µ4params.y;

        const auto first_term = 4 * z * z * z - 12 * z * z * µ1 + 12 * z * µ1_2 - 12 * µ1_3 + 12 * µ1 * µ2 - 4 * µ3;
        const auto second_term = 4 * (z - µ1) * (-3 * µ1_4 + 6 * µ1_2 * µ2 - 4 * µ1 * µ3 + µ4);

        force_µ4 = -µ4params.x * over_n / var_sq * (first_term - second_term / var) * offset_term;
        energy_µ4 = offset_term * offset_term * Scalar(0.5) * µ4params.x / n;
    }
    Scalar force_µ2, energy_µ2;
    {
        force_µ2 = -µ2params.x * (2*z - 2 * µ1) * over_n * (var - µ2params.y);
        energy_µ2 = (var - µ2params.y) * (var - µ2params.y) * Scalar(0.5) * µ2params.x;
    }
    Scalar force = force_µ2 + force_µ4;
    d_force[idx] = {Scalar(0), Scalar(0), Scalar(force), Scalar(energy_µ2 + energy_µ4)};

    d_virial[virial_pitch * 2 + idx] = Scalar(0.5) * Scalar(pos.x * force);
    d_virial[virial_pitch * 4 + idx] = Scalar(0.5) * Scalar(pos.y * force);
    d_virial[virial_pitch * 5 + idx] = Scalar(pos.z * force);
}

template<int blockSize>
__global__ void gpu_compute_moments(unsigned int group_size,
                                    unsigned int *d_rtags,
                                    unsigned int *d_group_members,
                                    Scalar4* d_pos,
                                    Scalar4* d_moment_reduction,
                                    const BoxDim box,
                                    int3* d_image){
    unsigned int i = threadIdx.x + blockIdx.x * blockDim.x; // one thread per particle
    unsigned int idx = i < group_size ? d_group_members[i] : 0;
    //unsigned int idx = d_rtags[tag];
    Scalar4 postype = d_pos[idx];

    Scalar4 sum = {0., 0., 0., 0.};


    if(i < group_size) {
        Scalar z_pos = postype.z + (box.getL().z * d_image[idx].z);
        sum.x += z_pos;
        sum.y += z_pos * z_pos;
        sum.z += z_pos * z_pos * z_pos;
        sum.w += z_pos * z_pos * z_pos * z_pos;
    }
    __syncthreads();
    typedef cub::BlockReduce<Scalar, blockSize> BlockReduce;
    __shared__ typename BlockReduce::TempStorage temp_storage;
    Scalar4 aggregate = {0., 0., 0., 0.};
    aggregate.x = BlockReduce(temp_storage).Sum(sum.x);
    __syncthreads();
    aggregate.y = BlockReduce(temp_storage).Sum(sum.y);
    __syncthreads();
    aggregate.z = BlockReduce(temp_storage).Sum(sum.z);
    __syncthreads();
    aggregate.w = BlockReduce(temp_storage).Sum(sum.w);
    __syncthreads();

    if(threadIdx.x == 0) {
        d_moment_reduction[blockIdx.x].x = aggregate.x / group_size;
        d_moment_reduction[blockIdx.x].y = aggregate.y / group_size;
        d_moment_reduction[blockIdx.x].z = aggregate.z / group_size;
        d_moment_reduction[blockIdx.x].w = aggregate.w / group_size;
    }
}

template<int blockSize>
__global__ void gpu_reduce_moments(Scalar4* reduction_data,
                                                Scalar4* reduced_data,
                                                unsigned int grid)
{
    Scalar4 sum = {0., 0., 0., 0.};
    unsigned int i = threadIdx.x;
    while(i < grid){
        sum.x += reduction_data[i].x;
        sum.y += reduction_data[i].y;
        sum.z += reduction_data[i].z;
        sum.w += reduction_data[i].w;
        i += blockSize;
    }
    __syncthreads();
    typedef cub::BlockReduce<Scalar, blockSize> BlockReduce;
    __shared__ typename BlockReduce::TempStorage temp_storage;
    Scalar4 aggregate = {0., 0., 0., 0.};

    aggregate.x = BlockReduce(temp_storage).Sum(sum.x);
    __syncthreads();
    aggregate.y = BlockReduce(temp_storage).Sum(sum.y);
    __syncthreads();
    aggregate.z = BlockReduce(temp_storage).Sum(sum.z);
    __syncthreads();
    aggregate.w = BlockReduce(temp_storage).Sum(sum.w);
    __syncthreads();

    if(threadIdx.x == 0) {
        reduced_data[0].x = aggregate.x;
        reduced_data[0].y = aggregate.y;
        reduced_data[0].z = aggregate.z;
        reduced_data[0].w = aggregate.w;
    }
}

cudaError_t gpu_compute_kurtosis_forces(unsigned int group_size,
                                     unsigned int *d_group_members,
                                     unsigned int *d_rtags,
                                     Scalar4 *d_pos,
                                     int3* d_image,
                                     Scalar4 *d_force,
                                     Scalar *d_virial,
                                     unsigned int virial_pitch,
                                     Scalar4 *reduction_data,
                                     Scalar4 *collectives,
                                     const BoxDim box,
                                     unsigned int block_size,
                                     unsigned int N,
                                     Scalar2 params_mu2,
                                     Scalar2 params_mu4) {
    static unsigned int max_block_size = UINT_MAX;
    if (max_block_size == UINT_MAX) {
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, (const void *) gpu_compute_kurtosis_force);
        max_block_size = attr.maxThreadsPerBlock;

    }
    unsigned int run_block_size = min(block_size, max_block_size);

    // setup the grid to run the kernel
    //dim3 grid((group_one_size / run_block_size) + 1, 1, 1);
    dim3 grid_step_two(1, 1, 1);
    dim3 threads(run_block_size, 1, 1);
    //int shared_size = run_block_size * sizeof(Scalar);

    dim3 grid_t((group_size / run_block_size) + 1, 1, 1);
    switch(run_block_size){
        case 512:
            gpu_compute_moments<512> <<<grid_t, threads>>>(group_size, d_rtags, d_group_members, d_pos, reduction_data, box, d_image);
            gpu_reduce_moments<512> <<<grid_step_two, threads>>>(reduction_data,  collectives, (group_size / run_block_size) + 1);
            break;
        case 256:
            gpu_compute_moments<256> <<<grid_t, threads>>>(group_size, d_rtags, d_group_members, d_pos, reduction_data, box, d_image);
            gpu_reduce_moments<256> <<<grid_step_two, threads>>>(reduction_data, collectives, (group_size / run_block_size) + 1);
            break;
        case 128:
            gpu_compute_moments<128> <<<grid_t, threads>>>(group_size, d_rtags, d_group_members, d_pos, reduction_data, box, d_image);
            gpu_reduce_moments<128> <<<grid_step_two, threads>>>(reduction_data,  collectives, (group_size / run_block_size) + 1);
            break;
        case 64:
            gpu_compute_moments<64> <<<grid_t, threads>>>(group_size, d_rtags, d_group_members, d_pos, reduction_data, box, d_image);
            gpu_reduce_moments<64> <<<grid_step_two, threads>>>(reduction_data, collectives, (group_size / run_block_size) + 1);
            break;
        case 32:
            gpu_compute_moments<32> <<<grid_t, threads>>>(group_size, d_rtags, d_group_members, d_pos, reduction_data, box, d_image);
            gpu_reduce_moments<32> <<<grid_step_two, threads>>>(reduction_data, collectives, (group_size / run_block_size) + 1);
            break;
        default:
            throw std::runtime_error("Bad value of blocksize: " + std::to_string(run_block_size));
    }
    dim3 grid_z((N / run_block_size)+1,1,1);
    gpu_kurtosis_zero_forces_kernel<<<grid_z, threads>>>(d_force, d_virial, N, virial_pitch);

    dim3 grid_f((group_size / run_block_size) + 1, 1, 1);
    gpu_compute_kurtosis_force<<<grid_f,threads>>>(
            group_size,
            virial_pitch,
            d_rtags,
            d_group_members,
            d_pos,
            d_force,
            collectives,
            d_virial,
            params_mu2,
            params_mu4,
            box,
            d_image);

    return cudaSuccess;
}