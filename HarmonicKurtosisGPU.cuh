//
// Created by mgirard on 3/2/23.
//

#ifndef HOOMD_HARMONICKURTOSISGPU_CUH
#define HOOMD_HARMONICKURTOSISGPU_CUH
#include "hoomd/ParticleData.cuh"
#include "hoomd/HOOMDMath.h"
using namespace hoomd;
cudaError_t gpu_compute_kurtosis_forces(unsigned int group_size,
                                        unsigned int *d_group_members,
                                        unsigned int *d_rtags,
                                        Scalar4 *d_pos,
                                        int3* d_image,
                                        Scalar4 *d_force,
                                        Scalar *d_virial,
                                        unsigned int virial_pitch,
                                        Scalar4 *reduction_data,
                                        Scalar4 *collectives,
                                        const BoxDim box,
                                        unsigned int block_size,
                                        unsigned int N,
                                        Scalar2 params_mu2,
                                        Scalar2 params_mu4);

#endif //HOOMD_HARMONICKURTOSISGPU_CUH
