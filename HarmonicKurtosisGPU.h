//
// Created by mgirard on 3/2/23.
//

#ifndef HOOMD_HARMONICKURTOSISGPU_H
#define HOOMD_HARMONICKURTOSISGPU_H

#include "HarmonicKurtosis.h"
#include <hoomd/Autotuner.h>

class PYBIND11_EXPORT KurtosisForceComputeGPU : public KurtosisForceCompute{
public:
    KurtosisForceComputeGPU(std::shared_ptr<SystemDefinition> sysdef,
                            std::shared_ptr<ParticleGroup> group) : KurtosisForceCompute(sysdef, group){
        GPUArray<Scalar4> _red_data(m_pdata->getN() / 32 + 32, m_exec_conf);
        m_moment_reduction_data.swap(_red_data);

        std::vector<unsigned int> valid;
        m_tuner.reset(new Autotuner<1>({AutotunerBase::getTppListPow2(m_exec_conf)}, m_exec_conf, "KurtosisForceCompute"));
        this->m_autotuners.push_back(m_tuner);
    }


protected:
    virtual void computeForces(uint64_t timestep);
    GPUArray<Scalar4> m_moment_reduction_data;
    std::shared_ptr<Autotuner<1>> m_tuner;
};

void export_KurtosisForceComputeGPU(pybind11::module& m);

#endif //HOOMD_HARMONICKURTOSISGPU_H
