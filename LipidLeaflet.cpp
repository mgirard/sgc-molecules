/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "LipidLeaflet.h"

void LipidLeaflet::compute(uint64_t timestep) {
    m_cl->compute(timestep);
}

void export_lipidleaflet(py::module& m){
    py::class_<LipidLeaflet, std::shared_ptr<LipidLeaflet>>(m, "LipidLeaflet", py::base<CollectiveVariable<unsigned int>>())
    .def(py::init<std::shared_ptr<SystemDefinition>, std::shared_ptr<CellList>, std::shared_ptr<Molecules>>())
    .def("setCellListRCut", &LipidLeaflet::setCellListSize)
    .def("setOrientation", &LipidLeaflet::setBilayerOrientation);
}