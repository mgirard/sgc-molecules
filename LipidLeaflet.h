/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_LIPIDLEAFLET_H
#define HOOMD_LIPIDLEAFLET_H

#include <hoomd/CellList.h>
#include "CollectiveVariable.h"
#include "mcTypes.h"

using namespace hoomd;

class PYBIND11_EXPORT LipidLeaflet : public CollectiveVariable<unsigned int>{
public:
    LipidLeaflet(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<CellList> cl, std::shared_ptr<Molecules> molecules) : CollectiveVariable(sysdef, molecules), m_cl(cl){
        m_localMidplane.resize(m_pdata->getN());
        hashIdx = m_molecules->addCollectiveVariable(2, (Compute*) this); // the molecules of the bilayer have 2 states: top or bottom

        GPUArray<unsigned int> t_occ(0, m_exec_conf);
        m_cell_occupancy.swap(t_occ);

        GPUArray<Scalar> t_local_height(0, m_exec_conf);
        m_local_height.swap(t_local_height);

        GPUArray<Scalar> t_local_midplane(m_pdata->getN(), m_exec_conf);
        m_localMidplane.swap(t_local_midplane);

        m_cl->setComputeIdx(true);
    }

    virtual void compute(uint64_t timestep);

    void setBilayerOrientation(unsigned int dim){
        BilayerOrientation = static_cast<dimension>(dim);
    }

    GPUArray<Scalar>& getLocalMidplane(){return m_localMidplane;}

    void setCellListSize(Scalar size){
        m_cl->setNominalWidth(size);
    }

protected:
    std::shared_ptr<CellList> m_cl;
    GPUArray<unsigned int> m_cell_occupancy;
    GPUArray<Scalar> m_local_height;
    GPUArray<Scalar> m_localMidplane;
    unsigned int hashIdx = UINT32_MAX;
    dimension BilayerOrientation = dimension::Z;
};

void export_lipidleaflet(pybind11::module& m);

#endif //HOOMD_LIPIDLEAFLET_H
