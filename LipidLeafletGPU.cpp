/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "LipidLeafletGPU.h"
#include "LipidLeafletGPU.cuh"

void LipidLeafletGPU::compute(uint64_t timestep) {
    // update the cell list
    m_cl->compute(timestep);

    // make sure our grid is sufficiently large, leave some buffer room in case it later increases
    auto cellListIndexer = m_cl->getCellIndexer();
    auto cellIndexer = m_cl->getCellListIndexer();

    if(m_local_height.getNumElements() < cellListIndexer.getNumElements()) {
        m_local_height.resize(cellListIndexer.getNumElements() * 2);
        m_cell_occupancy.resize(cellListIndexer.getNumElements() * 2);
    }

    ArrayHandle<Scalar4> d_postype(m_pdata->getPositions(), access_location::device, access_mode::read);
    ArrayHandle<Scalar> d_local_height(m_local_height, access_location::device, access_mode::overwrite);
    ArrayHandle<unsigned int> d_occupancy(m_cell_occupancy, access_location::device, access_mode::overwrite);
    ArrayHandle<unsigned int> d_molecules(m_molecules->getMolecules(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_cellsize(m_cl->getCellSizeArray(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_cellIndex(m_cl->getIndexArray(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_collective(m_collective, access_location::device, access_mode::overwrite);
    ArrayHandle<Scalar> d_localMidplane(m_localMidplane, access_location::device, access_mode::overwrite);

    cudaComputeLocalHeightArgs localHeightArgs;
    localHeightArgs.d_postype = d_postype.data;
    localHeightArgs.cellListIndexer = cellListIndexer;
    localHeightArgs.cellIndexer = cellIndexer;
    localHeightArgs.dimension = static_cast<unsigned int>(BilayerOrientation);
    localHeightArgs.d_occupancy = d_occupancy.data;
    localHeightArgs.d_cell_height = d_local_height.data;
    localHeightArgs.d_molecule = d_molecules.data;
    localHeightArgs.d_cellsize = d_cellsize.data;
    localHeightArgs.d_cellIndex = d_cellIndex.data;

    m_tuner_local_z->begin();
    auto tuner_params_local_z = m_tuner_local_z->getParam();
    localHeightArgs.threadPerCell = tuner_params_local_z[0];
    localHeightArgs.blocksize = tuner_params_local_z[1];
    cudaComputeLocalHeight(localHeightArgs);
    m_tuner_local_z->end();

    if(m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();

    cudaReduceLocalHeightArgs reduceArgs;
    reduceArgs.d_height_sum = d_local_height.data;
    reduceArgs.d_occupancy = d_occupancy.data;
    reduceArgs.cellListIndexer = cellListIndexer;
    reduceArgs.dimension = static_cast<unsigned int>(BilayerOrientation);
    reduceArgs.box = m_pdata->getBox();
    m_tuner_reduce->begin();
    auto tuner_params_reduce = m_tuner_reduce->getParam();
    reduceArgs.threadPerLine = tuner_params_reduce[0];
    reduceArgs.blocksize = tuner_params_reduce[1];
    cudaReduceLocalHeight(reduceArgs);
    m_tuner_reduce->end();
    if(m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();

    //ArrayHandle<Scalar> d_radius(m_pdata->getDiameters(), access_location::device, access_mode::readwrite);
    cudaAssignHeightsArgs assignArgs;
    assignArgs.d_localMidplane = d_localMidplane.data;
    assignArgs.d_collective = d_collective.data;
    assignArgs.box = m_pdata->getBox();
    assignArgs.cellIndexer = cellIndexer;
    assignArgs.cellListIndexer = cellListIndexer;
    assignArgs.d_postype = d_postype.data;
    assignArgs.dimension = static_cast<unsigned int>(BilayerOrientation);
    assignArgs.d_averageHeight = d_local_height.data;
    assignArgs.N = m_pdata->getN();
    //assignArgs.d_radius = d_radius.data;

    m_tuner_assign->begin();
    assignArgs.blocksize = m_tuner_assign->getParam()[0];
    cudaAssignHeights(assignArgs);
    m_tuner_assign->end();


    cudaReduceMoleculesArgs leafletArgs;
    leafletArgs.d_molecules = d_molecules.data;
    leafletArgs.d_collective = d_collective.data;
    ArrayHandle<unsigned int> d_hashes(m_molecules->getHashes(), access_location::device, access_mode::readwrite);
    ArrayHandle<unsigned int> d_molecule_list(m_molecules->getMoleculeList(), access_location::device, access_mode::read);
    // DEBUG VALUE

    if(hashIdx != UINT32_MAX){
        auto hashConfiguration = m_molecules->getCVHash(hashIdx);
        leafletArgs.d_hash = d_hashes.data;
        leafletArgs.hash_offset = hashConfiguration.hashBitStart;
    }else{
        leafletArgs.d_hash = nullptr;
    }
    leafletArgs.moleculePitch = m_molecules->getMoleculeList().getPitch();
    leafletArgs.d_molecules = d_molecule_list.data;
    leafletArgs.maxMolSize = m_molecules->getMaxLength();
    leafletArgs.NMolecules = m_molecules->getN();
    // DEBUG VALUE
    //leafletArgs.d_radius = d_radius.data;
    m_tuner_leaflet->begin();
    leafletArgs.blocksize = m_tuner_leaflet->getParam()[0];
    cudaReduceMolecules(leafletArgs);
    m_tuner_leaflet->end();
    if(m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();
}

void export_lipidleafletGPU(py::module& m){
    py::class_<LipidLeafletGPU, std::shared_ptr<LipidLeafletGPU>>(m, "LipidLeafletGPU", py::base<LipidLeaflet>())
    .def(py::init<std::shared_ptr<SystemDefinition>, std::shared_ptr<CellList>, std::shared_ptr<Molecules>>());
}