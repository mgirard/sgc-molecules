/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */
#include "mcTypes.h"
#include "LipidLeafletGPU.cuh"
#include <hoomd/WarpTools.cuh>

using namespace hoomd;

template<dimension dim, unsigned int threadPerCell = 2>
__global__ void kernelCellHeight(const Scalar4* d_postype,
                                 const unsigned int* d_molecule,
                                 unsigned int* d_occupancy,
                                 const unsigned int* d_cellsize,
                                 const unsigned int* d_cellIndex,
                                 Scalar* d_height_sum,
                                 Index2D cellIndex,
                                 Index3D cellListIndex){
    unsigned int ncells = cellListIndex.getW() * cellListIndex.getH() * cellListIndex.getD();
    unsigned int myCell = (threadIdx.x + blockDim.x * blockIdx.x) / threadPerCell;

    if(myCell >= ncells)
        return;

    unsigned int n_cell_particles = d_cellsize[myCell];
    unsigned int n_valid_particles = 0;
    auto sum_position = Scalar(0.0);
    for(auto myParticle = threadIdx.x % threadPerCell; myParticle < n_cell_particles; myParticle+=threadPerCell){
        auto particleIdx = d_cellIndex[cellIndex(myParticle, myCell)];
        auto myPosition = d_postype[particleIdx];
        if(d_molecule[particleIdx] != UINT32_MAX){
            n_valid_particles++;
            switch (dim) {
                case dimension::X :
                    sum_position += myPosition.x;
                    break;
                case dimension::Y :
                    sum_position += myPosition.y;
                    break;
                case dimension::Z :
                    sum_position += myPosition.z;
                    break;
            }
        }
    }
    hoomd::detail::WarpReduce<unsigned int, threadPerCell> particleReduce;
    n_valid_particles = particleReduce.Sum(n_valid_particles);
    hoomd::detail::WarpReduce<Scalar, threadPerCell> positionReduce;
    sum_position = positionReduce.Sum(sum_position);
    if(threadIdx.x % threadPerCell == 0){
        d_occupancy[myCell] = n_valid_particles;
        d_height_sum[myCell] = sum_position;
    }
}
template<unsigned int tpc>
struct dispatchCellHeight{
        static void launch(const cudaComputeLocalHeightArgs& args, const unsigned int maxblocksize){
            if(tpc == args.threadPerCell){
                unsigned int blocksize = std::min(args.blocksize, maxblocksize);
                auto grid = args.cellListIndexer.getNumElements() * tpc / blocksize + 1;
                if(args.dimension == 0)
                    kernelCellHeight<dimension::X, tpc><<<grid, blocksize>>>(args.d_postype, args.d_molecule, args.d_occupancy, args.d_cellsize, args.d_cellIndex, args.d_cell_height, args.cellIndexer, args.cellListIndexer);
                if(args.dimension == 1)
                    kernelCellHeight<dimension::Y, tpc><<<grid, blocksize>>>(args.d_postype, args.d_molecule, args.d_occupancy, args.d_cellsize, args.d_cellIndex, args.d_cell_height, args.cellIndexer, args.cellListIndexer);
                if(args.dimension == 2)
                    kernelCellHeight<dimension::Z, tpc><<<grid, blocksize>>>(args.d_postype, args.d_molecule, args.d_occupancy, args.d_cellsize, args.d_cellIndex, args.d_cell_height, args.cellIndexer, args.cellListIndexer);

            }else{
                dispatchCellHeight<tpc/2>::launch(args, maxblocksize);
            }
        }
    };

template<>
struct dispatchCellHeight<0>{
    static void launch(const cudaComputeLocalHeightArgs& args, const unsigned int maxblocksize){

    }
};

cudaError_t cudaComputeLocalHeight(const cudaComputeLocalHeightArgs& args){
    static unsigned int maxblocksize = UINT32_MAX;
    if(maxblocksize == UINT32_MAX){
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, kernelCellHeight<dimension::Z, 32>);
        maxblocksize = attr.maxThreadsPerBlock;
        cudaFuncGetAttributes(&attr, kernelCellHeight<dimension::Z, 1>);
        maxblocksize = std::min(maxblocksize, (unsigned int) attr.maxThreadsPerBlock);
    }

    dispatchCellHeight<32>::launch(args, maxblocksize);
    return cudaSuccess;
}




template<dimension dim = dimension::Z, unsigned int threadPerLine = 2>
__global__ void kernelCellReduce(Scalar* d_height_sum,
                                 const unsigned int* d_occupancy,
                                 const Index3D cellListIndexer,
                                 const BoxDim box){
    unsigned int nInPlane;
    unsigned int nOutOfPlane;
    unsigned int myCell = (threadIdx.x + blockIdx.x * blockDim.x) / threadPerLine;
    unsigned int inPlaneFirst, inPlaneSecond;
    switch (dim) {
        case dimension::X :
            nInPlane = cellListIndexer.getD() * cellListIndexer.getH();
            nOutOfPlane = cellListIndexer.getW();
            inPlaneFirst = myCell % cellListIndexer.getH();
            inPlaneSecond = myCell / cellListIndexer.getH();
            break;
        case dimension::Y :
            nInPlane = cellListIndexer.getW() * cellListIndexer.getD();
            nOutOfPlane = cellListIndexer.getH();
            inPlaneFirst = myCell % cellListIndexer.getW();
            inPlaneSecond = myCell / cellListIndexer.getW();
            break;
        case dimension::Z :
            nInPlane = cellListIndexer.getW() * cellListIndexer.getH();
            nOutOfPlane = cellListIndexer.getD();
            inPlaneFirst = myCell % cellListIndexer.getW();
            inPlaneSecond = myCell / cellListIndexer.getW();
            break;
    }

    if(myCell >= nInPlane)
        return;

    // scan cells to find min & max value (periodic bc detection)
    auto maxValue = Scalar(NAN);
    auto minValue = Scalar(NAN);
    for(auto outOfPlane = threadIdx.x % threadPerLine; outOfPlane < nOutOfPlane; outOfPlane += threadPerLine){
        unsigned int myLinearCell;
        switch (dim) {
            case dimension::X :
                myLinearCell = cellListIndexer(outOfPlane, inPlaneFirst, inPlaneSecond);
                break;
            case dimension::Y :
                myLinearCell = cellListIndexer(inPlaneFirst, outOfPlane, inPlaneSecond);
                break;
            case dimension::Z :
                myLinearCell = cellListIndexer(inPlaneFirst, inPlaneSecond, outOfPlane);
                break;
        }
        maxValue = max(maxValue, d_height_sum[myLinearCell] / d_occupancy[myLinearCell]);
        minValue = min(minValue, d_height_sum[myLinearCell] / d_occupancy[myLinearCell]);
    }


    hoomd::detail::WarpReduce<Scalar, threadPerLine> positionReduce;
    //minValue = positionReduce.Reduce(minValue, cub::Min());
    //maxValue = positionReduce.Reduce(maxValue, cub::Max());
    Scalar temp(NAN);
    for(auto offset = threadPerLine / 2; offset >0; offset /= 2){
        temp = __shfl_down_sync(0xFFFFFFFF, minValue, offset, threadPerLine);
        minValue = min(minValue, temp);
        temp = __shfl_down_sync(0xFFFFFFFF, maxValue, offset, threadPerLine);
        maxValue = max(maxValue, temp);
    }

    minValue = __shfl_sync(0xFFFFFFFF, minValue, 0, threadPerLine);
    maxValue = __shfl_sync(0xFFFFFFFF, maxValue, 0, threadPerLine);

    bool pbcCrossing;
    auto lengths = box.getL();
    auto pbcFractionDifferential = nOutOfPlane > 3 ? Scalar(nOutOfPlane - 3.0) / Scalar(nOutOfPlane) : Scalar(0.0);
    switch (dim) {
        case dimension::X :
            pbcCrossing = (maxValue - minValue) > lengths.x * pbcFractionDifferential;
            break;
        case dimension::Y :
            pbcCrossing = (maxValue - minValue) > lengths.y * pbcFractionDifferential;
            break;
        case dimension::Z :
            pbcCrossing = (maxValue - minValue) > lengths.z * pbcFractionDifferential;
            break;
    }

    auto averageHeight = Scalar(0.0);
    auto nBeads = 0u;
    for(auto outOfPlane = threadIdx.x % threadPerLine; outOfPlane < nOutOfPlane; outOfPlane += threadPerLine){
        unsigned int myLinearCell;
        switch (dim) {
            case dimension::X :
                myLinearCell = cellListIndexer(outOfPlane, inPlaneFirst, inPlaneSecond);
                break;
            case dimension::Y :
                myLinearCell = cellListIndexer(inPlaneFirst, outOfPlane, inPlaneSecond);
                break;
            case dimension::Z :
                myLinearCell = cellListIndexer(inPlaneFirst, inPlaneSecond, outOfPlane);
                break;
        }
        auto cellHeight = d_height_sum[myLinearCell];
        nBeads += d_occupancy[myLinearCell];
        if(pbcCrossing && cellHeight < 0.0) {
            switch (dim) {
                case dimension::X :
                    cellHeight += lengths.x * d_occupancy[myLinearCell];
                    break;
                case dimension::Y :
                    cellHeight += lengths.y * d_occupancy[myLinearCell];
                    break;
                case dimension::Z :
                    cellHeight += lengths.z * d_occupancy[myLinearCell];
                    break;
            }
        }
        averageHeight += cellHeight;
    }

    averageHeight = positionReduce.Reduce(averageHeight, cub::Sum());
    hoomd::detail::WarpReduce<unsigned int, threadPerLine> occupancyReduce;
    nBeads = occupancyReduce.Reduce(nBeads, cub::Sum());
    if(threadIdx.x % threadPerLine == 0){
        auto bilayerHeight = averageHeight / static_cast<Scalar>(nBeads);
        unsigned int myLinearCell;
        switch (dim) {
            case dimension::X :
                myLinearCell = cellListIndexer(0, inPlaneFirst, inPlaneSecond);
                break;
            case dimension::Y :
                myLinearCell = cellListIndexer(inPlaneFirst, 0, inPlaneSecond);
                break;
            case dimension::Z :
                myLinearCell = cellListIndexer(inPlaneFirst, inPlaneSecond, 0);
                break;
        }
        d_height_sum[myLinearCell] = bilayerHeight;
    }
}

template<unsigned int tpl>
struct dispatchCellReduce{
    static void launch(const cudaReduceLocalHeightArgs& args, const unsigned int maxblocksize){
        if(args.threadPerLine == tpl){
            unsigned int blocksize = std::min(maxblocksize, args.blocksize);
            if(args.dimension == 0) {
                auto grid = (tpl * args.cellListIndexer.getH() * args.cellListIndexer.getD()) / blocksize + 1;
                kernelCellReduce<dimension::X, tpl><<<grid, blocksize>>>(args.d_height_sum, args.d_occupancy, args.cellListIndexer, args.box);
            }
            if(args.dimension == 1) {
                auto grid = (tpl * args.cellListIndexer.getW() * args.cellListIndexer.getD()) / blocksize + 1;
                kernelCellReduce<dimension::Y, tpl><<<grid, blocksize>>>(args.d_height_sum, args.d_occupancy, args.cellListIndexer, args.box);
            }
            if(args.dimension == 2) {
                auto grid = (tpl * args.cellListIndexer.getH() * args.cellListIndexer.getW()) / blocksize + 1;
                kernelCellReduce<dimension::Z, tpl><<<grid, blocksize>>>(args.d_height_sum, args.d_occupancy, args.cellListIndexer, args.box);
            }
        }else{
            dispatchCellReduce<tpl/2>::launch(args, maxblocksize);
        }
    }
};

template<>
struct dispatchCellReduce<0>{
    static void launch(const cudaReduceLocalHeightArgs& args, const unsigned int maxblocksize){}
};

cudaError_t cudaReduceLocalHeight(const cudaReduceLocalHeightArgs& args){
    static unsigned int maxblocksize = UINT32_MAX;
    if(maxblocksize == UINT32_MAX){
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, kernelCellReduce<dimension::X, 1>);
        maxblocksize = attr.maxThreadsPerBlock;
        cudaFuncGetAttributes(&attr, kernelCellReduce<dimension::X, 32>);
        maxblocksize = std::min(maxblocksize, (unsigned int) attr.maxThreadsPerBlock);
    }
    dispatchCellReduce<32>::launch(args, maxblocksize);
    return cudaSuccess;

}

template<dimension dim = dimension::Z>
__global__ void kernelAssignValues(Scalar* d_averageHeight,
                                   const Scalar4* d_postype,
                                   unsigned int* d_collective,
                                   Scalar* d_localMidplane,
                                   const Index3D cellListIndexer,
                                   const Index2D cellIndexer,
                                   const BoxDim box,
                                   const unsigned int N
                                   //const unsigned int *d_hashes
                                   ){
    unsigned int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if(idx >= N)
        return;

    Scalar4 myPosition = d_postype[idx];
    // compute the bin index

    Scalar3 p = make_scalar3(myPosition.x, myPosition.y, myPosition.z);

    Scalar3 f = box.makeFraction(p);
    uint3 dims = make_uint3(cellListIndexer.getW(), cellListIndexer.getH(), cellListIndexer.getD());

    int ib = (int)(f.x * dims.x);
    int jb = (int)(f.y * dims.y);
    int kb = (int)(f.z * dims.z);

    // if the particle is slightly outside, move back into grid
    if (ib < 0) ib = 0;
    if (ib >= dims.x) ib = dims.x - 1;

    if (jb < 0) jb = 0;
    if (jb >= dims.y) jb = dims.y - 1;

    if (kb < 0) kb = 0;
    if (kb >= dims.z) kb = dims.z - 1;
    unsigned int myBox;
    switch (dim) {
        case dimension::X :
            myBox = cellListIndexer(0, jb, kb);
            break;
        case dimension::Y :
            myBox = cellListIndexer(ib, 0, kb);
            break;
        case dimension::Z :
            myBox = cellListIndexer(ib, jb, 0);
            break;
    }
    auto myAverageCoordinate = d_averageHeight[myBox];
    d_localMidplane[idx] = myAverageCoordinate;
    Scalar heightFromMidplane;
    switch (dim) {
        case dimension::X :
            heightFromMidplane = box.minImage(make_scalar3(p.x - myAverageCoordinate, 0.0, 0.0)).x;
            break;
        case dimension::Y :
            heightFromMidplane = box.minImage(make_scalar3(0.0, p.y - myAverageCoordinate, 0.0)).y;
            break;
        case dimension::Z :
            heightFromMidplane = box.minImage(make_scalar3(0.0, 0.0, p.z - myAverageCoordinate)).z;
            break;
    }
    d_collective[idx] = __scalar_as_int(heightFromMidplane);
}

cudaError_t cudaAssignHeights(const cudaAssignHeightsArgs& args){
    static unsigned int maxblocksize = UINT32_MAX;
    if(maxblocksize == UINT32_MAX){
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, kernelAssignValues<dimension::X>);
        maxblocksize = attr.maxThreadsPerBlock;
    }

    unsigned int blocksize = std::min(args.blocksize, maxblocksize);
    auto grid = args.N / blocksize + 1;
    if(args.dimension == 0){
        kernelAssignValues<dimension::X><<<grid, blocksize>>>(args.d_averageHeight, args.d_postype, args.d_collective, args.d_localMidplane, args.cellListIndexer, args.cellIndexer, args.box, args.N); //, args.d_radius);
    }
    if(args.dimension == 1){
        kernelAssignValues<dimension::Y><<<grid, blocksize>>>(args.d_averageHeight, args.d_postype, args.d_collective, args.d_localMidplane, args.cellListIndexer, args.cellIndexer, args.box, args.N); //, args.d_radius);
    }
    if(args.dimension == 2){
        kernelAssignValues<dimension::Z><<<grid, blocksize>>>(args.d_averageHeight, args.d_postype, args.d_collective, args.d_localMidplane, args.cellListIndexer, args.cellIndexer, args.box, args.N); //, args.d_radius);
    }
    return cudaSuccess;
}

__global__ void kernelReduceMolecules(unsigned int *d_collective,
                                      const unsigned int *d_molecules,
                                      unsigned int *d_hash,
                                      const unsigned int hash_offset,
                                      const unsigned int moleculePitch,
                                      const unsigned int NMolecules,
                                      const unsigned int maxMolSize){
    const unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(idx >= NMolecules)
        return;

    unsigned int atom = 0;
    auto averageCV = Scalar(0.0);
    while(d_molecules[atom + moleculePitch * idx] != UINT32_MAX && atom < maxMolSize){
        auto myAtom = d_molecules[atom + moleculePitch * idx];
        auto myCV = d_collective[myAtom];
        //d_radius[myAtom] = __int_as_scalar(myCV);
        averageCV += __int_as_scalar(myCV);
        atom++;
    }
    unsigned int CV = averageCV > 0.0 ? 1 : 0;

    atom = 0;
    while(d_molecules[atom + moleculePitch * idx] != UINT32_MAX && atom < maxMolSize){
        auto myAtom = d_molecules[atom + moleculePitch * idx];
        d_collective[myAtom] = CV;
        //d_radius[myAtom] = static_cast<Scalar>(CV);
        atom++;
    }

    // if we need to compute hashes
    if(d_hash){
        auto hash = d_hash[idx];
        auto mask = ~(1u << hash_offset);
        auto new_hash = (hash & mask) | (CV << hash_offset);
        d_hash[idx] = new_hash;
    }
}


cudaError_t cudaReduceMolecules(const cudaReduceMoleculesArgs& args){
    static unsigned int maxblocksize = UINT32_MAX;
    if(maxblocksize == UINT32_MAX){
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, kernelReduceMolecules);
        maxblocksize = attr.maxThreadsPerBlock;
    }
    unsigned int blocksize = std::min(maxblocksize, args.blocksize);
    auto grid = args.NMolecules / blocksize + 1;

    kernelReduceMolecules<<<grid, blocksize>>>(
            args.d_collective,
            args.d_molecules,
            args.d_hash,
            args.hash_offset,
            args.moleculePitch,
            args.NMolecules,
            args.maxMolSize);//,
            //args.d_radius); // RADIUS IS DEBUG
    return cudaSuccess;
}














