/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifdef __CLION_IDE__
#include "../ParticleData.cuh"
#include "../HOOMDMath.h"
#include "../GPUPartition.cuh"
#include "../CellList.h"
#endif

#ifndef HOOMD_LIPIDLEAFLETGPU_CUH
#define HOOMD_LIPIDLEAFLETGPU_CUH


#include <hoomd/ParticleData.cuh>
#include <hoomd/HOOMDMath.h>
#include <hoomd/GPUPartition.cuh>
#include <hoomd/Index1D.h>
using namespace hoomd;

struct cudaComputeLocalHeightArgs{
    Scalar4* d_postype;
    unsigned int* d_molecule;
    unsigned int* d_occupancy;
    unsigned int* d_cellsize;
    unsigned int* d_cellIndex;
    Scalar* d_cell_height;
    Index3D cellListIndexer;
    Index2D cellIndexer;
    unsigned int threadPerCell;
    unsigned int blocksize;
    unsigned int dimension = 2;
};

cudaError_t cudaComputeLocalHeight(const cudaComputeLocalHeightArgs& args);

struct cudaReduceLocalHeightArgs{
    Scalar* d_height_sum;
    unsigned int *d_occupancy;
    Index3D cellListIndexer;
    BoxDim box;

    unsigned int threadPerLine;
    unsigned int blocksize;
    unsigned int dimension = 2;
};

cudaError_t cudaReduceLocalHeight(const cudaReduceLocalHeightArgs& args);

struct cudaAssignHeightsArgs{
    Scalar* d_averageHeight;
    Scalar4* d_postype;
    Scalar* d_localMidplane;
    unsigned int* d_collective;
    Index3D cellListIndexer;
    Index2D cellIndexer;
    BoxDim box;
    unsigned int N;
    unsigned int blocksize;
    unsigned int dimension = 2;
    // DEBUG
    Scalar* d_radius;
};

cudaError_t cudaAssignHeights(const cudaAssignHeightsArgs& args);

struct cudaReduceMoleculesArgs{
    unsigned int *d_collective;
    unsigned int *d_molecules;
    unsigned int *d_hash;
    unsigned int hash_offset;
    unsigned int moleculePitch;
    unsigned int NMolecules;
    unsigned int maxMolSize;
    unsigned int blocksize;
    //DEBUG VALUE
   // Scalar* d_radius;
};

cudaError_t cudaReduceMolecules(const cudaReduceMoleculesArgs& args);

#endif //HOOMD_LIPIDLEAFLETGPU_CUH
