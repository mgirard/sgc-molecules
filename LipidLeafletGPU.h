/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_LIPIDLEAFLETGPU_H
#define HOOMD_LIPIDLEAFLETGPU_H
#include "LipidLeaflet.h"
#include <hoomd/Autotuner.h>


class PYBIND11_EXPORT LipidLeafletGPU : public LipidLeaflet{
public:
    LipidLeafletGPU(std::shared_ptr<SystemDefinition> sysdef, std::shared_ptr<CellList> cl, std::shared_ptr<Molecules> molecules) : LipidLeaflet(sysdef, cl, molecules){

        m_tuner_local_z.reset(new Autotuner<2>({AutotunerBase::getTppListPow2(m_exec_conf), AutotunerBase::makeBlockSizeRange(m_exec_conf)}, m_exec_conf, "TunerLipidLeaflet_local_z"));
        m_tuner_reduce.reset(new Autotuner<2>({AutotunerBase::getTppListPow2(m_exec_conf), AutotunerBase::makeBlockSizeRange(m_exec_conf)}, m_exec_conf, "TunerLipidLeaflet_reduce"));
        m_tuner_assign.reset(new Autotuner<1>({AutotunerBase::makeBlockSizeRange(m_exec_conf)}, m_exec_conf, "TunerLipidLeaflet_assign"));
        m_tuner_leaflet.reset(new Autotuner<1>({AutotunerBase::makeBlockSizeRange(m_exec_conf)}, m_exec_conf, "TunerLipidLeaflet_leaflet"));
        this->m_autotuners.insert(this->m_autotuners.end(), {m_tuner_local_z, m_tuner_reduce, m_tuner_assign, m_tuner_leaflet});
    }

    virtual void compute(uint64_t timestep);

protected:

    std::shared_ptr<Autotuner<2>> m_tuner_local_z;
    std::shared_ptr<Autotuner<2>> m_tuner_reduce;
    std::shared_ptr<Autotuner<1>> m_tuner_assign;
    std::shared_ptr<Autotuner<1>> m_tuner_leaflet;
};

void export_lipidleafletGPU(pybind11::module& m);

#endif //HOOMD_LIPIDLEAFLETGPU_H
