/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

#include <hoomd/md/CosineSqAngleForceCompute.h>

#ifndef HOOMD_MCANGLECOSINESQ_H
#define HOOMD_MCANGLECOSINESQ_H

using namespace hoomd;
class PYBIND11_EXPORT CosineSquaredAngleComputeMC : public hoomd::md::CosineSqAngleForceCompute{
public:
    explicit CosineSquaredAngleComputeMC(std::shared_ptr<SystemDefinition> sysdef) : CosineSqAngleForceCompute(sysdef){}

protected:
    void computeForces(uint64_t timestep) override;
};

//! Exports the AngleForceCompute class to python
void export_CosineSqAngleForceComputeMC(pybind11::module& m);

#endif //HOOMD_MCANGLECOSINESQ_H
