/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

#include "MCAngleCosineSqGPU.h"
#include "MCAngleCosineSqGPU.cuh"

namespace py = pybind11;
using namespace hoomd;
using namespace hoomd::md;

void CosineSquaredAngleComputeMCGPU::computeForces(uint64_t timestep) {
    // start the profile
    // the angle table is up to date: we are good to go. Call the kernel
    hoomd::ArrayHandle<hoomd::Scalar4> d_pos(m_pdata->getPositions(), hoomd::access_location::device, hoomd::access_mode::read);

    hoomd::BoxDim box = m_pdata->getGlobalBox();

    hoomd::ArrayHandle<hoomd::Scalar4> d_force(m_force,hoomd::access_location::device,hoomd::access_mode::overwrite);
    hoomd::ArrayHandle<hoomd::Scalar> d_virial(m_virial,hoomd::access_location::device,hoomd::access_mode::overwrite);
    hoomd::ArrayHandle<hoomd::Scalar2> d_params(m_params, hoomd::access_location::device, hoomd::access_mode::read);

    hoomd::ArrayHandle<hoomd::AngleData::members_t> d_gpu_anglelist(m_angle_data->getGPUTable(), hoomd::access_location::device,hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> d_gpu_angle_pos_list(m_angle_data->getGPUPosTable(), hoomd::access_location::device,hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> d_gpu_n_angles(m_angle_data->getNGroupsArray(), hoomd::access_location::device, hoomd::access_mode::read);

    // run the kernel on the GPU
    m_tuner->begin();
    auto param = m_tuner->getParam();
    gpuMC_compute_cosinesq_angle_forces(d_force.data,
                                      d_virial.data,
                                      m_virial.getPitch(),
                                      m_pdata->getN(),
                                      d_pos.data,
                                      box,
                                      d_gpu_anglelist.data,
                                      d_gpu_angle_pos_list.data,
                                      m_angle_data->getGPUTableIndexer().getW(),
                                      d_gpu_n_angles.data,
                                      d_params.data,
                                      m_angle_data->getNTypes(),
                                      param[0]);

    if(m_exec_conf->isCUDAErrorCheckingEnabled())
    CHECK_CUDA_ERROR();
    m_tuner->end();
}

void export_CosineSqAngleForceComputeMCGPU(pybind11::module& m){
    py::class_<CosineSquaredAngleComputeMCGPU, hoomd::md::CosineSqAngleForceComputeGPU, std::shared_ptr<CosineSquaredAngleComputeMCGPU> >(m, "CosineSquaredAngleForceComputeMCGPU")
            .def(py::init< std::shared_ptr<hoomd::SystemDefinition> >())
            ;
}