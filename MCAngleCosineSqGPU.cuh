/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

#include <hoomd/BondedGroupData.cuh>
#include <hoomd/ParticleData.cuh>
#include <hoomd/HOOMDMath.h>

#ifndef HOOMD_MCANGLECOSINESQGPU_CUH
#define HOOMD_MCANGLECOSINESQGPU_CUH
using namespace hoomd;
cudaError_t gpuMC_compute_cosinesq_angle_forces(Scalar4* d_force,
                                              Scalar* d_virial,
                                              const unsigned int virial_pitch,
                                              const unsigned int N,
                                              const Scalar4 *d_pos,
                                              const BoxDim& box,
                                              const group_storage<3> *atable,
                                              const unsigned int *apos_list,
                                              const unsigned int pitch,
                                              const unsigned int *n_angles_list,
                                              Scalar2 *d_params,
                                              unsigned int n_angle_types,
                                              int block_size);
#endif //HOOMD_MCANGLECOSINESQGPU_CUH
