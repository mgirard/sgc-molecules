/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */
#include <hoomd/md/CosineSqAngleForceComputeGPU.h>

#ifndef HOOMD_MCANGLECOSINESQGPU_H
#define HOOMD_MCANGLECOSINESQGPU_H

class PYBIND11_EXPORT CosineSquaredAngleComputeMCGPU : public hoomd::md::CosineSqAngleForceComputeGPU{
public:
    explicit CosineSquaredAngleComputeMCGPU(std::shared_ptr<hoomd::SystemDefinition> sysdef) : CosineSqAngleForceComputeGPU(sysdef) {}

protected:
    void computeForces(uint64_t timestep) override;
};

void export_CosineSqAngleForceComputeMCGPU(pybind11::module& m);

#endif //HOOMD_MCANGLECOSINESQGPU_H
