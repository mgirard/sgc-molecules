/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "Molecules.h"
namespace py=pybind11;
void updateMoleculeList(std::vector<std::vector<unsigned int>>& molecules, std::vector<unsigned int>& tagLookup, const std::pair<unsigned int, unsigned int> newPair){
    // this takes a new pair of two bonded components, and updates molecule list and molecule lookups
    auto idx_i = newPair.first;
    auto idx_j = newPair.second;

    if(tagLookup[idx_i] == UINT32_MAX && tagLookup[idx_j] == UINT32_MAX){
        auto molecule_idx = static_cast<unsigned int>(molecules.size());
        std::vector<unsigned int> molecule = {idx_i, idx_j};
        molecules.push_back(molecule);
        tagLookup[idx_i] = tagLookup[idx_j] = molecule_idx;
    }
    else if(tagLookup[idx_i] == UINT32_MAX){
        molecules[tagLookup[idx_j]].push_back(idx_i);
        tagLookup[idx_i] = tagLookup[idx_j];
    }
    else if(tagLookup[idx_j] == UINT32_MAX){
        molecules[tagLookup[idx_i]].push_back(idx_j);
        tagLookup[idx_j] = tagLookup[idx_i];
    }
    else if (tagLookup[idx_i] != tagLookup[idx_j]){
        if(tagLookup[idx_i] < tagLookup[idx_j])
            std::swap(idx_i, idx_j);
        auto molecule_i = tagLookup[idx_i];
        auto molecule_j = tagLookup[idx_j];
        for(auto&& tag_j : molecules[molecule_j]) {
            molecules[molecule_i].push_back(tag_j);
            tagLookup[tag_j] = molecule_i;
        }
        molecules.erase(molecules.begin() + molecule_j);
    }
}

Molecules::Molecules(std::shared_ptr<hoomd::SystemDefinition> sysdef,
                     std::shared_ptr<hoomd::ParticleGroup> group) :
      Compute(sysdef), m_group(group){

    hoomd::GPUArray<unsigned int> t_m_molecule(m_pdata->getN(), m_exec_conf);
    m_molecule.swap(t_m_molecule);
    hoomd::GPUArray<unsigned int> t_m_molecule_alt(m_pdata->getN(), m_exec_conf);
    m_molecule_alt.swap(t_m_molecule_alt);

    hoomd::GPUArray<unsigned int> t_beadPosition(m_pdata->getN(), m_exec_conf);
    m_beadPosition.swap(t_beadPosition);
    hoomd::ArrayHandle<unsigned int> h_beadPosition(m_beadPosition, hoomd::access_location::host, hoomd::access_mode::overwrite);
    std::fill(h_beadPosition.data, h_beadPosition.data + m_pdata->getN(), UINT32_MAX);

    hoomd::GPUArray<unsigned int> t_beadPosition_alt(m_pdata->getN(), m_exec_conf);
    m_beadPosition_alt.swap(t_beadPosition_alt);
    hoomd::GPUArray<unsigned int> t_beadState(m_pdata->getN(), m_exec_conf);
    m_beadState.swap(t_beadState);

    hoomd::GPUArray<unsigned int> t_beadState_alt(m_pdata->getN(), m_exec_conf);
    m_beadState_alt.swap(t_beadState_alt);

    hoomd::GPUArray<unsigned int> t_swapGroup(m_pdata->getN(), m_exec_conf);
    m_swapGroup.swap(t_swapGroup);
    hoomd::GPUArray<unsigned int> t_swapGroup_alt(m_pdata->getN(), m_exec_conf);
    m_swapGroup_alt.swap(t_swapGroup_alt);

    hoomd::ArrayHandle<unsigned int> h_swapGroup(m_swapGroup, hoomd::access_location::host, hoomd::access_mode::overwrite);
    std::fill(h_swapGroup.data, h_swapGroup.data + m_pdata->getN(), UINT32_MAX);

    hoomd::GPUArray<unsigned int> t_sort_order(m_pdata->getN(), m_exec_conf);
    m_current_sort_order.swap(t_sort_order);
    m_pdata->getParticleSortSignal().connect<Molecules, &Molecules::setDirty>(this);
    hoomd::ArrayHandle<unsigned int> RTags(m_pdata->getRTags(), hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> h_current_sort_order(m_current_sort_order, hoomd::access_location::host, hoomd::access_mode::overwrite);
    for(auto i = 0u; i < m_pdata->getN(); i++){
        h_current_sort_order.data[i] = RTags.data[i];
    }

    // we need to build a molecular picture of all particles in group that are connected by bonds
    std::vector<std::vector<unsigned int>> molecules;
    std::vector<unsigned int> tag_to_mol(m_pdata->getN());
    std::fill(tag_to_mol.begin(), tag_to_mol.end(), UINT32_MAX);
    auto bonds = sysdef->getBondData();
    for(auto i = 0u; i < bonds->getNGlobal(); i++){
        auto bond = bonds->getMembersByIndex(i);
        updateMoleculeList(molecules, tag_to_mol, std::make_pair(bond.tag[0], bond.tag[1]));
    }
    auto constraints = sysdef->getConstraintData();
    for(auto i = 0u; i < constraints->getNGlobal(); i++){
        auto cons = constraints->getMembersByIndex(i);
        updateMoleculeList(molecules, tag_to_mol, std::make_pair(cons.tag[0], cons.tag[1]));
    }

    auto it = molecules.begin();
    while(it != molecules.end()){
        bool valid = false;
        for(auto&& tag : *it){
            valid |= m_group->isMember(RTags.data[tag]);
        }
        if(valid)
            ++it;
        else
            molecules.erase(it);
    }

    m_molecule_length = std::accumulate(molecules.begin(), molecules.end(), 0, [&](std::size_t prev, const std::vector<unsigned int>& molB){return std::max(prev, molB.size());});

    hoomd::GPUArray<unsigned int> t_mlist(m_molecule_length, molecules.size(), m_exec_conf);
    m_molecule_list.swap(t_mlist);

    hoomd::GPUArray<unsigned int> t_msortedlist(m_molecule_length, molecules.size(), m_exec_conf);
    m_sorted_molecule_list.swap(t_msortedlist);
    //GPUArray<unsigned int> t_msortedlist_alt(m_molecule_length, molecules.size(), m_exec_conf);
    //m_sorted_molecule_list_alt.swap(t_msortedlist_alt);
    hoomd::ArrayHandle<unsigned int> h_molecule_list(m_molecule_list, hoomd::access_location::host, hoomd::access_mode::overwrite);
    hoomd::ArrayHandle<unsigned int> h_sorted_molecule_list(m_sorted_molecule_list, hoomd::access_location::host, hoomd::access_mode::overwrite);
    for(auto i = 0u; i < molecules.size(); i++){
        for(auto j = 0u; j < m_molecule_list.getPitch(); j++) {
            h_molecule_list.data[i * m_molecule_list.getPitch() + j] = j < molecules[i].size() ? molecules[i][j] : UINT32_MAX;
            h_sorted_molecule_list.data[i * m_molecule_list.getPitch() + j] = j < molecules[i].size() ? RTags.data[molecules[i][j]] : UINT32_MAX;
        }
    }


    // molecular-wise data
    m_N_molecules = molecules.size();
    hoomd::GPUArray<unsigned int> t_swapHashes(molecules.size(), m_exec_conf);
    m_swapHashes.swap(t_swapHashes);
    hoomd::GPUArray<unsigned int> t_moleculeLock(molecules.size(), m_exec_conf);
    m_moleculeLock.swap(t_moleculeLock);
    hoomd::GPUArray<unsigned int> t_moleculeSize(molecules.size(), m_exec_conf);
    m_moleculeSize.swap(t_moleculeSize);

    hoomd::ArrayHandle<unsigned int> h_moleculeSize(m_moleculeSize, hoomd::access_location::host, hoomd::access_mode::overwrite);
    // set arrays
    hoomd::ArrayHandle<unsigned int> h_molecule(m_molecule, hoomd::access_location::host, hoomd::access_mode::overwrite);
    std::fill(h_molecule.data, h_molecule.data + m_pdata->getN(), UINT32_MAX);
    for(auto i = 0u; i < molecules.size(); i++){
        auto& mol = molecules[i];
        h_moleculeSize.data[i] = mol.size();
        for(auto&& tag : mol){
            auto idx = RTags.data[tag];
            h_molecule.data[idx] = i;
        }
    }
    hoomd::GPUArray<hoomd::Scalar> t_molecule_swap_potential(1, m_exec_conf);
    m_molecule_swap_potential.swap(t_molecule_swap_potential);
}

unsigned int Molecules::addSwapGroup(const std::shared_ptr<hoomd::ParticleGroup>& group, unsigned int nstate, const hoomd::GPUArray<unsigned int>* const tagMapping, extraMappingType mappingType) {
    unsigned int currentSwapGroupId = m_swapGroups.size();
    swapGroup currentSwapGroup(m_exec_conf);

    unsigned int groupNBits = std::ceil(std::log(nstate) / std::log(2));

    // find the largest amount of beads in a single molecule of this swap group
    hoomd::ArrayHandle<unsigned int> h_RTags(m_pdata->getRTags(), hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> h_swapGroup(m_swapGroup, hoomd::access_location::host, hoomd::access_mode::readwrite);

    for(auto tag = 0u; tag < m_pdata->getN(); tag++){
        auto idx = h_RTags.data[tag];
        if(group->isMember(idx)){ // tag i is part of this swap group
            if(h_swapGroup.data[idx] != UINT32_MAX)
                throw std::runtime_error("adding a swap group for a swappable bead");
            h_swapGroup.data[idx] = currentSwapGroupId;
        }
    }

    hoomd::ArrayHandle<unsigned int> h_molecule_list(m_molecule_list, hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> h_beadPosition(m_beadPosition, hoomd::access_location::host, hoomd::access_mode::readwrite);
    auto maxBeads = 0;
    for(auto mol = 0u; mol < m_N_molecules; mol++){
        auto currentMolBeads = 0;
        for(auto bead = 0u; bead < m_molecule_length; bead++){
            auto myTag = h_molecule_list.data[mol * m_molecule_list.getPitch() + bead];
            if(myTag != UINT32_MAX && group->isMember(h_RTags.data[myTag])) {
                h_beadPosition.data[h_RTags.data[myTag]] = currentHashBits + currentMolBeads * groupNBits;
                currentMolBeads++;
            }
        }
        maxBeads = std::max(maxBeads, currentMolBeads);
    }
    currentSwapGroup.maxBeads = maxBeads;
    currentSwapGroup.hashStart = currentHashBits;
    currentSwapGroup.beadHashSize = groupNBits;
    currentHashBits += groupNBits * maxBeads;
    resizeHashes();

    if(tagMapping) {
        currentSwapGroup.has_extra_mapping = true;
        currentSwapGroup.mappingType = mappingType;
        currentSwapGroup.mapping.resize(m_pdata->getN());
        currentSwapGroup.mapping_alt.resize(m_pdata->getN());
        hoomd::ArrayHandle<unsigned int> h_extraMapping(currentSwapGroup.mapping, hoomd::access_location::host, hoomd::access_mode::overwrite);
        hoomd::ArrayHandle<unsigned int> h_argMapping(*tagMapping, hoomd::access_location::host, hoomd::access_mode::read);
        for(auto tag = 0u; tag < m_pdata->getN(); tag++){
            auto idx = h_RTags.data[tag];
            h_extraMapping.data[idx] = h_argMapping.data[tag];
        }
    }
    m_swapGroups.push_back(std::move(currentSwapGroup));
    return currentSwapGroupId;
}

void Molecules::rebuildIndexes() {
    hoomd::ArrayHandle<unsigned int> h_tags(m_pdata->getTags(), hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::ArrayHandle<unsigned int> newOrder(m_pdata->getRTags(), hoomd::access_location::host, hoomd::access_mode::read);

    hoomd::ArrayHandle<unsigned int> h_sorted_molecule_list(m_sorted_molecule_list, hoomd::access_location::host, hoomd::access_mode::readwrite);
    hoomd::ArrayHandle<unsigned int> h_unsrt_molecule_list(m_molecule_list, hoomd::access_location::host, hoomd::access_mode::read);
    //ArrayHandle<unsigned int> h_sorted_molecule_list_alt(m_sorted_molecule_list_alt, access_location::host, access_mode::overwrite);
    {
        hoomd::ArrayHandle<unsigned int> h_molecule(m_molecule, hoomd::access_location::host, hoomd::access_mode::read);
        hoomd::ArrayHandle<unsigned int> h_molecule_alt(m_molecule_alt, hoomd::access_location::host, hoomd::access_mode::overwrite);

        hoomd::ArrayHandle<unsigned int> h_beadState(m_beadState, hoomd::access_location::host, hoomd::access_mode::read);
        hoomd::ArrayHandle<unsigned int> h_beadState_alt(m_beadState_alt, hoomd::access_location::host, hoomd::access_mode::overwrite);

        hoomd::ArrayHandle<unsigned int> h_beadPosition(m_beadPosition, hoomd::access_location::host, hoomd::access_mode::read);
        hoomd::ArrayHandle<unsigned int> h_beadPosition_alt(m_beadPosition_alt, hoomd::access_location::host, hoomd::access_mode::overwrite);

        hoomd::ArrayHandle<unsigned int> h_swapGroup(m_swapGroup, hoomd::access_location::host, hoomd::access_mode::read);
        hoomd::ArrayHandle<unsigned int> h_swapGroup_alt(m_swapGroup_alt, hoomd::access_location::host, hoomd::access_mode::overwrite);

        hoomd::ArrayHandle<unsigned int> oldOrder(m_current_sort_order, hoomd::access_location::host, hoomd::access_mode::read);
        for(auto i = 0u; i < m_pdata->getN(); i++){
            h_molecule_alt.data[newOrder.data[i]] = h_molecule.data[oldOrder.data[i]];
            h_beadState_alt.data[newOrder.data[i]] = h_beadState.data[oldOrder.data[i]];
            h_beadPosition_alt.data[newOrder.data[i]] = h_beadPosition.data[oldOrder.data[i]];
            h_swapGroup_alt.data[newOrder.data[i]] = h_swapGroup.data[oldOrder.data[i]];
            //h_sorted_molecule_list_alt.data[newOrder.data[i]] = h_sorted_molecule_list.data[oldOrder.data[i]]; // invalid read / writes of size 4
            for(auto&& group : m_swapGroups){
                if(group.has_extra_mapping) {
                    hoomd::ArrayHandle<unsigned int> h_mapping_alt(group.mapping_alt, hoomd::access_location::host, hoomd::access_mode::readwrite);
                    hoomd::ArrayHandle<unsigned int> h_mapping(group.mapping, hoomd::access_location::host, hoomd::access_mode::read);
                    h_mapping_alt.data[newOrder.data[i]] = h_mapping.data[oldOrder.data[i]];
                }
            }
        }
        for(auto mol = 0u; mol < m_N_molecules; mol++){
            for(auto atom = 0u; atom < m_sorted_molecule_list.getPitch(); atom++){
                if(h_sorted_molecule_list.data[mol * m_sorted_molecule_list.getPitch() + atom] == UINT32_MAX)
                    continue;
                auto idx = mol * m_sorted_molecule_list.getPitch() + atom;
                h_sorted_molecule_list.data[idx] = newOrder.data[h_unsrt_molecule_list.data[idx]];
            }
        }
    }

    m_molecule.swap(m_molecule_alt);
    m_beadState.swap(m_beadState_alt);
    m_beadPosition.swap(m_beadPosition_alt);
    m_swapGroup.swap(m_swapGroup_alt);
    //m_sorted_molecule_list.swap(m_sorted_molecule_list_alt);

    for(auto&& group : m_swapGroups)
        if(group.has_extra_mapping)
            group.mapping.swap(group.mapping_alt);
    // copy the new order into this
    {
    hoomd::ArrayHandle<unsigned int> currentOrdering(m_current_sort_order, hoomd::access_location::host, hoomd::access_mode::overwrite);
        for(auto i = 0u; i < m_pdata->getN(); i++){
            currentOrdering.data[i] = newOrder.data[i];
        }
    }
}

void Molecules::releaseLock() {
    hoomd::ArrayHandle<unsigned int> h_lock(m_moleculeLock, hoomd::access_location::host, hoomd::access_mode::overwrite);
    for(auto i = 0u; i < m_N_molecules; i++){
        h_lock.data[i] = 0;
    }
}

void Molecules::compute(uint64_t timestep) {
    for(auto&& c : m_CVHashes)
        if(c.compute)
            c.compute->compute(timestep);
}

unsigned int Molecules::addCollectiveVariable(unsigned int nstate, Compute* ptr) {
    collectiveVariableHash hashInfo;
    unsigned int CVIdx = m_CVHashes.size();
    unsigned int nbits = std::ceil(std::log(nstate) / std::log(2));
    hashInfo.hashBits = nbits;
    hashInfo.hashBitStart = currentHashBits;
    currentHashBits += nbits;
    if(ptr)
        hashInfo.compute = ptr;
    else
        hashInfo.compute = nullptr;
    m_CVHashes.push_back(hashInfo);

    resizeHashes();
    return CVIdx;
}

hoomd::Scalar Molecules::computePotentialPopulationProduct() {
    hoomd::ArrayHandle<unsigned int> h_moltypes(m_swapHashes, hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::ArrayHandle<hoomd::Scalar> h_potential(m_molecule_swap_potential, hoomd::access_location::host, hoomd::access_mode::read);
    hoomd::Scalar product = 0.0;
    for(auto i = 0u; i < m_N_molecules; i++)
        product += h_potential.data[h_moltypes.data[i]];
    return product;
}

void Molecules::connectGSDStateSignal(std::shared_ptr<hoomd::GSDDumpWriter> writer) {
    return;
}

int Molecules::slotWriteGSDState(gsd_handle &handle) {
    return 0;
}

void export_Molecules(pybind11::module& m){
    py::class_<Molecules, hoomd::Compute, std::shared_ptr<Molecules> >(m, "Molecules")
            .def(py::init<std::shared_ptr<hoomd::SystemDefinition>, std::shared_ptr<hoomd::ParticleGroup>>())
            .def("set_mu", &Molecules::setChemicalPotential)
            .def("getPotentialPopulationProduct", &Molecules::computePotentialPopulationProduct)
            .def("connectGSDStateSignal", &Molecules::connectGSDStateSignal);
}





