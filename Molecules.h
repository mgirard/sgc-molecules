/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include <hoomd/Compute.h>
#include <hoomd/ParticleGroup.h>
#include <hoomd/GSDDumpWriter.h>

#ifndef HOOMD_MOLECULES_H
#define HOOMD_MOLECULES_H

enum class extraMappingType{angles = 0};

struct swapGroup{
    bool has_extra_mapping = false;
    unsigned int hashStart = 0;
    unsigned int beadHashSize = 0;
    unsigned int maxBeads = 0;
    extraMappingType mappingType = extraMappingType::angles;
    hoomd::GPUArray<unsigned int> mapping;
    hoomd::GPUArray<unsigned int> mapping_alt;

    explicit swapGroup(const std::shared_ptr<const hoomd::ExecutionConfiguration>& exec) : mapping(0, exec), mapping_alt(0, exec){
    }

    swapGroup(swapGroup&) = delete;
    swapGroup(swapGroup&& other) noexcept :
    has_extra_mapping(other.has_extra_mapping),
    hashStart(other.hashStart),
    beadHashSize(other.beadHashSize),
    maxBeads(other.maxBeads),
    mappingType(other.mappingType){
        mapping.swap(other.mapping);
        mapping_alt.swap(other.mapping_alt);
    }
};

struct collectiveVariableHash{
    unsigned int hashBitStart = 0;
    unsigned int hashBits = 0;
    hoomd::Compute* compute;
};

class Molecules : public hoomd::Compute {
public:
    Molecules(std::shared_ptr<hoomd::SystemDefinition> sysdef, std::shared_ptr<hoomd::ParticleGroup> group);

    unsigned int addSwapGroup(const std::shared_ptr<hoomd::ParticleGroup>& group, unsigned int nstate, const hoomd::GPUArray<unsigned int>* const tagMapping = nullptr, extraMappingType mappingType = extraMappingType::angles);

    unsigned int addCollectiveVariable(unsigned int nstate, Compute* ptr = nullptr);

    const swapGroup& getSwapGroup(unsigned int groupIdx){checkRebuild(); return m_swapGroups[groupIdx];}

    collectiveVariableHash getCVHash(unsigned int CVIdx){checkRebuild(); return m_CVHashes[CVIdx];}

    virtual void releaseLock();

    hoomd::GPUArray<unsigned int>& getHashes() {checkRebuild(); return m_swapHashes;}

    hoomd::GPUArray<unsigned int>& getLock(){checkRebuild(); return m_moleculeLock;}

    hoomd::GPUArray<unsigned int>& getMolecules(){checkRebuild(); return m_molecule;}

    hoomd::GPUArray<unsigned int>& getHashStart(){checkRebuild(); return m_beadPosition;}

    hoomd::GPUArray<unsigned int>& getBeadStates(){checkRebuild(); return m_beadState;}

    hoomd::GPUArray<unsigned int>& getSwapGroups(){checkRebuild(); return m_swapGroup;}

    hoomd::GPUArray<unsigned int>& getMoleculeList(){checkRebuild(); return m_sorted_molecule_list;}

    hoomd::GPUArray<hoomd::Scalar>& getPotentials(){checkRebuild(); return m_molecule_swap_potential;}

    unsigned int getN() const {return m_N_molecules;}

    unsigned int getMaxLength() const {return m_molecule_length;}

    void setDirty(){m_dirty = true;}

    void setChemicalPotential(unsigned int hash, hoomd::Scalar value){
        if(hash >= m_molecule_swap_potential.getNumElements())
            throw std::runtime_error("Hash value supplied: " + std::to_string(hash) + " is smaller than maximum number of hashes: " + std::to_string((1u << (currentHashBits+1)) - 1));
        hoomd::ArrayHandle<hoomd::Scalar> h_hashes(m_molecule_swap_potential, hoomd::access_location::host, hoomd::access_mode::readwrite);
        h_hashes.data[hash] = value;
    }

    hoomd::Scalar computePotentialPopulationProduct();

    void virtual compute(uint64_t timestep);

    void connectGSDStateSignal(std::shared_ptr<hoomd::GSDDumpWriter> writer);

    int slotWriteGSDState(gsd_handle& handle);

protected:
    std::shared_ptr<hoomd::ParticleGroup> m_group;

    bool m_dirty = true;
    hoomd::GPUArray<unsigned int> m_molecule_list;
    hoomd::GPUArray<unsigned int> m_sorted_molecule_list;
    unsigned int m_molecule_length;
    unsigned int m_N_molecules;

    void checkRebuild(){
        if(m_dirty)
            rebuildIndexes();
        m_dirty = false;
    }

    void resizeHashes(){
        m_molecule_swap_potential.resize(1u << currentHashBits);
        hoomd::ArrayHandle<hoomd::Scalar> h_swapPotential(m_molecule_swap_potential, hoomd::access_location::host, hoomd::access_mode::overwrite);
        std::fill(h_swapPotential.data, h_swapPotential.data + m_molecule_swap_potential.getNumElements(), -INFINITY);
    }

    virtual void rebuildIndexes();

    hoomd::GPUArray<hoomd::Scalar> m_molecule_swap_potential; //! chemical potentials associated with available molecules

    unsigned int currentHashBits = 0; //! how many bits are used by the hash
    hoomd::GPUArray<unsigned int> m_swapHashes; //! list of molecular hashes
    hoomd::GPUArray<unsigned int> m_moleculeLock; //! 0 if molecule isn't touched by any thread, 1 otherwise. Use this with atomicCAS operations
    hoomd::GPUArray<unsigned int> m_moleculeSize;

    hoomd::GPUArray<unsigned int> m_molecule; //! to which molecule the bead belongs to
    hoomd::GPUArray<unsigned int> m_beadPosition; //! position along the molecule for hash calculation, this is the start bit of this bead in the hash
    hoomd::GPUArray<unsigned int> m_beadState; //! current bead state
    hoomd::GPUArray<unsigned int> m_swapGroup; //! to which swap group a particle belongs

    hoomd::GPUArray<unsigned int> m_molecule_alt; //! to which molecule the bead belongs to
    hoomd::GPUArray<unsigned int> m_beadPosition_alt; //! position along the molecule for hash calculation, this is the start bit of this bead in the hash
    hoomd::GPUArray<unsigned int> m_beadState_alt; //! current bead state
    hoomd::GPUArray<unsigned int> m_swapGroup_alt; //! to which swap group a particle belongs

    hoomd::GPUArray<unsigned int> m_current_sort_order;
    std::vector<swapGroup> m_swapGroups;
    std::vector<collectiveVariableHash> m_CVHashes;
};

void export_Molecules(pybind11::module&);

#endif //HOOMD_MOLECULES_H
