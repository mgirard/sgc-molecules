/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

#ifndef __POTENTIAL_PAIR_MC_H__
#define __POTENTIAL_PAIR_MC_H__

#include <hoomd/md/PotentialPair.h>

#include <iostream>
#include <stdexcept>
#include <memory>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "hoomd/HOOMDMath.h"
#include "hoomd/Index1D.h"
#include "hoomd/GlobalArray.h"
#include "hoomd/ForceCompute.h"
#include <hoomd/md/NeighborList.h>


#ifdef ENABLE_MPI
#include "hoomd/Communicator.h"
#endif


/*! \file PotentialPairMC.h
    \brief Defines the template class for standard pair potentials
    \details The heart of the code that computes pair potentials is in this file.
    \note This header cannot be compiled by nvcc
*/

#ifdef __HIPCC__
#error This header cannot be compiled by nvcc
#endif

//! Template class for computing pair potentials
/*! <b>Overview:</b>
    PotentialPairMC computes standard pair potentials (and forces) between all particle pairs in the simulation. It
    employs the use of a neighbor list to limit the number of computations done to only those particles with the
    cutoff radius of each other. The computation of the actual V(r) is not performed directly by this class, but
    by an evaluator class (e.g. EvaluatorPairLJ) which is passed in as a template parameter so the computations
    are performed as efficiently as possible.

    PotentialPairMC handles most of the gory internal details common to all standard pair potentials.
     - A cutoff radius to be specified per particle type pair
     - The energy can be globally shifted to 0 at the cutoff
     - XPLOR switching can be enabled
     - Per type pair parameters are stored and a set method is provided
     - Logging methods are provided for the energy
     - And all the details about looping through the particles, computing dr, computing the virial, etc. are handled

    A note on the design of XPLOR switching:
    We need to be able to handle smooth XPLOR switching in systems of mixed LJ/WCA particles. There are three modes to
    enable all of the various use-cases:
     - Mode 1: No shifting. All pair potentials are computed as is and not shifted to 0 at the cutoff.
     - Mode 2: Shift everything. All pair potentials (no matter what type pair) are shifted so they are 0 at the cutoff
     - Mode 3: XPLOR switching enabled. A r_on value is specified per type pair. When r_on is less than r_cut, normal
       XPLOR switching will be applied to the unshifted potential. When r_on is greater than r_cut, the energy will
       be shifted. In this manner, a valid r_on value can be given for the LJ interactions and r_on > r_cut can be set
       for WCA (which will then be shifted).

    XPLOR switching gets significantly more complicated for all pair potentials when shifted potentials are used. Thus,
    the combination of XPLOR switching + shifted potentials will not be supported to avoid slowing down the calculation
    for everyone.

    <b>Implementation details</b>

    rcutsq, ronsq, and the params are stored per particle type pair. It wastes a little bit of space, but benchmarks
    show that storing the symmetric type pairs and indexing with Index2D is faster than not storing redundant pairs
    and indexing with Index2DUpperTriangular. All of these values are stored in GlobalArray
    for easy access on the GPU by a derived class. The type of the parameters is defined by \a param_type in the
    potential evaluator class passed in. See the appropriate documentation for the evaluator for the definition of each
    element of the parameters.

    For profiling and logging, PotentialPairMC needs to know the name of the potential. For now, that will be queried from
    the evaluator. Perhaps in the future we could allow users to change that so multiple pair potentials could be logged
    independently.

    \sa export_PotentialPairMC()
*/

template < class evaluator >
class PYBIND11_EXPORT PotentialPairMC  : public hoomd::md::PotentialPair<evaluator> //ForceCompute
    {
    public:
        //! Param type from evaluator
        typedef typename evaluator::param_type param_type;

        //! Construct the pair potential
        PotentialPairMC(std::shared_ptr<hoomd::SystemDefinition> sysdef,
                      std::shared_ptr<hoomd::md::NeighborList> nlist, unsigned int type_override);
        //! Destructor
        virtual ~PotentialPairMC();

        virtual void computeForces(uint64_t);

        protected:

        unsigned int m_type_override;
    };

/*! \param sysdef System to compute forces on
    \param nlist Neighborlist to use for computing the forces
    \param log_suffix Name given to this instance of the force
*/
template < class evaluator >
PotentialPairMC< evaluator >::PotentialPairMC(std::shared_ptr<hoomd::SystemDefinition> sysdef,
                                                std::shared_ptr<hoomd::md::NeighborList> nlist, unsigned int type_override)
    : hoomd::md::PotentialPair<evaluator>(sysdef, nlist), m_type_override(type_override)
    {
    }

template< class evaluator >
PotentialPairMC< evaluator >::~PotentialPairMC()
    {
    }

template< class evaluator >
void PotentialPairMC< evaluator >::computeForces(uint64_t timestep)
    {
        throw std::runtime_error("This is only implemented on GPU");
    }


//! Export this pair potential to python
/*! \param name Name of the class in the exported python module
    \tparam T Class type to export. \b Must be an instantiated PotentialPairMC class template.
*/;
template < class T > void export_PotentialPairMC(pybind11::module& m, const std::string& name)
    {
    pybind11::class_<PotentialPairMC<T>, hoomd::md::PotentialPair<T>, std::shared_ptr<PotentialPairMC<T>> > PotentialPairMC(m, name.c_str());
    PotentialPairMC.def(pybind11::init< std::shared_ptr<hoomd::SystemDefinition>, std::shared_ptr<hoomd::md::NeighborList>, unsigned int>());
    }


#endif // __POTENTIAL_PAIR_MC_H__
