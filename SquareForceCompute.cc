// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander


#include "SquareForceCompute.h"

namespace py = pybind11;

using namespace std;

/*! \file SquareForceCompute.cc
    \brief Contains code for the SquareForceCompute class
*/


/*! \param sysdef SystemDefinition containing the ParticleData to compute forces on
    \param group_one A group of particles
    \param z0 Center of the potential well in z
    \param k Strength of the potential (units of energy / length^2)
*/
SquareForceCompute::SquareForceCompute(std::shared_ptr<SystemDefinition> sysdef,
                                       unsigned int reference,
                                       std::shared_ptr<ParticleGroup> group_two,
                                       Scalar z0,
                                       Scalar k)
        : ForceCompute(sysdef), m_reference(reference), m_group_two(group_two), m_z0(z0), m_k(k)
    {
        m_exec_conf->msg->notice(5) << "Constructing SquareForceCompute" << endl;
    }

SquareForceCompute::~SquareForceCompute()
    {
        m_exec_conf->msg->notice(5) << "Destroying SquareForceCompute" << endl;
    }


/*! This function calls rearrangeForces() whenever the particles have been sorted
    \param timestep Current timestep
*/
void SquareForceCompute::computeForces(uint64_t timestep)
    {
        // set torques to zero and forget them
        ArrayHandle<Scalar4> h_torque(m_torque, access_location::host, access_mode::overwrite);
        memset(h_torque.data, 0, sizeof(Scalar4) * m_force.getNumElements());

    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_force(m_force, access_location::host, access_mode::overwrite);
    ArrayHandle<unsigned int> h_rtag(m_pdata->getRTags(), access_location::host, access_mode::read);

    const BoxDim& Box = m_pdata->getGlobalBox();

    assert(h_pos.data != NULL);
    assert(h_force.data != NULL);

    memset(h_force.data, 0, sizeof(Scalar4) * m_force.getNumElements());

    }


void export_SquareForceCompute(py::module& m)
    {
    py::class_< SquareForceCompute, std::shared_ptr<SquareForceCompute> >(m,"SquareForce",py::base<ForceCompute>())
    .def(py::init< std::shared_ptr<SystemDefinition>, unsigned int, std::shared_ptr<ParticleGroup>, Scalar, Scalar >())
    .def("setParameters", &SquareForceCompute::setForce)
        .def("setClamp", &SquareForceCompute::setClamp)
        .def("getPartialZ", &SquareForceCompute::getPartialDerivativeZ0)
        .def("getPartialK", &SquareForceCompute::getPartialDerivativeK)
        .def("getCollective", &SquareForceCompute::getCollective)
    ;
    }
