// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander

#include "hoomd/ForceCompute.h"
#include "hoomd/ParticleGroup.h"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <memory>
#include <map>

/*! \file SquareForceCompute.h
    \brief Declares a class for computing constant forces
*/

#ifdef NVCC
#error This header cannot be compiled by nvcc
#endif

#include <pybind11/pybind11.h>

#ifndef __SQFORCECOMPUTE_H__
#define __SQFORCECOMPUTE_H__

//! Adds a constant force to a number of particles
/*! \ingroup computes
*/
using namespace hoomd;

class PYBIND11_EXPORT SquareForceCompute : public ForceCompute
    {
    public:
        //! Constructs the compute
        SquareForceCompute(std::shared_ptr<SystemDefinition> sysdef,
                       unsigned int ref, std::shared_ptr<ParticleGroup> group_two,
                       Scalar z0, Scalar k);

        //! Destructor
        virtual ~SquareForceCompute();

        //! Set the force to a new value
        void setForce(Scalar z0, Scalar k)
        {
            m_z0 = z0;
            m_k = k;
        }

        void setClamp(Scalar rint, Scalar rext){
            if(rint > rext)
                throw std::runtime_error("Setting inner clamping radius to be larger than outer");
            m_rint = rint;
            m_rext = rext;
            }

    Scalar getPartialDerivativeZ0(){
        ArrayHandle<Scalar> h_collective(m_reduced, access_location::host, access_mode::read);
        //ArrayHandle<Scalar4> h_postype(m_pdata->getPositions(), access_location::host, access_mode::read);
        return m_k * (m_z0 - (m_pdata->getPosition(m_reference).z - h_collective.data[0]));
        }

    Scalar getPartialDerivativeK(){
        auto energy = calcEnergySum();
        return energy * 2.0 / m_k;
        }

    Scalar getCollective(){
        ArrayHandle<Scalar> h_collective(m_reduced, access_location::host, access_mode::read);
        return h_collective.data[0];
        }

    protected:
        //! Actually compute the forces
        virtual void computeForces(uint64_t timestep);

        //! Group of particles to apply force to
        unsigned int m_reference;
        std::shared_ptr<ParticleGroup> m_group_two;

        GPUArray<Scalar> m_block_data;
        GPUArray<Scalar> m_bblock_data;
        GPUArray<Scalar> m_partial_normalization;
        GPUArray<Scalar> m_partial_normalization_derivative;
        GPUArray<Scalar> m_reduced; //!< [Sum(fi), Sum(fi * zi), Sum(fj), Sum(fj*zj)]

        Scalar m_z0; //!< Spring constant
        Scalar m_k;
        Scalar m_rint;
        Scalar m_rext;
};

//! Exports the SquareForceComputeClass to python
void export_SquareForceCompute(pybind11::module& m);

#endif
