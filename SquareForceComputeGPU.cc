// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander


#include "SquareForceComputeGPU.h"
#include "SquareForceComputeGPU.cuh"


namespace py = pybind11;
using namespace std;

/*! \file SquareForceComputeGPU.cc
    \brief Contains code for the SquareForceComputeGPU class
*/


/*! \param sysdef SystemDefinition containing the ParticleData to compute forces on
    \param group A group of particles
    \param z0 Center of the potential well in z
    \param k Strength of the potential (units of energy / length^2)
*/
SquareForceComputeGPU::SquareForceComputeGPU(std::shared_ptr<SystemDefinition> sysdef,
                                             unsigned int reference,
                                             std::shared_ptr<ParticleGroup> group_two,
                                             Scalar z0,
                                             Scalar k)
        : SquareForceCompute(sysdef, reference, group_two, z0, k) {
    if (!m_exec_conf->isCUDAEnabled()) {
        m_exec_conf->msg->error() << "Creating a SquareForceComputeGPU with no GPU in the execution configuration"
                                  << endl;
        throw std::runtime_error("Error initializing SquareForceComputeGPU");
    }
    m_exec_conf->msg->notice(5) << "Constructing SquareForceComputeGPU" << endl;
    m_tuner.reset(new Autotuner<1>({{32, 64, 128, 256, 512, 1024}}, this->m_exec_conf, "SquareForceCompute"));

    GPUArray<Scalar> reduction_data(m_pdata->getNGlobal() / 32 + 32, m_exec_conf);
    m_block_data.swap(reduction_data);

    GPUArray<Scalar> rreduction_data(m_pdata->getNGlobal() / 32 + 32, m_exec_conf);
    m_bblock_data.swap(rreduction_data);

    GPUArray<Scalar> normalization(m_pdata->getNGlobal(), m_exec_conf);
    m_partial_normalization.swap(normalization);

    GPUArray<Scalar> dnormalization(m_pdata->getNGlobal(), m_exec_conf);
    m_partial_normalization_derivative.swap(dnormalization);

    GPUArray<Scalar> reduced_data(6, m_exec_conf);
    m_reduced.swap(reduced_data);
}

SquareForceComputeGPU::~SquareForceComputeGPU() {
    m_exec_conf->msg->notice(5) << "Destroying SquareForceComputeGPU" << endl;
}


void SquareForceComputeGPU::computeForces(uint64_t timestep) {

    //unsigned int group_one_size = m_group_one->getNumMembers();
    unsigned int group_two_size = m_group_two->getNumMembers();
    {
        ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::device, access_mode::read);
        ArrayHandle<Scalar4> h_for(m_force, access_location::device, access_mode::overwrite);
        ArrayHandle<Scalar> h_vir(m_virial, access_location::device, access_mode::overwrite);
        ArrayHandle<unsigned int> h_rtag(m_pdata->getRTags(), access_location::device, access_mode::read);
        BoxDim box = m_pdata->getBox();

        //ArrayHandle<unsigned int> d_group_members_one(m_group_one->getIndexArray(), access_location::device, access_mode::read);
        ArrayHandle<unsigned int> d_group_members_two(m_group_two->getIndexArray(), access_location::device, access_mode::read);
        ArrayHandle<Scalar> d_collective(m_reduced, access_location::device, access_mode::readwrite);
        ArrayHandle<Scalar> d_norm(m_partial_normalization, access_location::device, access_mode::readwrite);
        ArrayHandle<Scalar> d_normd(m_partial_normalization_derivative, access_location::device, access_mode::readwrite);

        ArrayHandle<int3> d_image(m_pdata->getImages(), access_location::device, access_mode::read);

        m_tuner->begin();
        gpu_compute_squareforces(m_reference,
                                 group_two_size,
                                 d_group_members_two.data,
                                 h_rtag.data,
                                 h_pos.data,
                                 d_image.data,
                                 h_for.data,
                                 h_vir.data,
                                 m_virial.getPitch(),
                                 d_norm.data,
                                 d_normd.data,
                                 d_collective.data,
                                 box,
                                 m_tuner->getParam()[0],
                                 m_pdata->getN(),
                                 make_scalar4(m_k, m_z0, m_rint, m_rext));

        if (m_exec_conf->isCUDAErrorCheckingEnabled())
                CHECK_CUDA_ERROR();
        m_tuner->end();
    }
}


void export_SquareForceComputeGPU(py::module &m) {
    py::class_<SquareForceComputeGPU, std::shared_ptr<SquareForceComputeGPU> >(m, "SquareForceGPU",
                                                                               py::base<SquareForceCompute>())
            .def(py::init<std::shared_ptr<SystemDefinition>, unsigned int, std::shared_ptr<ParticleGroup>, Scalar, Scalar>())
            .def("setParameters", &SquareForceCompute::setForce);
}
