// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

// Maintainer: joaander

#include "hoomd/HOOMDMath.h"
#include "hoomd/ParticleData.cuh"
#include <cub/block/block_reduce.cuh>
// #include "LangevinForceCompute.h"
#include "SquareForceComputeGPU.cuh"
#include <stdio.h>

using namespace hoomd;

struct clamp_data
    {
    Scalar val, derivative;
    };

__host__ __device__ clamp_data inline clamp(const Scalar r,
                                            const Scalar rint = Scalar(1.5),
                                            const Scalar rext = Scalar(2.0))
    {
    clamp_data ret;
    Scalar clamp = r < rint ? 0.0 : r > rext ? 1.0 : (r - rint) / (rext - rint);
    ret.val = Scalar(1.0) - clamp * clamp * (Scalar(3.0) - 2 * clamp);
    ret.derivative = -6.0 * (clamp - clamp * clamp) / (rext - rint);
    return ret;
    }

__global__ void gpu_compute_squareforces_kernel(const unsigned int* d_group_two_members,
                                                const unsigned int* d_rtags,
                                                const unsigned int reference,
                                                const unsigned int group_size_two,
                                                const Scalar4* d_pos,
                                                Scalar4* d_force,
                                                Scalar* d_virial,
                                                const Scalar* normalization_factors,
                                                const Scalar* normalization_derivatives,
                                                const unsigned int virial_pitch,
                                                const Scalar* reduced_data,
                                                const BoxDim box,
                                                const Scalar4 params)
    {
    unsigned int group_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (group_idx > (group_size_two))
        {
        return;
        }
    bool is_reference = group_idx == 0;
    const unsigned int* ptr = d_group_two_members;
    unsigned int offset = is_reference ? 0 : 1;
    unsigned int idx = is_reference ? d_rtags[reference] : ptr[group_idx - offset];

    Scalar4 ppos = d_pos[idx];
    Scalar4 ref = d_pos[d_rtags[reference]];

    // extract in-plane position
    Scalar3 dr = {ppos.x - ref.x, ppos.y - ref.y, ppos.z - ref.z};
    dr = box.minImage(dr);
    Scalar xy_norm = sqrt(dr.x * dr.x + dr.y * dr.y);


    // compute collectives
    Scalar zbar = reduced_data[0];
    Scalar weight = reduced_data[1];

    // just return if we have weight of 0, user should log collective of nan
    if(weight == 0)
        return;

    Scalar dz = box.minImage(make_scalar3(0., 0., ref.z - zbar)).z;
    Scalar partial_x = reduced_data[2];
    Scalar partial_y = reduced_data[3];
    Scalar partial_xz = reduced_data[4];
    Scalar partial_yz = reduced_data[5];

    // params.x is our force constant, and params.y equilibrium of the bond 1/2 k ((zref - <z>) - z0)^2
    Scalar z = (dz - params.y) * params.x;

    // we generate a force in (x,y) since our particle number depends on (x,y) coordinates
    Scalar4 F;
    if(is_reference){
        F = make_scalar4(z * (zbar * partial_x - partial_xz) / weight, z * (zbar * partial_y - partial_yz), -z, Scalar(0.5) * z * (dz - params.y));
        }else{
        Scalar grad_xy = z * normalization_derivatives[idx] * (-zbar + ppos.z) / weight;
        Scalar ixy_norm = xy_norm > 0.0 ? 1.0 / xy_norm : 1.0;
        F = make_scalar4(grad_xy * dr.x * ixy_norm,
                         grad_xy * dr.y * ixy_norm,
                             z * normalization_factors[idx] / weight,
                             0.0);
        }
    d_force[idx] = F;
    d_virial[virial_pitch * 0 + idx] = dr.x * F.x;
    d_virial[virial_pitch * 1 + idx] = Scalar(0.5) * (dr.y * F.x + dr.x * F.y);
    d_virial[virial_pitch * 2 + idx] = Scalar(0.5) * (dr.x * F.z + dr.z * F.x);
    d_virial[virial_pitch * 3 + idx] = dr.y * F.y;
    d_virial[virial_pitch * 4 + idx] = Scalar(0.5) * (dr.y * F.z + dr.z * F.y);
    d_virial[virial_pitch * 5 + idx] = dr.z * F.z;
    }

template<int blockSize>
__global__ void gpu_compute_collective_step_one(const unsigned int group_size,
                                                const unsigned int reference,
                                                const unsigned int* d_rtags,
                                                const unsigned int* d_group_members,
                                                const Scalar4* d_pos,
                                                Scalar* collectives,
                                                Scalar* normalization_factors,
                                                Scalar* normalization_derivatives,
                                                const BoxDim box,
                                                const Scalar4 params)
    {
    // use one block for the calculation
    unsigned int i = threadIdx.x; // one thread per particle

    // accumulators
    Scalar xi(0.0), zeta(0.0), thread_weight(0.0);
    Scalar thread_partial_x(0.0), thread_partial_y(0.0), thread_partial_xz(0.0), thread_partial_yz(0.0);

    const Scalar4 postype_reference = d_pos[d_rtags[reference]];
    Scalar max_z = box.getL().z;

    while(i < group_size) {
        const unsigned idx = d_group_members[i];
        Scalar4 postype = d_pos[idx];

        // compute in-plane position
        Scalar3 xy = {postype.x - postype_reference.x, postype.y - postype_reference.y, 0.0};
        xy = box.minImage(xy);
        Scalar r = fast::sqrt(xy.x * xy.x + xy.y * xy.y);

        // compute clamping
        clamp_data clamping_values = clamp(r, params.z, params.w);
        Scalar weight = clamping_values.val;
        Scalar weight_derivative = clamping_values.derivative;

        normalization_factors[idx] = weight;
        normalization_derivatives[idx] = weight_derivative;

        // compute mean-z position by COM using PBC; this formula is based on z in (0, max), so we are shifted by pi
        Scalar theta = 2.0 * postype.z / max_z;
        Scalar sin, cos;
        sincospi(theta, &sin, &cos);

        xi += cos * weight;
        zeta += sin * weight;
        thread_weight += weight;

        // force calculation for reference involves the total partial derivatives, mask  r = 0 which would produce nans
        if(r > 0.0)
            {
            thread_partial_x += xy.x * weight_derivative / r;
            thread_partial_y += xy.y * weight_derivative / r;
            thread_partial_xz += xy.x * weight_derivative * postype.z / r;
            thread_partial_yz += xy.y * weight_derivative * postype.z / r;
            }
        i += blockSize;
    }

    __syncthreads();
    typedef cub::BlockReduce<Scalar, blockSize> BlockReduce;
    __shared__ typename BlockReduce::TempStorage temp_storage;

    Scalar total_weight = BlockReduce(temp_storage).Sum(thread_weight);
    __syncthreads();
    Scalar total_xi = BlockReduce(temp_storage).Sum(xi);
    __syncthreads();
    Scalar total_zeta = BlockReduce(temp_storage).Sum(zeta);
    __syncthreads();
    Scalar total_partial_x = BlockReduce(temp_storage).Sum(thread_partial_x);
    __syncthreads();
    Scalar total_partial_y = BlockReduce(temp_storage).Sum(thread_partial_y);
    __syncthreads();
    Scalar total_partial_xz = BlockReduce(temp_storage).Sum(thread_partial_xz);
    __syncthreads();
    Scalar total_partial_yz = BlockReduce(temp_storage).Sum(thread_partial_yz);

    // compute COM on thread0, and write back
    if (threadIdx.x == 0)
        {
        // based on comment above, we are shifted by pi, so we need to invert results
        Scalar bar_theta = atan2(total_zeta / total_weight, total_xi / total_weight); //+ M_PI;
        Scalar zcom = bar_theta * max_z / (2 * M_PI);
        collectives[0] = zcom;
        collectives[1] = total_weight;
        collectives[2] = total_partial_x;
        collectives[3] = total_partial_y;
        collectives[4] = total_partial_xz;
        collectives[5] = total_partial_yz;
        }
    }


cudaError_t gpu_compute_squareforces(unsigned int reference,
                                     unsigned int group_two_size,
                                     unsigned int* d_group_two_members,
                                     unsigned int* d_rtags,
                                     Scalar4* d_pos,
                                     int3* d_image,
                                     Scalar4* d_force,
                                     Scalar* d_virial,
                                     unsigned int virial_pitch,
                                     Scalar* normalization,
                                     Scalar* normalization_derivative,
                                     Scalar* collectives,
                                     const BoxDim& box,
                                     unsigned int block_size,
                                     unsigned int N,
                                     Scalar4 params)
    {
    static unsigned int max_block_size = UINT_MAX;
    if (max_block_size == UINT_MAX)
        {
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, (const void*)gpu_compute_squareforces_kernel);
        max_block_size = attr.maxThreadsPerBlock;
        }
    unsigned int run_block_size = min(block_size, max_block_size);
    // setup the grid to run the kernel
    dim3 threads(run_block_size, 1, 1);

    switch (run_block_size)
        {
    case 512:
        gpu_compute_collective_step_one<512><<<1, threads>>>(group_two_size,
                                                                  reference,
                                                                  d_rtags,
                                                                  d_group_two_members,
                                                                  d_pos,
                                                             collectives,
                                                                  normalization,
                                                                  normalization_derivative, box, params);
        break;
    case 256:
        gpu_compute_collective_step_one<256><<<1, threads>>>(group_two_size,
                                                                  reference,
                                                                  d_rtags,
                                                                  d_group_two_members,
                                                                  d_pos,
                                                             collectives,
                                                                  normalization,
                                                                  normalization_derivative, box, params);
        break;
    case 128:
        gpu_compute_collective_step_one<128><<<1, threads>>>(group_two_size,
                                                                  reference,
                                                                  d_rtags,
                                                                  d_group_two_members,
                                                                  d_pos,
                                                             collectives,
                                                                  normalization,
                                                                  normalization_derivative, box, params);
        break;
    case 64:
        gpu_compute_collective_step_one<64><<<1, threads>>>(group_two_size,
                                                                 reference,
                                                                 d_rtags,
                                                                 d_group_two_members,
                                                                 d_pos,
                                                            collectives,
                                                                 normalization,
                                                                 normalization_derivative, box, params);
        break;
    case 32:
        gpu_compute_collective_step_one<32><<<1, threads>>>(group_two_size,
                                                                 reference,
                                                                 d_rtags,
                                                                 d_group_two_members,
                                                                 d_pos,
                                                            collectives,
                                                                 normalization,
                                                                 normalization_derivative, box, params);
        break;
        }

    hipMemsetAsync(d_force, 0, sizeof(Scalar4) * N);
    hipMemsetAsync(d_virial, 0, sizeof(Scalar) * 6 * virial_pitch);

    dim3 grid_f(((1 + group_two_size) / run_block_size) + 1, 1, 1);
    gpu_compute_squareforces_kernel<<<grid_f, threads>>>(d_group_two_members,
                                                         d_rtags,
                                                         reference,
                                                         group_two_size,
                                                         d_pos,
                                                         d_force,
                                                         d_virial,
                                                         normalization,
                                                         normalization_derivative,
                                                         virial_pitch,
                                                         collectives,
                                                         box,
                                                         params);

    return cudaSuccess;
    }
