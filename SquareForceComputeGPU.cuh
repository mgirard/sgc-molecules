// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander


#include "hoomd/ParticleData.cuh"
#include "hoomd/HOOMDMath.h"


#ifndef __SQFORCECOMPUTEGPU_CUH__
#define __SQFORCECOMPUTEGPU_CUH__
using namespace hoomd;
cudaError_t gpu_compute_squareforces(unsigned int reference,
                                     unsigned int group_two_size,
                                     unsigned int *d_group_two_members,
                                     unsigned int *d_rtags,
                                     Scalar4 *d_pos,
                                     int3* d_image,
                                     Scalar4 *d_force,
                                     Scalar *d_virial,
                                     unsigned int virial_pitch,
                                     Scalar *normalization,
                                     Scalar *normalization_derivative,
                                     Scalar *collectives,
                                     const BoxDim &box,
                                     unsigned int block_size,
                                     unsigned int N,
                                     Scalar4 params);


#endif
