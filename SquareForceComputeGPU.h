// Copyright (c) 2009-2018 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander

#include "hoomd/ForceCompute.h"
#include "hoomd/ParticleGroup.h"
#include "SquareForceCompute.h"

#include <memory>
#include <map>

/*! \file SquareForceComputeGPU.h
    \brief Declares a class for computing constant forces
*/

#ifdef NVCC
#error This header cannot be compiled by nvcc
#endif

#include <pybind11/pybind11.h>
#include <hoomd/Autotuner.h>

#ifndef __SQFORCECOMPUTEGPU_H__
#define __SQFORCECOMPUTEGPU_H__

//! Adds a constant force to a number of particles
/*! \ingroup computes
*/

using namespace hoomd;

class PYBIND11_EXPORT SquareForceComputeGPU : public SquareForceCompute
    {
    public:
        //! Constructs the compute
        SquareForceComputeGPU(std::shared_ptr<SystemDefinition> sysdef,
                          unsigned int ref, std::shared_ptr<ParticleGroup> group_two,
                          Scalar z0, Scalar k);

        //! Destructor
        virtual ~SquareForceComputeGPU();


    protected:
        //! Actually compute the forces
        virtual void computeForces(uint64_t timestep);
        //GPUArray<unsigned int> m_groupTags;

    private:
        std::shared_ptr<Autotuner<1>> m_tuner;
};

//! Exports the SquareForceComputeGPUClass to python
void export_SquareForceComputeGPU(pybind11::module& m);

#endif
