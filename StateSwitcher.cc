/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "StateSwitcher.h"
#include <vector>
#include <exception>
#include <numeric>
#include <random>
#include <pybind11/stl.h>
namespace py = pybind11;

StateSwitcher::StateSwitcher(std::shared_ptr<SystemDefinition> sysdef,
                             std::shared_ptr<Trigger> trigger,
                             std::shared_ptr<ParticleGroup> group,
                             std::shared_ptr<CellList> cl,
                             std::shared_ptr<Molecules> molecules,
                             py::list py_angleTypes,
                             py::list py_angles,
                             py::list py_nonBondedTypes,
                             py::list py_nonBonded,
                             py::list py_charges,
                             py::list py_chargeForce,
                             unsigned int seed,
                             std::string name) : Updater(sysdef, trigger), m_group(group), m_cl(cl), m_molecules(molecules), m_seed(seed), /*ghost_state(ghost),*/ m_name(name){
    m_seed = m_seed*0x12345677 + 0x12345 ; m_seed^=(m_seed>>16u); m_seed*= 0x45679;
    m_exec_conf->msg->notice(5) << "Constructing StateSwitcher" << std::endl;

    m_cl->setComputeIdx(true);
    m_cl->setMultiple(2);

    //! acquire MC evaluations
    for(auto angle : py_angles)
        m_angle_forces.push_back(angle.cast<std::shared_ptr<ForceCompute>>());
    for(auto lj : py_nonBonded)
        m_lj_forces.push_back(lj.cast<std::shared_ptr<ForceCompute>>());

    swapCharges = py_charges.size() != 0;
    if(swapCharges)
        m_charge_force = py_chargeForce[0].cast<std::shared_ptr<ForceCompute>>();
    else // independently of whether we're swapping charges, we need to call m_charge_force->getForceArray() and we can't dereference a null pointer
        m_charge_force = std::make_shared<ForceCompute>(sysdef);

    swapAngles = !m_angle_forces.empty();
    swapTypes = !m_lj_forces.empty();
    auto nItems = std::max(std::max(m_lj_forces.size(), m_angle_forces.size()), py_charges.size());

    if((m_lj_forces.size() != nItems && !m_lj_forces.empty()) || (m_angle_forces.size() != nItems && !m_angle_forces.empty()) || (py_charges.size() != nItems && py_charges.size() != 0))
        throw std::runtime_error("Amount of supplied angles / non-bonded / charges doesn't match");

    if(!swapAngles && !swapTypes && !swapCharges)
        throw std::runtime_error("No swappable types / angle / charge supplied");

    //! acquire actual types
    GPUArray<unsigned int> temp_lj(m_lj_forces.size(), m_exec_conf);
    nonBondedTypes.swap(temp_lj);
    ArrayHandle<unsigned int> h_nonBondedTypes(nonBondedTypes, access_location::host, access_mode::overwrite);

    GPUArray<unsigned int> temp_angles(m_angle_forces.size(), m_exec_conf);
    angleTypes.swap(temp_angles);
    ArrayHandle<unsigned int> h_angleTypes(angleTypes, access_location::host, access_mode::overwrite);

    GPUArray<Scalar> temp_charges(py_charges.size(), m_exec_conf);
    chargeValues.swap(temp_charges);
    ArrayHandle<Scalar> h_chargeValues(chargeValues, access_location::host, access_mode::overwrite);

    // build internal description of swaps
    for(auto i = 0u; i < m_angle_forces.size(); i++)
        h_angleTypes.data[i] = py_angleTypes[i].cast<unsigned int>();
    for(auto i = 0u; i < m_lj_forces.size(); i++)
        h_nonBondedTypes.data[i] = py_nonBondedTypes[i].cast<unsigned int>();
    for(auto i = 0u; i < py_charges.size(); i++)
        h_chargeValues.data[i] = py_charges[i].cast<Scalar>();


    GPUArray<Scalar4*> tempnonBondedEnergy(m_lj_forces.size(), m_exec_conf);
    nonBondedEnergy.swap(tempnonBondedEnergy);

    GPUArray<Scalar4*> tempAngleEnergy(m_angle_forces.size(), m_exec_conf);
    angleEnergy.swap(tempAngleEnergy);

    m_nstates = std::max(std::max(m_angle_forces.size(), m_lj_forces.size()), py_charges.size());
    m_nbits = std::ceil(std::log(m_nstates) / std::log(2));

    // build an array that lets us move from tag -> angle
    GPUArray<unsigned int> tag_to_angle(m_pdata->getN(), m_exec_conf);
    tagToAngle.swap(tag_to_angle);
    ArrayHandle<unsigned int> h_tagToAngle(tagToAngle, access_location::host, access_mode::overwrite);

    ArrayHandle <Scalar4> postype(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> RTags(m_pdata->getRTags(), access_location::host, access_mode::read);

    std::fill(h_tagToAngle.data, h_tagToAngle.data + m_pdata->getN(), UINT32_MAX);

    if(swapAngles) { // we only build the tagToAngle array if we're swapping angles
        auto angles = sysdef->getAngleData();
        for (unsigned int idx = 0; idx != angles->getNGlobal(); idx++) {
            auto atype = angles->getTypeByIndex(idx);

            bool valid = false;
            for (auto i = 0u; i < m_nstates; i++)
                valid |= h_angleTypes.data[i] == atype;
            if (!valid)
                continue;

            auto acons = angles->getMembersByIndex(idx);
            h_tagToAngle.data[acons.tag[1]] = idx;
        }
    }

    if(swapAngles)
        swapGroup = molecules->addSwapGroup(group, m_nstates, &tagToAngle);
    else
        swapGroup = molecules->addSwapGroup(group, m_nstates);

}

StateSwitcher::~StateSwitcher() {
    m_exec_conf->msg->notice(5) << "Destroying StateSwitcher" << std::endl;
}

void StateSwitcher::update(uint64_t timestep) {
    if(!isInitialized)
        compute_current_states();
    m_cl->compute(timestep);
    m_molecules->compute(timestep);
    if(swapAngles)
        for(auto& f : m_angle_forces)
            f->forceCompute(timestep);
    if(swapTypes)
        for(auto& f : m_lj_forces)
            f->forceCompute(timestep);
    if(swapCharges)
        m_charge_force->forceCompute(timestep);

    ArrayHandle<unsigned int> h_angle_types(angleTypes, access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_lj_types(nonBondedTypes, access_location::host, access_mode::read);

    ArrayHandle<Scalar4> h_postype(m_pdata->getPositions(), access_location::host, access_mode::readwrite);

    ArrayHandle<Scalar4*> h_lj_ptr(nonBondedEnergy, access_location::host, access_mode::overwrite);
    ArrayHandle<Scalar4*> h_angle_ptr(angleEnergy, access_location::host, access_mode::overwrite);
    for(auto i = 0u; i < m_nstates; i++){
        if(swapTypes){
            ArrayHandle<Scalar4> ljf(m_lj_forces[i]->getForceArray(), access_location::host, access_mode::read);
            h_lj_ptr.data[i] = ljf.data;
        }
        if(swapAngles){
            ArrayHandle<Scalar4> af(m_angle_forces[i]->getForceArray(), access_location::host, access_mode::read);
            h_angle_ptr.data[i] = af.data;
        }
    }

    ArrayHandle<Scalar4> h_chargeEnergy(m_charge_force->getForceArray(), access_location::host, access_mode::read);
    ArrayHandle<typeval_t> h_angletypeval(m_sysdef->getAngleData()->getTypeValArray(), access_location::host, access_mode::readwrite);
    ArrayHandle<Scalar> h_chargeValues(chargeValues, access_location::host, access_mode::read);

    ArrayHandle<Scalar> h_charges(m_pdata->getCharges(), access_location::host, access_mode::readwrite);
    ArrayHandle<unsigned int> h_beadGroup(m_molecules->getSwapGroups(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_beadstate(m_molecules->getBeadStates(), access_location::host, access_mode::readwrite);
    ArrayHandle<unsigned int> h_molecule(m_molecules->getMolecules(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_hash(m_molecules->getHashes(), access_location::host, access_mode::readwrite);
    ArrayHandle<Scalar> h_mus(m_molecules->getPotentials(), access_location::host, access_mode::read);

    const auto& mySwapGroup = m_molecules->getSwapGroup(swapGroup);
    ArrayHandle<unsigned int> h_angle_map(mySwapGroup.mapping, access_location::host, access_mode::read);

    ArrayHandle<unsigned int> h_cellsize(m_cl->getCellSizeArray(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_cell_index(m_cl->getIndexArray(), access_location::host, access_mode::read);

    ArrayHandle<unsigned int> h_moleculeOffset(m_molecules->getHashStart(), access_location::host, access_mode::read);
    auto indexer = m_cl->getCellIndexer();
    auto cell_indexer = m_cl->getCellListIndexer();
    auto cldim = m_cl->getDim();
    uint3 start = {m_starting_indexes & 1u, (m_starting_indexes >> 1u) & 1u, (m_starting_indexes >> 2u) & 1u};

    std::mt19937 generator(timestep * 0xd43ea371 + 0xa02ac986);

    for(auto cell = 0u; cell < cldim.x * cldim.y * cldim.z / 8; cell++){
        const unsigned int mask = (1u << mySwapGroup.beadHashSize) - 1;

        unsigned int cx = cell % (cldim.x / 2),
                     cy = (cell / (cldim.x / 2)) % (cldim.y / 2),
                     cz = cell / (cldim.x * cldim.y / 4);
        cx = 2*cx + start.x;
        cy = 2*cy + start.y;
        cz = 2*cz + start.z;

        auto ci = indexer(cx, cy, cz);
        auto mycellsize = h_cellsize.data[ci];
        if(mycellsize == 0)
            continue;
        std::uniform_int_distribution<unsigned int> rparticle(0, mycellsize-1);
        auto ip = rparticle(generator);
        auto myParticle = h_cell_index.data[cell_indexer(ci, ip)];
        if(h_beadGroup.data[myParticle] != swapGroup)
            continue;

        auto istate = h_beadstate.data[myParticle];
        std::uniform_int_distribution<unsigned int> stateSelect(0, m_nstates - 2);
        auto jstate = stateSelect(generator);
        jstate += jstate >= istate ? 1 : 0;

        auto mol = h_molecule.data[myParticle];
        auto mtypei = h_hash.data[mol];

        auto pos = h_moleculeOffset.data[myParticle];

        auto mtypej = mtypei & ~((mask) << pos);
        mtypej |= jstate << pos;

        Scalar energy = 0.0f;
        if(swapAngles)
            energy += h_angle_ptr.data[jstate][myParticle].w - h_angle_ptr.data[istate][myParticle].w;

        if(swapTypes)
            energy += Scalar(2.0) * (h_lj_ptr.data[jstate][myParticle].w - h_lj_ptr.data[istate][myParticle].w);

        if(swapCharges){
            auto ic = h_chargeValues.data[istate];
            auto jc = h_chargeValues.data[jstate];
            energy += Scalar(2.0) * (jc - ic) * h_chargeEnergy.data[myParticle].w;
        }

        energy += (h_mus.data[mtypei] - h_mus.data[mtypej]);
        if(!isfinite(energy))
            continue;
        energy *= m_beta;
        auto rmetro = std::uniform_real_distribution<Scalar>(0,1);
        auto rv = rmetro(generator);
        auto criteria = exp(-energy);
        if(rv > criteria)
            continue;

        h_beadstate.data[myParticle] = jstate;
        h_hash.data[mol] = mtypej;
        if(swapTypes)
            h_postype.data[myParticle].w = __int_as_scalar(h_lj_types.data[jstate]);
        if(swapAngles) {
            auto iangle = h_angle_map.data[myParticle];
            if(iangle != UINT32_MAX)
                h_angletypeval.data[iangle].type = h_angle_types.data[jstate];
        }
        if(swapCharges)
            h_charges.data[myParticle] = h_chargeValues.data[jstate];
    }

}


void StateSwitcher::compute_current_states() {

    ArrayHandle<unsigned int> h_beadStates(m_molecules->getBeadStates(), access_location::host, access_mode::readwrite);
    ArrayHandle<unsigned int> h_hashes(m_molecules->getHashes(), access_location::host, access_mode::readwrite);
    ArrayHandle<unsigned int> h_hashStart(m_molecules->getHashStart(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_beadSwapGroup(m_molecules->getSwapGroups(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_molecules(m_molecules->getMolecules(), access_location::host, access_mode::read);

    const auto& mySwapGroup = m_molecules->getSwapGroup(swapGroup);
    
    ArrayHandle <Scalar4> postype(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> lj_types(nonBondedTypes, access_location::host, access_mode::read);
    ArrayHandle<unsigned int> angle_types(angleTypes, access_location::host, access_mode::read);
    ArrayHandle<Scalar> chargeVal(chargeValues, access_location::host, access_mode::read);

    auto angles = m_sysdef->getAngleData();
    ArrayHandle<Scalar> particleCharges(m_pdata->getCharges(), access_location::host, access_mode::read);

    unsigned int mem_sz = mySwapGroup.beadHashSize;

    for(auto idx = 0u; idx < m_pdata->getN(); idx++){
        if(h_beadSwapGroup.data[idx] != swapGroup) // this bead doesnt belong to us
            continue;
        auto beadtype = static_cast<unsigned int>(__scalar_as_int(postype.data[idx].w));
        auto charge = particleCharges.data[idx];
        auto idx_mol = h_molecules.data[idx];
        ArrayHandle<unsigned int> extraMapping(mySwapGroup.mapping, access_location::host, access_mode::read);

        for(auto swapType = 0u; swapType < m_nstates; swapType++){
            // check if the current swap type matches all criteria (particle type, angle type & charge value)
            bool valid = true;
            if(swapTypes)
                valid &= beadtype == lj_types.data[swapType];
            if(swapAngles && extraMapping.data[idx] != UINT32_MAX){
                auto angleType = angles->getTypeByIndex(extraMapping.data[idx]);
                valid &= angleType == angle_types.data[swapType];
            }
            if(swapCharges)
                valid &= charge == chargeVal.data[swapType];

            // recompute the hash associated with this molecule
            if(valid){
                h_beadStates.data[idx] = swapType;
                auto currentHash = h_hashes.data[idx_mol];
                auto offset = h_hashStart.data[idx];
                auto mask = (~((1u << mem_sz) - 1)) << offset;
                auto newHash = currentHash & mask;
                newHash |= swapType << offset;
                h_hashes.data[idx_mol] = newHash;
            }
        }
    }
    // this should be rarely called and we can check that there were no errors.
    CHECK_CUDA_ERROR();
    isInitialized = true;
}

unsigned int StateSwitcher::getMaxBeads() {
    const auto& sg = m_molecules->getSwapGroup(swapGroup);
    return sg.maxBeads;
}

void export_StateSwitcher(py::module& m){
    py::class_< StateSwitcher, Updater, std::shared_ptr<StateSwitcher> >(m,"StateSwitcher")
            .def(py::init< std::shared_ptr<SystemDefinition>, std::shared_ptr<Trigger>,std::shared_ptr<ParticleGroup>, std::shared_ptr<CellList>, std::shared_ptr<Molecules>,
                    py::list, py::list, py::list, py::list, py::list, py::list, unsigned int, std::string>())
            .def("set_temperature", &StateSwitcher::set_temperature)
            .def("getMaxBeads", &StateSwitcher::getMaxBeads)
            .def("recompute_states", &StateSwitcher::compute_current_states)
            .def("setRCut", &StateSwitcher::setRCut);
}