/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_StateSwitcher_H
#define HOOMD_StateSwitcher_H

#include <hoomd/Updater.h>
#include <hoomd/ForceCompute.h>
#include <hoomd/md/NeighborListBinned.h>
#include <vector>
#include "Molecules.h"

namespace py = pybind11;
using namespace hoomd;

class PYBIND11_EXPORT StateSwitcher: public Updater {
public:
    // it would be better to make the constructor only take # of beads and recompute states later
    StateSwitcher(std::shared_ptr<SystemDefinition> sysdef,
                  std::shared_ptr<Trigger> trigger,
                  std::shared_ptr<ParticleGroup> group,
                  std::shared_ptr<CellList> cl,
                  std::shared_ptr<Molecules> molecularDescription,
                  py::list py_angleTypes,
                  py::list py_angles,
                  py::list py_nonBondedTypes,
                  py::list py_nonBonded,
                  py::list py_chargeForce,
                  py::list py_charges,
                  unsigned int seed,
                  std::string name);

    virtual void update(uint64_t timestep);

    void set_temperature(Scalar T){m_beta = Scalar(1.0) / T;}

    void compute_current_states();

    void setRCut(Scalar rcut){
        m_cl->setNominalWidth(rcut);
    }

    ~StateSwitcher();

    unsigned int getMaxBeads();

protected:
    unsigned int m_starting_indexes = 0;

    // group to operate on
    std::shared_ptr<ParticleGroup> m_group;

    // we need to track objects which compute actual energies of swapping. Each particle can also be tagged to an angle

    // keep track of disabled force computes to compute the swap energies
    std::vector<std::shared_ptr<ForceCompute>> m_angle_forces;
    std::vector<std::shared_ptr<ForceCompute>> m_lj_forces;
    std::shared_ptr<ForceCompute> m_charge_force; //! charge is universal and not per-type

    std::shared_ptr<CellList> m_cl;
    std::shared_ptr<Molecules> m_molecules;
    unsigned int swapGroup;

    GPUArray<unsigned int> nonBondedTypes; //! available non-bonded types to switch to
    GPUArray<unsigned int> angleTypes; //! available angle types to switch to
    GPUArray<Scalar> chargeValues;

    unsigned int m_seed; //! rng seed
    GPUArray<unsigned int> tagToAngle; //! array of indexes where tagToAngle[i] points to angle where angle.b = i
    Scalar m_beta;

    //! pointers to energy calculation to access ForceCompute energies on GPU
    GPUArray<Scalar4*> nonBondedEnergy;
    GPUArray<Scalar4*> angleEnergy;

    //bool ghost_state = false; // indicate whether the last type is a ghost
    bool swapTypes = false;
    bool swapAngles = false;
    bool swapCharges = false;
    bool isInitialized = false;

    unsigned int m_nstates = 2;
    unsigned int m_nbits = 1;
    std::string m_name;

};

void export_StateSwitcher(pybind11::module& m);

#endif //HOOMD_StateSwitcher_H
