/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include <hoomd/md/NeighborListBinned.h>
#include <hoomd/GPUArray.h>
#include "StateSwitcherGPU.h"
#include "StateSwitcherGPU.cuh"

namespace py = pybind11;

int myPow(int x, int p)
{
    if (p == 0) return 1;
    if (p == 1) return x;

    int tmp = myPow(x, p/2);
    if (p%2 == 0) return tmp * tmp;
    else return x * tmp * tmp;
}

StateSwitcherGPU::StateSwitcherGPU(std::shared_ptr<SystemDefinition> sysdef,
                                   std::shared_ptr<Trigger> trigger,
                                   std::shared_ptr<ParticleGroup> group,
                                   std::shared_ptr<CellList> cl,
                                   std::shared_ptr<Molecules> molecules,
                                   py::list py_angle_types,
                                   py::list py_angles,
                                   py::list py_lj_types,
                                   py::list py_ljs,
                                   py::list charge_val,
                                   py::list charge_force,
                                   unsigned int seed,
                                   std::string name) :
        StateSwitcher(sysdef, trigger, group, cl, molecules, py_angle_types, py_angles, py_lj_types, py_ljs, charge_val, charge_force, seed,  name) {
    m_exec_conf->msg->notice(5) << "Constructing StateSwitcherGPU" << std::endl;

    m_tuner.reset(new hoomd::Autotuner<1>({AutotunerBase::makeBlockSizeRange(m_exec_conf)/*Eventually, AutotunerBase::getTppListPow2(m_exec_conf)*/}, m_exec_conf, "StateSwitcherGPU"));
    this->m_autotuners.push_back(m_tuner);
}

StateSwitcherGPU::~StateSwitcherGPU() {
    m_exec_conf->msg->notice(5) << "Destroying StateSwitcherGPU" << std::endl;
}

void StateSwitcherGPU::update(uint64_t timestep) {
    if(!isInitialized)
        compute_current_states();

    // Force computation of angles & lj
    m_cl->compute(timestep);
    m_molecules->compute(timestep);
    if(swapAngles)
        for(auto& f : m_angle_forces)
            f->forceCompute(timestep);
    if(swapTypes)
        for(auto& f : m_lj_forces)
            f->forceCompute(timestep);
    if(swapCharges)
        m_charge_force->forceCompute(timestep);

    if(m_exec_conf->isCUDAErrorCheckingEnabled())
        CHECK_CUDA_ERROR();

    ArrayHandle<unsigned int> d_angle_types(angleTypes, access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_lj_types(nonBondedTypes, access_location::device, access_mode::read);

    // grab the required arrays
    ArrayHandle <Scalar4> postype(m_pdata->getPositions(), access_location::device, access_mode::readwrite);


    // acquire array on cpu and set it
    {
        ArrayHandle<Scalar4*> h_nonBondedPointers(nonBondedEnergy, access_location::host, access_mode::overwrite);
        ArrayHandle<Scalar4*> h_anglePointers(angleEnergy, access_location::host, access_mode::overwrite);
        for (auto i = 0u; i != m_nstates; i++) {
            if(swapTypes) {
                ArrayHandle<Scalar4> ljf(m_lj_forces[i]->getForceArray(), access_location::device, access_mode::read);
                h_nonBondedPointers.data[i] = ljf.data;
            }
            if(swapAngles) {
                ArrayHandle<Scalar4> af(m_angle_forces[i]->getForceArray(), access_location::device, access_mode::read);
                h_anglePointers.data[i] = af.data;
            }
        }
    }

    ArrayHandle<Scalar4*> d_lj_ptr(nonBondedEnergy, access_location::device, access_mode::read);
    ArrayHandle<Scalar4*> d_angle_ptr(angleEnergy, access_location::device, access_mode::read);
    ArrayHandle<Scalar4> d_chargeEnergy(m_charge_force->getForceArray(), access_location::device, access_mode::read);
    ArrayHandle <typeval_t> angle_typeval(m_sysdef->getAngleData()->getTypeValArray(), access_location::device,access_mode::readwrite);
    ArrayHandle<Scalar> d_chargeValues(chargeValues, access_location::device, access_mode::read);

    ArrayHandle<unsigned int> d_beadGroup(m_molecules->getSwapGroups(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_beadstate(m_molecules->getBeadStates(), access_location::device, access_mode::readwrite);
    ArrayHandle<unsigned int> d_molecule(m_molecules->getMolecules(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_hash(m_molecules->getHashes(), access_location::device, access_mode::readwrite);
    ArrayHandle<Scalar> mus(m_molecules->getPotentials(), access_location::device, access_mode::read);

    const auto& mySwapGroup = m_molecules->getSwapGroup(swapGroup);
    ArrayHandle<unsigned int> angle_map(mySwapGroup.mapping, access_location::device, access_mode::read);

    ArrayHandle<unsigned int> cellsize(m_cl->getCellSizeArray(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> cell_index(m_cl->getIndexArray(), access_location::device, access_mode::read);

    ArrayHandle<unsigned int> d_moleculeOffset(m_molecules->getHashStart(), access_location::device, access_mode::read);
    ArrayHandle<unsigned int> d_molecularLock(m_molecules->getLock(), access_location::device, access_mode::readwrite);

    ArrayHandle<Scalar> d_charges(m_pdata->getCharges(), access_location::device, access_mode::readwrite);

    MS_update_args args;
    args.N = m_pdata->getN();


    // we use nullptr as signaling
    if(swapAngles)
        args.d_angles = d_angle_ptr.data;
    else
        args.d_angles = nullptr;
    if(swapTypes)
        args.d_ljs = d_lj_ptr.data;
    else
        args.d_ljs = nullptr;

    if(swapCharges) {
        args.d_charges = d_chargeEnergy.data;
        args.charge_values = d_chargeValues.data;
        args.d_pdata_charge = d_charges.data;
    }else {
        args.d_charges = nullptr;
        args.charge_values = nullptr;
        args.d_pdata_charge = nullptr;
    }

    args.d_postype = postype.data;
    args.d_beadGroup = d_beadGroup.data;
    args.d_state = d_beadstate.data;
    args.d_cellsize = cellsize.data;

    args.starting_values = {m_starting_indexes & 1, m_starting_indexes >> 1 & 1, m_starting_indexes >> 2 & 1};
    args.d_anglemap = angle_map.data;
    args.d_angle_typeval = angle_typeval.data;
    uint3 cldim = m_cl->getDim();
    args.cells = {cldim.x >> 1, cldim.y >> 1, cldim.z >> 1};
    args.ncells = args.cells.x * args.cells.y * args.cells.z;
    args.indexer = m_cl->getCellIndexer();
    args.cell_indexer = m_cl->getCellListIndexer();
    args.d_cellidx = cell_index.data;
    args.timestep = timestep;
    args.seed = m_seed;

    args.angle_types = d_angle_types.data;
    args.lj_types = d_lj_types.data;
    args.moleculeOffset = d_moleculeOffset.data;
    args.molecularLock = d_molecularLock.data;

    args.beta = m_beta;
    args.d_mu = mus.data;
    args.d_mtypes = d_hash.data;
    args.d_tagtomol = d_molecule.data;
    args.nbits = mySwapGroup.beadHashSize;
    args.nstates = m_nstates;
    args.swapGroup = swapGroup;

    m_tuner->begin();
    auto param = m_tuner->getParam();
    args.blocksize = param[0];
    gpu_MS_update_types(args);
    if (m_exec_conf->isCUDAErrorCheckingEnabled())
            CHECK_CUDA_ERROR();
    m_tuner->end();

    m_molecules->releaseLock();
    m_starting_indexes = (m_starting_indexes + 1) % 8; // roll the starting value of the grids
    if(swapAngles)
        m_sysdef->getAngleData()->setDirty();
}

void export_StateSwitcherGPU(py::module &m) {
    py::class_ < StateSwitcherGPU, StateSwitcher, std::shared_ptr < StateSwitcherGPU > >
            (m, "StateSwitcherGPU")
            .def(py::init < std::shared_ptr < SystemDefinition > , std::shared_ptr<Trigger>,
                    std::shared_ptr < ParticleGroup > ,
                            std::shared_ptr<CellList>,
                                    std::shared_ptr<Molecules>,
                    py::list,
                    py::list,
                    py::list,
                    py::list,
                    py::list,
                    py::list,
                    unsigned int,
                    std::string > ());

}