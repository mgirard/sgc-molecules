/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include "StateSwitcherGPU.cuh"
//#include <hoomd/Saru.h>
#include <hoomd/RandomNumbers.h>
#include <utility>
#include <cstdint>
//#include "hoomd/extern/cub/cub/cub.cuh"
#include <cub/cub.cuh>

namespace hoomd
    {

constexpr __host__ __device__ unsigned int flog2(unsigned int x)
    {
    return x == 1 ? 0 : 1 + flog2(x >> 1u);
    }

constexpr __host__ __device__ unsigned int nbits(unsigned int x)
    {
    return x == 1 ? 0 : flog2(x - 1) + 1;
    }

constexpr __host__ __device__ unsigned int setnbits(unsigned int n)
    {
    return (1u << n) - 1;
    }

struct KernelArgs
    {
    Scalar4** angle_forces;
    Scalar4** lj_forces;
    Scalar4* charge_force;
    Scalar4* d_postype;
    typeval_union* d_typeval;
    Scalar* d_charges;
    unsigned int* cell_size;
    unsigned int* d_cell_idx;
    unsigned int* d_tag_to_angle;
    unsigned int* current_state;

    unsigned int* molecularLock;
    unsigned int* moleculeOffset;

    unsigned int* d_beadGroup;
    uint3 cell_dims;
    uint3 start;
    Index3D cell_indexer;
    Index2D cell_list_indexer;
    unsigned int ncells;
    unsigned int timestep;
    unsigned int seed;
    unsigned int* lj_types;
    unsigned int* angle_types;
    Scalar* charge_values;
    Scalar beta;
    unsigned int N;
    unsigned int* d_moltype;
    unsigned int* d_mol_tags;
    unsigned int* d_tag_to_mol;
    Scalar* d_mu;
    unsigned int states;
    unsigned int swapGroup;
    };

template<unsigned int mem_sz> __global__ void gpu_MS_update_types_kernel(const KernelArgs args)
    {
    constexpr unsigned int mask = setnbits(mem_sz); // make a mask containing mem_sz bits set to 1

    unsigned int compute_idx = threadIdx.x + blockDim.x * blockIdx.x; // linear cell number
    if (compute_idx >= args.ncells || compute_idx >= args.N)
        return;

    unsigned int cx = compute_idx % (args.cell_dims.x),
                 cy = (compute_idx / args.cell_dims.x) % args.cell_dims.y,
                 cz = compute_idx / (args.cell_dims.x * args.cell_dims.y);
    cx = (cx << 1) + args.start.x;
    cy = (cy << 1) + args.start.y;
    cz = (cz << 1) + args.start.z;
    unsigned int ci = args.cell_indexer(cx, cy, cz);

    unsigned int my_cell_size = args.cell_size[ci];
    if (my_cell_size == 0)
        return;

    hoomd::RandomGenerator rng({142, args.timestep, (std::uint16_t) args.seed}, {compute_idx, 0});

    hoomd::UniformIntDistribution uniformSelect(my_cell_size - 1);
    unsigned int i = uniformSelect(rng);

    unsigned int particle_index = args.d_cell_idx[args.cell_list_indexer(i, ci)];
    if (args.d_beadGroup[particle_index] != args.swapGroup)
        return;

    unsigned int istate = args.current_state[particle_index];
    hoomd::UniformIntDistribution UniformJSelect(args.states - 2);
    unsigned int jstate = UniformJSelect(rng);
    jstate += jstate >= istate ? 1 : 0;

    unsigned int mol = args.d_tag_to_mol[particle_index];
    unsigned int mtypei = args.d_moltype[mol];

    // find where we are along the molecule
    unsigned int pos = args.moleculeOffset[particle_index];

    // set relevant bits to 0
    unsigned int mtypej
        = mtypei & ~((mask) << pos); // this sets mem_sz bits to 1, shifts and inverts as mask
    // set relevant bits to jstate
    mtypej |= jstate << (pos);

    Scalar deltaE = 0.0f;
    if (args.angle_forces)
        deltaE += (args.angle_forces[jstate][particle_index].w
                   - args.angle_forces[istate][particle_index].w);

    if (args.lj_forces)
        deltaE += Scalar(2.0)
                  * (args.lj_forces[jstate][particle_index].w
                     - args.lj_forces[istate][particle_index].w);

    if (args.charge_force)
        {
        // get the charges of both states
        auto icharge = args.charge_values[istate];
        auto jcharge = args.charge_values[jstate];
        // because the charge force estimates the energy if q = 1.0, the coulomb energy for being in state i is : qi * E(q = 1) and deltaE = (qj - qi) * E(q=1); factor of 2.0 added since half the energy is stored in particle i of all pairs
        deltaE += Scalar(2.0) * (jcharge - icharge) * args.charge_force[particle_index].w;
        }

    deltaE += (args.d_mu[mtypei] - args.d_mu[mtypej]);

    // infinite values of deltaE implies non-allowed species
    if (!isfinite(deltaE))
        return;

    deltaE *= args.beta;
    // check for exp(-deltaE) < random(0,1) and flip if
    hoomd::UniformDistribution<float> randomU(0, 1);
    Scalar rd = randomU(rng); // saru.s<Scalar>(0.0, 1.0);
    Scalar expe = deltaE < 0.0 ? 1.0 : deltaE > 32.0 ? 0.0 : fast::exp(-deltaE);

    if (rd > expe) // reject the move
        return;
    auto lockResult = atomicCAS(&args.molecularLock[mol], 0, 1);
    if (lockResult)
        return;

    args.current_state[particle_index] = jstate;
    args.d_moltype[mol] = mtypej;
    if (args.lj_forces)
        args.d_postype[particle_index].w = __int_as_scalar(args.lj_types[jstate]);
    if (args.angle_forces)
        {
        unsigned int angle_idx = args.d_tag_to_angle[particle_index];
        if (angle_idx != UINT32_MAX)
            args.d_typeval[angle_idx].type = args.angle_types[jstate];
        }
    if (args.charge_force)
        args.d_charges[particle_index] = args.charge_values[jstate];
    }

cudaError_t gpu_MS_update_types(const MS_update_args& args)
    {
    static unsigned int max_block_size = UINT32_MAX;

    if (max_block_size == UINT32_MAX)
        {
        cudaFuncAttributes attr;
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<1>);
        max_block_size = attr.maxThreadsPerBlock;
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<2>);
        max_block_size = std::max(max_block_size, (unsigned int)attr.maxThreadsPerBlock);
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<4>);
        max_block_size = std::max(max_block_size, (unsigned int)attr.maxThreadsPerBlock);
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<5>);
        max_block_size = std::max(max_block_size, (unsigned int)attr.maxThreadsPerBlock);
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<6>);
        max_block_size = std::max(max_block_size, (unsigned int)attr.maxThreadsPerBlock);
        cudaFuncGetAttributes(&attr, (const void*)gpu_MS_update_types_kernel<8>);
        max_block_size = std::max(max_block_size, (unsigned int)attr.maxThreadsPerBlock);
        max_block_size = (max_block_size / 2) * 2;
        printf("Maximum blocksize found for StateSwitcher : %u\n", max_block_size);
        }
    unsigned int round_ncells_up = args.ncells - 1;
    round_ncells_up |= round_ncells_up >> 1;
    round_ncells_up |= round_ncells_up >> 2;
    round_ncells_up |= round_ncells_up >> 4;
    round_ncells_up |= round_ncells_up >> 8;
    round_ncells_up |= round_ncells_up >> 16;
    round_ncells_up++;
    // printf("Executing particle swaps for %u molecules \n", args.Nmolecules);
    unsigned int blocksize
        = std::min(std::min(max_block_size, args.blocksize), std::max(round_ncells_up, 32u));

    dim3 threads(blocksize, 1, 1);
    dim3 grid(args.ncells / blocksize + 1, 1, 1);
    KernelArgs kargs;
    kargs.angle_types = args.angle_types;
    kargs.d_postype = args.d_postype;
    kargs.lj_types = args.lj_types;
    kargs.beta = args.beta;
    kargs.angle_forces = args.d_angles;
    kargs.lj_forces = args.d_ljs;
    kargs.beta = args.beta;
    kargs.cell_dims = args.cells;
    kargs.cell_indexer = args.indexer;
    kargs.cell_list_indexer = args.cell_indexer;
    kargs.cell_size = args.d_cellsize;
    kargs.d_cell_idx = args.d_cellidx;
    kargs.current_state = args.d_state;
    kargs.timestep = args.timestep;
    kargs.start = args.starting_values;
    kargs.d_mu = args.d_mu;
    kargs.ncells = args.ncells;
    kargs.seed = args.seed;
    kargs.N = args.N;
    kargs.d_mol_tags = args.d_molecules;
    kargs.d_tag_to_mol = args.d_tagtomol;
    kargs.d_tag_to_angle = args.d_anglemap;
    kargs.d_beadGroup = args.d_beadGroup;
    kargs.d_typeval = args.d_angle_typeval;
    kargs.d_moltype = args.d_mtypes;
    kargs.states = args.nstates;
    kargs.moleculeOffset = args.moleculeOffset;
    kargs.molecularLock = args.molecularLock;
    kargs.swapGroup = args.swapGroup;
    kargs.charge_values = args.charge_values;
    kargs.charge_force = args.d_charges;
    kargs.d_charges = args.d_pdata_charge;
    switch (args.nbits)
        {
    case 1:
        gpu_MS_update_types_kernel<1><<<grid, threads>>>(kargs);
        break;
    case 2:
        gpu_MS_update_types_kernel<2><<<grid, threads>>>(kargs);
        break;
    case 3:
        gpu_MS_update_types_kernel<3><<<grid, threads>>>(kargs);
        break;
    case 4:
        gpu_MS_update_types_kernel<4><<<grid, threads>>>(kargs);
        break;
    case 5:
        gpu_MS_update_types_kernel<5><<<grid, threads>>>(kargs);
        break;
    case 6:
        gpu_MS_update_types_kernel<6><<<grid, threads>>>(kargs);
        break;
    case 7:
        gpu_MS_update_types_kernel<7><<<grid, threads>>>(kargs);
        break;
    case 8:
        gpu_MS_update_types_kernel<8><<<grid, threads>>>(kargs);
        break;
        }
    return cudaSuccess;
    }

    }