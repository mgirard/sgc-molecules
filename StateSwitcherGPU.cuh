/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#include <hoomd/HOOMDMath.h>
#include <hoomd/Index1D.h>

#ifndef HOOMD_StateSwitcherGPU_CUH
#define HOOMD_StateSwitcherGPU_CUH

namespace hoomd
    {

#ifdef __HIPCC__
    //! A union to allow storing a Scalar constraint value or a type integer (GPU declaration)
    union typeval_union {
    unsigned int type;
    Scalar val;
    };
#else

//! Forward declaration of typeval union
union typeval_union;
#endif

// typedef hoomd::typeval_union typeval_t;

struct MS_update_args
    {
    Scalar4** d_ljs;
    Scalar4** d_angles;
    Scalar4* d_charges;
    Scalar4* d_postype;
    Scalar* d_pdata_charge;
    unsigned int* d_cellsize;
    unsigned int* d_cellidx;
    unsigned int* d_tag;
    unsigned int* moleculeOffset;
    unsigned int* molecularLock;
    uint3 starting_values;
    uint3 cells;
    Index3D indexer;
    Index2D cell_indexer;
    typeval_union* d_angle_typeval;
    unsigned int* d_anglemap;
    unsigned int* d_beadGroup;
    unsigned int* d_state;
    unsigned int ncells;
    unsigned int blocksize;
    unsigned int seed;
    unsigned int timestep;
    unsigned int* lj_types;
    unsigned int* angle_types;
    Scalar* charge_values;
    unsigned int N;
    Scalar beta;
    unsigned int* d_molecules;
    unsigned int* d_mtypes;
    unsigned int* d_tagtomol;
    Scalar* d_mu;
    unsigned int nstates;
    unsigned int nbits;
    unsigned int NMolecules;
    unsigned int swapGroup;
    };

cudaError_t gpu_MS_update_types(const MS_update_args& args);
    }
#endif //HOOMD_StateSwitcherGPU_CUH
