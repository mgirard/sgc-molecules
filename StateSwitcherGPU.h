/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_StateSwitcherGPU_H
#define HOOMD_StateSwitcherGPU_H

#include <hoomd/Updater.h>
#include <hoomd/ForceCompute.h>
#include <hoomd/CellListGPU.h>
#include <hoomd/md/NeighborListGPUBinned.h>
#include "StateSwitcher.h"

using namespace hoomd;

class PYBIND11_EXPORT StateSwitcherGPU: public StateSwitcher {
public:
    StateSwitcherGPU(std::shared_ptr <SystemDefinition> sysdef,
                 std::shared_ptr<Trigger> trigger,
                     std::shared_ptr <ParticleGroup> group,
                     std::shared_ptr <CellList> cl,
                     std::shared_ptr <Molecules> molecules,
                     pybind11::list py_angle_types,
                     pybind11::list py_angles,
                     pybind11::list py_lj_types,
                     pybind11::list py_ljs,
                     pybind11::list charge_val,
                     pybind11::list charge_force,
                     unsigned int seed,
                     std::string name);

    ~StateSwitcherGPU();

    virtual void update(uint64_t timestep);

protected:
    std::shared_ptr<Autotuner<1>> m_tuner;
    //std::unique_ptr<Autotuner> m_tuner_mpt;
};

void export_StateSwitcherGPU(pybind11::module& m);

#endif //HOOMD_StateSwitcher_H
