Getting started
-----------------

The SGC-molecules plugin is an extension of the HOOMD-Blue molecular dynamics engine. To begin, download
the package: git clone https://gitlab.mpcdf.mpg.de/mgirard/sgc-molecules, then follow the instruction for built-in
components:

.. code-block:: bash

    cd path/hoomd-blue/hoomd
    ln -s path/this_plugin plugin_muMC
    cd ../build
    make


