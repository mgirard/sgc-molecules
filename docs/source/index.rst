
Welcome the Semi-Grand canonical plug-in documentation!
=======================================================

This HOOMD-Blue plugin is designed to enable semi-grand canonical ensemble calculations based on molecules. It is currently tailored for calculations
involving lipid bilayers

.. toctree::
   :maxdepth: 3
   :caption: Introduction

   getting-started
   published-example

.. toctree::
   :maxdepth: 3
   :caption: Python Documentation

   sgcmolecules




Indices
******************

* :ref:`genindex`
* :ref:`modindex`

Source code
************

The git repository is on `gitlab <https://gitlab.mpcdf.mpg.de/mgirard/sgc-molecules>`_



