Collective Variables
---------------------

.. rubric:: Overview

.. autosummary::
    :nosignatures:

    plugin_muMC.collectives.Molecules
    plugin_muMC.collectives.LipidLeaflet

.. rubric:: Details

.. automodule:: plugin_muMC.collectives
    :synopsis: Collective descriptions molecules, their chemical fragments, leaflets and other collective variables
    :members:
