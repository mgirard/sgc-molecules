Custom Integrators
---------------------

.. rubric:: Overview

.. autosummary::
    :nosignatures:

    plugin_muMC.integrate.nptstochastic
    plugin_muMC.integrate.nphstochastic

.. rubric:: Details

.. automodule:: plugin_muMC.integrate
    :synopsis: Stochastic integration methods for pressure
    :members:
