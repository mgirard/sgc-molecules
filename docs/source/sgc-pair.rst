MC Pair potential evaluators
--------------------------------

.. rubric:: Overview

.. autosummary::
    :nosignatures:

    plugin_muMC.pair.lj_mc
    plugin_muMC.pair.charge_mc
    plugin_muMC.pair.TruncatedCoulomb

.. rubric:: Details

.. automodule:: plugin_muMC.pair
    :synopsis: Pair potentials, and special LJ for MC moves
    :members:
