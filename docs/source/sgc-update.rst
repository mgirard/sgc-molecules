Updater
---------------------

.. rubric:: Overview

.. autosummary::
    :nosignatures:

    plugin_muMC.update.StateDescriptor
    plugin_muMC.update.ManyStateSwitcher
    plugin_muMC.update.BilayerInverter

.. rubric:: Details

.. automodule:: plugin_muMC.update
    :synopsis: Update objects to change chemical nature of molecules
    :members:
