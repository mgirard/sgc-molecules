Semi-Grand canonical python package
=====================================

.. rubric:: Overview

.. autosummary::
    :nosignatures:

.. automodule:: plugin_muMC
    :members:

.. rubric:: Core

.. toctree::
    :maxdepth: 3

    sgc-collectives
    sgc-update
    sgc-pair
    sgc-integrate