/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 */

#ifndef HOOMD_MCTYPES_H
#define HOOMD_MCTYPES_H

enum class dimension{X = 0,Y = 1,Z = 2};

#endif //HOOMD_MCTYPES_H
