/* Copyright 2020 Max-Planck-Institut für Polymerforschung
 * This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
 *
 * Parts of this file include source code from HOOMD-Blue
 */

// Include the defined classes that are to be exported to python

#include <pybind11/pybind11.h>
#include <hoomd/md/PotentialPair.h>
#ifdef ENABLE_HIP
#include <hoomd/md/PotentialPairGPU.h>
#endif

#include "EvaluatorPairCoulombMC.h"
#include "EvaluatorPairGhostDPDLJThermo.h"
#include "EvaluatorPairTruncatedCoulomb.h"
#include "EvaluatorPairAlchemicalLJ.h"

#include "LipidLeaflet.h"
#include "LipidLeafletGPU.h"
#include "MCAngleCosineSq.h"
#include "MCAngleCosineSqGPU.h"
#include "PotentialPairGPU_MC.h"
#include "PotentialPair_MC.h"
#include "StateSwitcher.h"
#include "StateSwitcherGPU.h"

#include <hoomd/md/PotentialPairDPDThermo.h>
#include <hoomd/md/PotentialPairDPDThermoGPU.h>

#include "MCAngleCosineSq.h"
#include "MCAngleCosineSqGPU.h"

#include "LipidLeaflet.h"
#include "LipidLeafletGPU.h"

#include "SquareForceCompute.h"
#include "SquareForceComputeGPU.h"
#include "HarmonicKurtosis.h"
#include "HarmonicKurtosisGPU.h"

// specify the python module. Note that the name must expliclty match the PROJECT() name provided in
// CMakeLists
// (with an underscore in front)
PYBIND11_MODULE(_plugin_muMC, m)
    {
hoomd::md::detail::export_PotentialPair<EvaluatorPairCoulombMC>(m, "PotentialPairCoulombMC_base");
    export_PotentialPairMC<hoomd::md::EvaluatorPairLJ>(m, "PotentialPairLJMC");
    export_PotentialPairMC<EvaluatorPairCoulombMC>(m, "PotentialPairCoulombMC");
    hoomd::md::detail::export_PotentialPair<EvaluatorPairTruncatedCoulomb>(m, "PotentialPairTruncatedCoulomb");
    export_StateSwitcher(m);
    export_CosineSqAngleForceComputeMC(m);

    export_integerCollective(m);
    export_lipidleaflet(m);
    export_Molecules(m);
        hoomd::md::detail::export_PotentialPair<EvaluatorPairGhostDPDLJThermo>(m, "PotentialPairGhostDPDLJ");
        hoomd::md::detail::export_PotentialPairDPDThermo<EvaluatorPairGhostDPDLJThermo>(m, "PotentialPairGhostDPDLJThermoDPD");

        export_KurtosisForceCompute(m);
#ifdef ENABLE_HIP

    hoomd::md::detail::export_PotentialPair<EvaluatorPairAlchemicalLJ>(m, "PotentialPairAlchemicalLJ");
    export_SquareForceCompute(m);

    export_StateSwitcherGPU(m);
    export_PotentialPairGPUMC<hoomd::md::EvaluatorPairLJ>(m, "PotentialPairLJMCGPU");
    export_PotentialPairGPUMC<EvaluatorPairCoulombMC>(m, "PotentialPairCoulombMCGPU");
    hoomd::md::detail::export_PotentialPairGPU<EvaluatorPairTruncatedCoulomb>(m, "PotentialPairTruncatedCoulombGPU");
    export_CosineSqAngleForceComputeMCGPU(m);
    //export_TwoStepStochasticGPU(m);
    export_lipidleafletGPU(m);
    hoomd::md::detail::export_PotentialPairGPU<EvaluatorPairGhostDPDLJThermo>(m, "PotentialPairGhostDPDLJGPU");
    hoomd::md::detail::export_PotentialPairDPDThermoGPU<EvaluatorPairGhostDPDLJThermo>(m, "PotentialPairGhostDPDLJThermoDPDGPU");

    hoomd::md::detail::export_PotentialPairGPU<EvaluatorPairAlchemicalLJ>(m, "PotentialPairAlchemicalLJGPU");

    hoomd::md::detail::export_PotentialPairGPU<EvaluatorPairCoulombMC>(m, "PotentialPairCoulombMCGPU_base");
    export_SquareForceComputeGPU(m);
    export_KurtosisForceComputeGPU(m);
    #endif
    }
