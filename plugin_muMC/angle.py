# Copyright 2020 Max-Planck-Institut für Polymerforschung
# This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
#
# Parts of this file include source code from HOOMD-Blue
#

R""" Angle potentials.

Angles add forces between specified triplets of particles and are typically used to
model chemical angles between two bonds.

By themselves, angles that have been specified in an initial configuration do nothing. Only when you
specify an angle force (i.e. angle.harmonic), are forces actually calculated between the
listed particles.
"""

import os
docs = os.getenv('DOCS', None) == 'True' or os.getenv('READTHEDOCS', None) == 'True'

if not docs:
    import hoomd.md.angle
    from hoomd.data.typeparam import TypeParameter
    from hoomd.data.parameterdicts import TypeParameterDict
else:
    class _force(object):
        def __init__(self, *args, **kwargs):
            pass
    class hoomd(object):
        def __init__(self, *args, **kwargs):
            pass

import math
import sys

from . import _plugin_muMC

class cosinesq(hoomd.md.angle.Angle):
    R""" Monte-Carlo energy estimation for cosine squared angle potential.

    This angle potential needs to be defined for semi-grand canonical integration.


    """

    _ext_module = _plugin_muMC
    _cpp_class_name = 'CosineSquaredAngleForceComputeMC'

    def __init__(self):
        super().__init__()
        params = TypeParameter('params', 'angle_types',
                           TypeParameterDict(t0=float, k=float, len_keys=1))
        self._add_typeparam(params)

