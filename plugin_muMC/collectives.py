# Copyright 2020 Max-Planck-Institut für Polymerforschung
# This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
#

import os
docs = os.getenv('DOCS', None) == 'True' or os.getenv('READTHEDOCS', None) == 'True'
if not docs:
    from hoomd import _hoomd
    import hoomd
    from hoomd.operation import Compute
else:
    class pair(object):
        def __init__(self, *args, **kwargs):
            pass
    class hoomd(object):
        def __init__(self, *args, **kwargs):
            pass
    class _compute(object):
        def __init__(self, *args, **kwargs):
            pass

from itertools import product
import math
from . import _plugin_muMC
from hoomd.md import force
from hoomd.logging import log

class Molecules(Compute):
    R"""
    Collection of molecules for which hashes will be computed.

    The molecule parser assumes that every bead connected by either bonds or constraints belongs to the same molecule, even if intermediary beads
    are not in the filter supplied. To give an example, if we have a molecule A - B - C - D, and the filter contains beads A and D, this will
    still constitute a single molecule.
    """

    def __init__(self, filter):
        R"""
        Constructor
        """
        super().__init__()
        self.filter = filter
        self._fragments = []
        self.hash_size = 0
        self.dynamic_mu_call = None

        self._num_dependents = 0
        self._notified_fragments = 0

    def _attach_hook(self):
        group = self._simulation.state._get_group(self.filter)
        cpp_sys_def = self._simulation.state._cpp_sys_def
        self._cpp_obj = _plugin_muMC.Molecules(cpp_sys_def, group)
        super()._attach_hook()

    def _detach_hook(self):
        # we have to delete all fragments since they are created by external objects when they are attached
        self._fragments = []
        self.hash_size = 0
        self._notified_fragments = 0
        super()._detach_hook()

    def _add_fragment(self, state_names, repeats, name):
        self._fragments.append(Fragment(state_names, repeats, name, self.hash_size))
        self.hash_size += self._fragments[-1].hashbits
        return self._fragments[-1]

    def mu_function(self, function):
        """
        Give this object a function which will be called upon object creation to correctly set chemical potentials. The
        function should have signature F(molecules), and call molecules.set_mu
        :param function:
        :return:
        """
        self.dynamic_mu_call = function

    def _apply_mu(self):
        self._notified_fragments += 1
        if self.dynamic_mu_call and self._notified_fragments == self._num_dependents:
            self.dynamic_mu_call(self)

    def set_mu(self, state, value):
        """
        Sets the chemical potential of a given chemical state

        :param state: hash value of the state
        :type state: int
        :param value: chemical potential of the hash
        :type value: float
        :return: None
        """
        if not self._attached:
            raise SyntaxError("The method `set_mu` can only be called after object creation. "
                              "Attach a function to `mu_function` which takes this object as parameter")
        self._cpp_obj.set_mu(state, value)

    def fragments(self):
        R"""
        iterator over the states. Object iterated over is a dictionary that contains every descriptor used (e.g. chemical nature in semi-grand canonical
        ensembles and lipid leaflet), as well as the associated hash hash (with key 'hash').

        :return: Dictionary-like object with state description
        """
        prod = product(*self._fragments)

        for p in prod:
            myhash = 0
            states = {}
            for item in p:
                myhash += item['hash']
                states.update({item['fragment_name']: item['description']})
            states['hash'] = myhash
            yield states


class LipidLeaflet(Compute):
    R"""
    Adds the lipid leaflet as a molecular descriptors of molecules for the purposes of computing chemical potentials.

    The fragment name generated is always 'leaflet' and potential values are 'lower' and 'upper'. Leaflet determination is done by computing a local
    midplane. This is computed by decomposing the bilayer into a grid structure, with cells of size ~Rcut, a value that needs to be specified by
    calling setCellListRCut.

    :param molecules: Ensemble on which to compute leaflet side
    :type molecules: Molecules
    """

    def __init__(self, molecules, rcut):
        """
        Constructor
        """
        super().__init__(self)

        self.molecules = molecules
        self.molecules._num_dependents += 1
        self.rcut = rcut

    def _attach_hook(self):
        cpp_sys_def = self._simulation.state._cpp_sys_def
        is_cpu = isinstance(self._simulation.device, hoomd.device.CPU)

        if not self.molecules._attached:
            self.molecules._attach(self._simulation)

        if is_cpu:
            self.cpp_cl = _hoomd.CellList(cpp_sys_def)
            self._cpp_obj = _plugin_muMC.LipidLeaflet(cpp_sys_def, self.cpp_cl, self.molecules._cpp_obj)
        else:
            self.cpp_cl = _hoomd.CellListGPU(cpp_sys_def)
            self._cpp_obj = _plugin_muMC.LipidLeafletGPU(cpp_sys_def, self.cpp_cl, self.molecules._cpp_obj)

        self.molecules._add_fragment(['lower', 'upper'], 1, 'leaflet')
        self.molecules._apply_mu()
        self._cpp_obj.setCellListRCut(self.rcut)


class Fragment(object):
    def __init__(self, state_names=None, repeats=None, name=None, hash_offset=0):
        self.availableValues = state_names  # eg: ['saturated', 'unsaturated', 'empty'],
        self.nbitsPerState = math.ceil(math.log(len(self.availableValues)) / math.log(2))
        self.hashbits = repeats * self.nbitsPerState
        self.name = name
        self.hashOffset = hash_offset

        self.states = []
        for item in product(self.availableValues, repeat=repeats):
            hash = 0
            for index in range(len(item)):
                hash += self.availableValues.index(item[index]) << (self.nbitsPerState * index)
            hash <<= self.hashOffset
            self.states.append({'hash': hash, 'description': item, 'fragment_name': self.name})
        self.name = name

    def rename_states(self, state_names, repeats, name):
        self.availableValues = state_names
        self.name = name
        self.states = []
        for item in product(self.availableValues, repeat=repeats):
            hash = 0
            for index in range(len(item)):
                hash += self.availableValues.index(item[index]) << (self.nbitsPerState * index)
            hash <<= self.hashOffset
            self.states.append({'hash': hash, 'description': item, 'fragment_name': self.name})

    def __iter__(self):
        for state in self.states:
            yield state

class SpringToMembrane(force.Force):
    def __init__(self, tag, membrane_filter):
        super(SpringToMembrane, self).__init__()
        self.tag = tag
        self.membrane = membrane_filter
        self.z = None
        self.k = None
        self.rint = 1.5
        self.rout = 2.0

    def _attach_hook(self):
        cpp_sys_def = self._simulation.state._cpp_sys_def
        is_cpu = isinstance(self._simulation.device, hoomd.device.CPU)
        group = self._simulation.state._get_group(self.membrane)

        if is_cpu:
            cls = _plugin_muMC.SquareForce
        else:
            cls = _plugin_muMC.SquareForceGPU
        self._cpp_obj = cls(cpp_sys_def, self.tag, group, 0.0, 0.0)
        if self.z is not None and self.k is not None:
            self._cpp_obj.setParameters(self.z, self.k)
        self._cpp_obj.setClamp(self.rint, self.rout)

    def setParameters(self, z, k):
        self.z = z
        self.k = k
        if self._attached:
            self._cpp_obj.setParameters(z, k)

    def setClamp(self, rint, rout):
        self.rint = rint
        self.rout = rout
        if self._attached:
            self._cpp_obj.setClamp(rint, rout)

    @log(requires_run=True)
    def partialZ(self):
        """float: The potential energy :math:`U` of the system from this force \
        :math:`[\\mathrm{energy}]`."""
        self._cpp_obj.compute(self._simulation.timestep)
        return self._cpp_obj.getPartialZ()

    @log(requires_run=True)
    def partialK(self):
        """float: The potential energy :math:`U` of the system from this force \
        :math:`[\\mathrm{energy}]`."""
        self._cpp_obj.compute(self._simulation.timestep)
        return self._cpp_obj.getPartialK()

    @log(requires_run=True)
    def collective_location(self):
        self._cpp_obj.compute(self._simulation.timestep)
        return self._cpp_obj.getCollective()

class MembraneKurtosis(force.Force):
    def __init__(self, filter):
        super(MembraneKurtosis, self).__init__()
        self.filter = filter
        self.p_mu2 = dict(k=0.0, kappa0=0.0)
        self.p_mu4 = dict(k=0.0, kappa0=0.0)

    def setParameters_mu2(self, k, kappa0):
        self.p_mu2 = {'k': k, 'kappa0': kappa0}
        if self._attached:
            self._cpp_set_params()
    def setParameters_mu4(self, k, kappa0):
        self.p_mu4 = {'k': k, 'kappa0': kappa0}
        if self._attached:
            self._cpp_set_params()

    def _attach_hook(self):
        cpp_sys_def = self._simulation.state._cpp_sys_def
        is_cpu = isinstance(self._simulation.device, hoomd.device.CPU)
        group = self._simulation.state._get_group(self.filter)

        if is_cpu:
            self.cpp_class = _plugin_muMC.KurtosisForceCompute
        else:
            self.cpp_class = _plugin_muMC.KurtosisForceComputeGPU
        self._cpp_obj = self.cpp_class(cpp_sys_def, group)
        self._cpp_set_params()

    def _cpp_set_params(self):
        self._setParameters_mu2(**self.p_mu2)
        self._setParameters_mu4(**self.p_mu4)

    def _setParameters_mu2(self, k, kappa0):
        self._cpp_obj.setParameters_mu2(k, kappa0)

    def _setParameters_mu4(self, k, kappa0):
        self._cpp_obj.setParameters_mu4(k, kappa0)

