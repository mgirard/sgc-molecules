# Copyright 2020 Max-Planck-Institut für Polymerforschung
# This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
#
# Parts of this file include source code from HOOMD-Blue
#
import os
docs = os.getenv('DOCS', None) == 'True' or os.getenv('READTHEDOCS', None) == 'True'

if not docs:
    from hoomd import _hoomd
    from hoomd.md import _md;
    import hoomd;
    from hoomd.md.pair import Pair
    import math
else:
    class pair(object):
        def __init__(self, *args, **kwargs):
            pass
    class hoomd(object):
        def __init__(self, *args, **kwargs):
            pass
        def __getattr__(self, item):
            return None

from . import _plugin_muMC
from hoomd.data.parameterdicts import TypeParameterDict
from hoomd.data.parameterdicts import ParameterDict
from hoomd.data.typeparam import TypeParameter


class Pair_MC(Pair):
    def __init__(self, nlist, default_r_cut=None, default_r_on=0., mode='none', override=None):
        super().__init__(nlist, default_r_cut, default_r_on, mode)
        self.override = override

    def _attach_hook(self):
        if self.nlist._attached and self._simulation != self.nlist._simulation:
            warnings.warn(
                f"{self} object is creating a new equivalent neighbor list."
                f" This is happending since the force is moving to a new "
                f"simulation. Set a new nlist to suppress this warning.",
                RuntimeWarning)
            self.nlist = copy.deepcopy(self.nlist)
        self.nlist._attach(self._simulation)

        ov = self.override
        if isinstance(ov, str):
            ov = self._simulation.state._cpp_sys_def.getParticleData().getTypeByName(self.override)

        if isinstance(self._simulation.device, hoomd.device.CPU):
            cls = getattr(self._ext_module, self._cpp_class_name)
            self.nlist._cpp_obj.setStorageMode(
                _md.NeighborList.storageMode.half)
        else:
            cls = getattr(self._ext_module, self._cpp_class_name + "GPU")
            self.nlist._cpp_obj.setStorageMode(
                _md.NeighborList.storageMode.full)
        self._cpp_obj = cls(self._simulation.state._cpp_sys_def,
                            self.nlist._cpp_obj, ov)


class lj_mc(Pair_MC):
    R"""
    Sets up energy calculations for Monte-Carlo exchanges that use LJ forces.

    For each potential alchemical transformations, one of these forces needs to be created (and disabled) and passed to the semi-grand canonical
    updater

    :param r_cut: cutoff distance for the LJ force
    :type r_cut: float
    :param nlist: neighbour list to use
    :type nlist: py:mod:`hoomd.nlist`
    :param name: name of this force
    :type name: str
    :param override: beadtype used for LJ alchemical transformations
    :type override: str

    """
    # set static class data
    _ext_module = _plugin_muMC
    _cpp_class_name = "PotentialPairLJMC"
    _accepted_modes = ("none",)
    def __init__(self, nlist, default_r_cut=None, default_r_on=0., mode='none', override=None):
        super().__init__(nlist, default_r_cut, default_r_on, mode, override)
        params = TypeParameter(
            'params', 'particle_types',
            TypeParameterDict(sigma=float, epsilon=float, len_keys=2))
        self._add_typeparam(params)


class TruncatedCoulomb(Pair):
    R"""
    Sets up a truncated coulomb interaction as used by MARTINI force-field

    :param r_cut: cutoff distance for truncated forces
    :type r_cut: float
    :param nlist: neighbour list to use
    :type nlist: py:mod:`hoomd.nlist`
    :param name: name of this force
    :type name: str
    """

    # set static class data
    _ext_module = _plugin_muMC
    _cpp_class_name = "PotentialPairTruncatedCoulomb"
    _accepted_modes = ("none",)

    def __init__(self, nlist, default_r_cut=None, default_r_on=0., mode='none'):
        super().__init__(nlist, default_r_cut, default_r_on, mode)
        params = TypeParameter(
            'params', 'particle_types',
            TypeParameterDict(sigma=0.0, len_keys=2))
        self._add_typeparam(params)

class charge_mc(Pair_MC):
    R"""
    Sets up a truncated coulomb interaction for MC alchemical transformations of charges

    :param r_cut: cutoff distance for truncated forces
    :type r_cut: float
    :param nlist: neighbour list to use
    :type nlist: py:mod:`hoomd.nlist`
    :param name: name of this force
    :type name: str
    """

    # set static class data
    _ext_module = _plugin_muMC
    _cpp_class_name = "PotentialPairCoulombMC"
    _accepted_modes = ("none",)

    def __init__(self, nlist, default_r_cut=None, default_r_on=0., mode='none', override=None):
        super().__init__(nlist, default_r_cut, default_r_on, mode, override)
        params = TypeParameter(
            'params', 'particle_types',
            TypeParameterDict(sigma=0.0, len_keys=2))
        self._add_typeparam(params)

class ghost_dpdlj(Pair):
    R""" Dissipative Particle Dynamics with a LJ conservative force

    This is a variant of the base HOOMD DPDLJ class that also applies DPD friction forces even when conservative
    forces are zero for a given pair.
    """
    # set static class data
    _ext_module = _plugin_muMC
    _cpp_class_name = "PotentialPairGhostDPDLJThermoDPD"
    _accepted_modes = ("none",)

    def __init__(self, nlist, kT, default_r_cut=None, mode='none'):

        super().__init__(nlist=nlist,
                         default_r_cut=default_r_cut,
                         default_r_on=0,
                         mode=mode)
        params = TypeParameter(
            'params', 'particle_types',
            TypeParameterDict(epsilon=float,
                              sigma=float,
                              gamma=float,
                              len_keys=2))
        self._add_typeparam(params)

        d = ParameterDict(kT=hoomd.variant.Variant)
        self._param_dict.update(d)

        self.kT = kT

    def _attach_hook(self):
        """DPDLJ uses RNGs. Warn the user if they did not set the seed."""
        self._simulation._warn_if_seed_unset()
        super()._attach_hook()



class alchemicalLJ(Pair):
    # set static class data
    _ext_module = _plugin_muMC
    _cpp_class_name = "PotentialPairAlchemicalLJ"
    _accepted_modes = ("none",)

    def __init__(self, nlist, default_r_cut=None, default_r_on=0., mode='none'):
        super().__init__(nlist, default_r_cut, default_r_on, mode)
        params = TypeParameter(
            'params', 'particle_types',
            TypeParameterDict(mean_epsilon=float,
                              delta_epsilon=float,
                              mean_sigma_pow_6=float,
                              delta_sigma_pow_6=float,
                              len_keys=2))
        self._add_typeparam(params)

#    def process_coeff(self, coeff):
#        epsilon1 = coeff['epsilon_one']
#        epsilon2 = coeff['epsilon_two']
#        sigma1 = coeff['sigma_one']
#        sigma2 = coeff['sigma_two']
#        _lambda = coeff['lambda']
#
#        mean_eps = _lambda * epsilon1 + (1-_lambda) * epsilon2
##        mean_sigma_pow_6 = (_lambda * sigma1 + (1-_lambda) * sigma2)**6.0
#        delta_eps = epsilon1 - epsilon2
#        delta_sigma = (sigma1 - sigma2) / (_lambda * sigma1 +(1-_lambda) * sigma2)
#        return _hoomd.make_scalar4(mean_eps, mean_sigma_pow_6, delta_eps, delta_sigma)