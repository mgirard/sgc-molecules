# Copyright 2020 Max-Planck-Institut für Polymerforschung
# This file is part of the semi-grand canonical plugin, under BSD 3-Clause License
#
import os
docs = os.getenv('DOCS', None) == 'True' or os.getenv('READTHEDOCS', None) == 'True'

if not docs:
    from hoomd import _hoomd
    import hoomd
    from hoomd.md import _md
    #from hoomd.update import _updater
    from hoomd.operation import Updater

else:
    class pair(object):
        def __init__(self, *args, **kwargs):
            pass
    class hoomd(object):
        def __init__(self, *args, **kwargs):
            pass
    class _updater(object):
        def __init__(self, *args, **kwargs):
            pass

import numpy as np
import math
from . import _plugin_muMC
from . import collectives
from . import pair
from . import angle

class StateDescriptor(object):
    R"""
    Describes bead states available to a ManyStateSwitcher.

    The description is a series of lists (or None) of equal length. Index 0 of every list will describe state 0 and so on. The MC update force objects need to be passed as lists as well, with the notable exception of charge, which is a
    single entity since the force only depends on charge.

    :param charges: List of charges, in simulation units (e.g. [0, 0.23, -0.23])
    :type charges: list[float]
    :param charge_force: MC computation of charge changes
    :type charge_force: pair.charge_mc
    :param types: List of beadtypes
    :type types: list[str]
    :param type_force: force objects associated with type change for MC updates
    :type type_force: list[pair.lj_mc]
    :param angles: List of angle types (eg ['Sat180', 'Unsat120'])
    :type angles: list[str]
    :param angle_force: force objects associated with angle changes for MC updates
    :type angle_force: list[angle.cosinesq]

    """

    def __init__(self, charges=None, charge_force=None, types=None, type_force=None, angles=None, angle_force=None):

        self.charges = charges
        self.charge_force = charge_force

        self.types = types
        self.type_force = type_force

        self.angles = angles
        self.angle_force = angle_force


class ManyStatesSwitcher(Updater):
    R"""
    Attempts swaps between molecules chemical states to enforce a semi-grand canonical ensemble

    The semi-grand canonial ensemble can be used to change bead nature (non-bonded interactions) as well as angle potentials. In the latter case, it will swap angles where the bead is centered on. If we consider a set of molecules comprised
    of three beads of type A and B and angles 'A' and 'B', potential molecules types are AAA (with angle 'A'), AAB (with angle 'A') ABA (with angle 'B'), ABB (with angle 'B'), ... Note that in this case AAB and BAA would be different molecules.

    The semi-grand canonical ensemble can swap angle and/or bead types independently, i.e., one of the sets of lists may be left empty.

    Different ensembles can be used for different parts of the molecule. In such cases, they should be executed on different phases

    :param group: group on which to perform MC swaps
    :type group: hoomd.group.group
    :param molecules: molecule descriptors
    :type molecules: collectives.Molecules
    :param descriptor: description of the available states
    :type descriptor: StateDescriptor
    :param seed: seed for RNG generation
    :type seed: int
    :param stateNames: List of names associated with the different states. This is used to generate fragment description, in turn used to translate hash values into chemical information. Defaults to combination of angle and chemical types
    :type stateNames: list[str]
    :param period: MC update frequency
    :type period: int
    :param phase: When -1, start on the current time step. When >= 0, execute on steps where *(step + phase) % period == 0*. (see standard hoomd updater configuration)
    :type phase: int
    :param kT: temperature of the updater, used to compute probability exp(-E / kT)
    :type kT: float
    :param name: Suffix of name of this set of chemical potentials for fragment based chemical potential setting (see Molecules objects). Name of the fragment is SGC+name
    :type name: str

    """
    def __init__(self,
                 trigger,
                 filter,
                 molecules,
                 descriptor,
                 seed=np.random.randint(65535, 2000000),
                 stateNames=None,
                 kT=1.0,
                 name=None,
                 r_cut=None):

        super().__init__(trigger)

        if not isinstance(descriptor, StateDescriptor):
            raise SyntaxError("ManyStatesSwitcher requires a descriptor based on the StateDescriptor object")
        self.filter = filter
        self.stateNames = stateNames
        if name is None:
            self.name = ''
        else:
            self.name = '_' + name
        self.descriptor = descriptor
        self.molecules = molecules
        self.molecules._num_dependents += 1
        self.kT = kT
        self.seed = seed
        self.r_cut = r_cut

    def _attach_hook(self):
        group = self._simulation.state._get_group(self.filter)
        cpp_sys_def = self._simulation.state._cpp_sys_def

        is_cpu = isinstance(self._simulation.device, hoomd.device.CPU)

        if self.descriptor.angles is not None:
            self.angleTypes = [cpp_sys_def.getAngleData().getTypeByName(t) for t in self.descriptor.angles]
            for f in self.descriptor.angle_force:
                f._attach(self._simulation)
            self.angleForcesMC = [f._cpp_obj for f in self.descriptor.angle_force]
        else:
            self.angleTypes = []
            self.angleForcesMC = []

        if self.descriptor.types is not None:
            for f in self.descriptor.type_force:
                f._attach(self._simulation)
            self.nonBondedTypes = [cpp_sys_def.getParticleData().getTypeByName(t) for t in self.descriptor.types]
            self.nonBondedForcesMC = [f._cpp_obj for f in self.descriptor.type_force]
        else:
            self.nonBondedTypes = []
            self.nonBondedForcesMC = []

        if self.descriptor.charges is not None:
            self.descriptor.charge_force._attach(self._simulation)
            self.charges = [c for c in self.descriptor.charges]
            self.chargeForceMC = [self.descriptor.charge_force._cpp_obj]
        else:
            self.charges = []
            self.chargeForceMC = []

        # ensure that we have created the molecule object
        if not self.molecules._attached:
            self.molecules._attach(self._simulation)

        args = [cpp_sys_def,
                self.trigger,
                group,
                0,
                self.molecules._cpp_obj,
                self.angleTypes,
                self.angleForcesMC,
                self.nonBondedTypes,
                self.nonBondedForcesMC,
                self.charges,
                self.chargeForceMC,
                self.seed,
                self.name]

        if not is_cpu:
            self.cpp_cl = _hoomd.CellListGPU(cpp_sys_def)
            args[3] = self.cpp_cl
            self._cpp_obj = _plugin_muMC.StateSwitcherGPU(*args)
        else:
            self.cpp_cl = _hoomd.CellList(cpp_sys_def)
            args[3] = self.cpp_cl
            self._cpp_obj = _plugin_muMC.StateSwitcher(*args)

        self.set_params(kT=self.kT)
        self.nbeads = self._cpp_obj.getMaxBeads()

        if self.stateNames is not None:
            fragmentNames = self.stateNames
        else:
            iterables = (x for x in (self.nonBondedTypes, self.angleTypes, self.charges) if x)
            fragmentNames = list(zip(*iterables))

        self.myFragment = self.molecules._add_fragment(fragmentNames, self.nbeads, 'SGC' + self.name)
        self.molecules._apply_mu()
        if self.r_cut:
            self._cpp_obj.setRCut(self.r_cut)
        if self.kT:
            self._cpp_obj.set_temperature(self.kT)

    def set_params(self, kT=None):
        """
        Sets semi-grand canonical parameter kT
        :param kT: Simulation temperature
        :return:
        """
        if kT is not None:
            self.kT = kT
            if self._attached:
                self._cpp_obj.set_temperature(kT)

    def update_molecules(self):
        """
        Recompute states, for instance after restoring a snapshot

        :return:
        """
        self._cpp_obj.recompute_states()

    def setRCut(self, value):
        """
        Set minimal cell width for checkerboard decomposition. This value should be at least equal to interaction length

        :param value: Rcut value
        :type value: float
        :return:
        """
        self.r_cut = value
        if self._attached:
            self._cpp_obj.setRCut(value)
